export const AppConfig = {
  production: false,
  environment: 'LOCAL'
};


export const environment = {
  production: false,
  httpTimeout: 30000, // millisecond
  retryAttempt: 3,
  apiUrl: 'http://localhost:8774',
  recaptchaSiteKey: '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI',
  apiProfileMock: 'https://9b818ed5-bc14-4108-af94-155cbd4a9823.mock.pstmn.io',
  webEdgeServiceUrl: 'http://10.11.2.24:8664/rd-web-edge-service',
  webCitServiceUrl: 'http://10.11.2.24:8776/rd-cit-service',
  webUrlEpayment: 'http://10.11.2.21:4200/rd-efiling-web',

  epaymentUrl: 'http://10.11.2.25:8770/rd-epayment-service/epay',
  profileUrl: 'http://10.11.2.26:8773/rd-profile-service',
  pitUrl: 'http://10.11.2.24:8775/rd-pit-service',
  whtUrl: 'http://10.11.2.24:8779/rd-wht-service',
  printFormUrl: 'http://10.11.2.24:8800/rd-printform-service',
  masterUrl: 'http://10.11.2.24:8789/rd-master-service',
  registerUrl: 'http://10.11.2.26:8772/rd-register-service',
  authUrl: 'http://10.11.2.22:8762/rd-user-oauth-service',
  commonUrl: 'http://10.11.2.24:8797/rd-common-service',
  objectStorageUrl: 'http://10.11.2.25:8771/rd-object-storage-service'
};