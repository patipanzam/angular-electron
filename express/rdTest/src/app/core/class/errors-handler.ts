import { Injectable, ErrorHandler } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";

@Injectable()
export class ErrorsHandler implements ErrorHandler {
    
    handleError(error: Error | HttpErrorResponse) {   
        if (error instanceof HttpErrorResponse) {
           
           if (!navigator.onLine) {
             // Handle offline error
           } else {
             // Handle Http Error (error.status === 403, 404...)
           }
           
        } else {
          // Handle Client Error (Angular Error, ReferenceError...)     
        }
     
       console.error('error: ', error);
    }

}
