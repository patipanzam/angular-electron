import { Injectable } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { Observable, of, throwError } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { map, catchError, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import * as Config from '../config/constants';
import { environment } from '../../environments/environment';
import { User, UserService } from './user.service';

@Injectable()
export class AuthService {
  private jwtObject: any;
  private jwtHelper: JwtHelperService = new JwtHelperService();

  redirectUrl: string;
  isLoggedIn: Boolean = false;

  constructor(private http: HttpClient, private router: Router, private userService: UserService) { }

  login(user: User): Observable<any> {
    if (this.isLoggedIn) {
      return of(true);
    }

    if (user.username !== '' && user.password !== '') {
      const _url = `${environment.authUrl}/oauth/token`;
      const _body = `username=${user.username}&password=${user.password}&grant_type=password&client_id=rduserweb`;
      const _httpOptions = {
        headers: new HttpHeaders({
          'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
          'noToken': 'noToken'
        })
      };
      return this.http.post<any>(_url, _body, _httpOptions)
        .pipe(
          map(response => {
            this.setResponse(response);
          }),
          catchError(error => {
            this.isLoggedIn = false;
            return throwError(error);
          })
        );
    }
  }

  refreshAccessToken(): Observable<any> {
    const refreshToken = this.getRefreshToken();
    if (refreshToken && this.jwtHelper.isTokenExpired(refreshToken)) {
      console.log('AuthService.refreshAccessToken [refresh_token is expired ? true] => logout()');
      this.logout();
      return of(this.isLoggedIn);
    } else {
      const _url = `${environment.authUrl}/oauth/token`;
      const _body = `grant_type=refresh_token&refresh_token=${localStorage.getItem(Config.REFRESH_TOKEN_KEY)}&client_id=rduserweb`;
      const _httpOptions = {
        headers: new HttpHeaders({
          'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
          'noToken': 'noToken'
        })
      };
      return this.http.post<any>(_url, _body, _httpOptions)
        .pipe(
          tap(response => {
            this.setResponse(response);
          }, (error) => {
            this.isLoggedIn = false;
            return throwError(error);
          })
        );
    }
  }

  setResponse(response) {
    if (response.access_token) {
      localStorage.setItem(Config.TOKEN_KEY, response.access_token);
      localStorage.setItem(Config.REFRESH_TOKEN_KEY, response.refresh_token);
      this.jwtObject = this.jwtHelper.decodeToken(response.access_token);
      this.isLoggedIn = true;
      this.userService.setJwtObject(this.jwtObject);

      return of(this.isLoggedIn);
    } else {
      throw throwError(response.error_description);
    }
  }

  logout() {
    if(this.jwtObject && this.jwtObject.jti) {
      const _url = `${environment.authUrl}/user-auth/tokens/revoke/${this.jwtObject.jti}`;
      const _httpOptions = {
        headers: new HttpHeaders({
          'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
          'noToken': 'noToken'
        })
      };

      this.http.post<any>(_url, {}, _httpOptions).subscribe(() => { });
    }

    localStorage.removeItem(Config.TOKEN_KEY);
    localStorage.removeItem(Config.REFRESH_TOKEN_KEY);
    localStorage.removeItem(Config.MENU_KEY);
    this.isLoggedIn = false;
    this.redirectUrl = '/';
    this.router.navigate(['/login']);
  }

  checkAuthen() {
    const token = this.getToken();
    if (token) {
      this.jwtObject = this.jwtHelper.decodeToken(token);
      this.isLoggedIn = !this.jwtHelper.isTokenExpired(token);
      this.userService.setJwtObject(this.jwtObject);

      if (this.isLoggedIn) {
        console.log('AuthService.checkAuthen [token is expired ? false] => redirectPage()');
        this.redirectPage();
      } else {
        console.log('AuthService.checkAuthen [token is expired ? true] => refreshAccessToken()');
        this.refreshAccessToken().subscribe(
          () => {
            if (this.isLoggedIn) {
              this.redirectPage();
            }
          }, (err) => {
            console.log('error -> ', err);
          });
      }
    }
  }

  getToken(): string {
    return localStorage.getItem(Config.TOKEN_KEY) || '';
  }

  getRefreshToken(): string {
    return localStorage.getItem(Config.REFRESH_TOKEN_KEY);
  }

  redirectPage() {
    const redirect = this.redirectUrl ? this.redirectUrl : '/';
    const navigationExtras: NavigationExtras = {
      preserveQueryParams: true,
      preserveFragment: true
    };
    this.router.navigate([redirect], navigationExtras);
  }
}
