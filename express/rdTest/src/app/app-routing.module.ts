import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppCustomPreloader } from './app-routing-loader';
import { AuthGuard } from './auth/auth.guard';
import { MainComponent } from './core/main/main.component';

import { from } from 'rxjs';

const routes: Routes = [{
    path: '',
    component: MainComponent,
    // resolve: { resolveData: MainMenuResolve },
    children: [{
        path: '',
        // canActivate: [AuthGuard],
        loadChildren: './modules/tax/tax.module#TaxModule',
        data: { preload: true, delay: true }
    }, {
        path: 'tax',
        // canActivate: [AuthGuard],
        loadChildren: './modules/tax/tax.module#TaxModule',
        data: { preload: false }
    }]
}, {
    path: '**',
    redirectTo: ''
}];

@NgModule({
    imports: [RouterModule.forRoot(routes, {
        scrollPositionRestoration: 'enabled',
        preloadingStrategy: AppCustomPreloader,
        onSameUrlNavigation: "reload"
    })],
    exports: [RouterModule],
    providers: [AppCustomPreloader]
})
export class AppRoutingModule { }
