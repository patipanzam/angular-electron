import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { TaxFilingCardModalComponent } from './components/tax-filing-card-modal/tax-filing-card-modal.component';
import { TaxFilingExitModalComponent } from './components/tax-filing-exit-modal/tax-filing-exit-modal.component';
import { TaxFilingCardComponent } from './components/tax-filing-card/tax-filing-card.component';

import { TaxMenuService } from '../modules/tax/pages/tax-dashboard/service/tax-menu.service';
import { RouterModule } from '@angular/router';
import { WhtTaxComponent } from "../feature/components/tax-filing-card-modal/wht-tax/wht-tax.component";


@NgModule({
    declarations: [
        TaxFilingCardComponent,
        TaxFilingCardModalComponent,
        TaxFilingExitModalComponent, 
        WhtTaxComponent
    ],
    imports: [
        SharedModule,
        RouterModule,
    ],
    exports: [
        TaxFilingCardComponent,
        TaxFilingCardModalComponent,
        TaxFilingExitModalComponent,
    ],
    providers: [TaxMenuService]
})
export class FeatureModule { }
