import { Component, OnInit, ViewChild,TemplateRef, ElementRef, ChangeDetectorRef } from '@angular/core';
import { ModalDirective, TabsetComponent } from 'ngx-bootstrap';
import { TaxMenu } from '../../../../modules/tax/pages/tax-dashboard/models/tax-menu.model';
import { FormGroup, Validators, FormBuilder ,AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { TaxMenuService } from '../../../../modules/tax/pages/tax-dashboard/service/tax-menu.service';
import { UserService, UserModel } from '../../../../auth/user.service';
import { VersionModel } from '../../../../modules/tax/pages/tax-dashboard/models/version-model';
import { Location } from '@angular/common';
import { DateLocalThPipe } from '../../../../shared/pipes/date-local-th.pipe';
import { FileUploader, FileLikeObject } from 'ng2-file-upload/ng2-file-upload';
import { ValidationMessageConfigTh } from '../../../../config/validation-message.th';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';

@Component({
  selector: 'ef-wht-tax',
  templateUrl: './wht-tax.component.html',
  styleUrls: ['./wht-tax.component.scss']
})
export class WhtTaxComponent implements OnInit {
  @ViewChild(ModalDirective) modal: ModalDirective;
  @ViewChild('childModal') childModal: ModalDirective;
  @ViewChild('staticTabs') staticTabs: TabsetComponent;
  @ViewChild('templateUpload') templateUpload : any;
  
  //data upload
  modalRef : BsModalRef;
  resultUpload : string;
  ref_no : string;
  allowedMimeType = ['application/zip']
  allowedMimeType2 = ['text/csv','text/plain']
  maxFileSize = 2 * 1024 * 1024;
  uploader: FileUploader;
  uploader2: FileUploader;
  fileName;

  //data table
  menuSelected: TaxMenu;
  formData: any;
  pndList: any = [];
  dataTable: any = [];
  dataIsReal: any;
  pndListLoading: boolean;
  pageIndex = 1;
  pageSize = 5;
  total = 1;
  loading = true;

  formSaveData: FormGroup;
  formTransferdata: FormGroup;

  public checkAdvancedSearch: boolean;
  public onClickSearchAdvanced: boolean;
  public inputSearch = null;
  public month = null;
  public year = null;
  public formCodeGlo: string;
  public formSubmitAttempt: boolean;

  public showModalDeleteDraft: boolean = false;
  public showModalDeleteDraftSucess: boolean = false;
  public content: string;
  public draftNo;
  public version;
  public selectChoice;
  public prefix: string = "D01";

  chkChecked: boolean = false;
  chkCheckedSup: boolean = false;
  chkCheckedDefault: boolean = false;
  chkAdvanced: boolean = false;

  mookupDataDraft = [
    { Id13: '157040099474', titleName: 'นาย', firstName: 'รวบรวม', lastName: 'ร่วมมือ' },
    { Id13: '355509850588', titleName: 'นาง', firstName: 'ดำรง', lastName: 'คงยืน' },
    { Id13: '099276882643', titleName: 'นางสาว', firstName: 'กรรกนก', lastName: 'ชื่นบาน' },
  ];

  listBranchType = [
    {branchType:'W',branchTypeName:'หัก ณ ที่จ่าย'},
    {branchType:'V',branchTypeName:'มูลค่าเพิ่ม'},
    {branchType:'S',branchTypeName:'ธุรกิจเฉพาะ'},
  ]

  monthList = [
    { id: 1,  code: '01', nameLocal: 'มกราคม' },
    { id: 2,  code: '02', nameLocal: 'กุมภาพันธ์' },
    { id: 3,  code: '03', nameLocal: 'มีนาคม' },
    { id: 4,  code: '04', nameLocal: 'เมษายน' },
    { id: 5,  code: '05', nameLocal: 'พฤษภาคม' },
    { id: 6,  code: '06', nameLocal: 'มิถุนายน' },
    { id: 7,  code: '07', nameLocal: 'กรกฎาคม' },
    { id: 8,  code: '08', nameLocal: 'สิงหาคม' },
    { id: 9,  code: '09', nameLocal: 'กันยายน' },
    { id: 10, code: '10', nameLocal: 'ตุลาคม' },
    { id: 11, code: '11', nameLocal: 'พฤศจิกายน' },
    { id: 12, code: '12', nameLocal: 'ธันวาคม' },
  ];

  optionSelect = [{
    value: 1,
    text: "ปีภาษี/เดือน"
},
{
    value: 2,
    text: "หมายเลขอ้างอิงแบบฉับบร่าง"
}
]

  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;
  userModel: any = {};
  userProfile: any = {};
  dataPND: any;
  formName: string;
  formName2: string;

  
  public listBranchNoFiling = [];
  public listBranchTypeUpload = [];
  public listBranchNoUpload = [];
  public liclistDraft = [];
  public listTaxMonth = [];
  public listTaxYear = [];
  public listTaxMonthUpload = [];
  public listTaxYearUpload = [];
  public listTaxMonthCoppy = [];
  public filingNoList = [];
  public TaxYear = [];

  public listYear = [];
  public listMonth = [];

  pndFormUpload: FormGroup = this.fb.group({
    branchType: this.fb.control(null, Validators.required)
    , branchNo: this.fb.control(null, Validators.required)
    , year: this.fb.control(null, Validators.required)
    , month: this.fb.control(null, Validators.required)
  });

  pndFormDraft: FormGroup = this.fb.group({
    year: this.fb.control(null, Validators.required)
  });

  constructor(
    private router: Router,
    private elRef: ElementRef,
    private taxMenuService: TaxMenuService,
    private userService: UserService,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private routerActive: ActivatedRoute,
    private location: Location,
    private modalService : BsModalService
  ) {
    this.uploader = new FileUploader({
      url: 'https://evening-anchorage-3159.herokuapp.com/api/',
      allowedMimeType: this.allowedMimeType,
      autoUpload: true,
      maxFileSize: this.maxFileSize
    });
    this.uploader.onWhenAddingFileFailed = (item, filter, options) => this.onWhenAddingFileFailed(item, filter, options);
    
    this.uploader2 = new FileUploader({
      url: 'https://evening-anchorage-3159.herokuapp.com/api/',
      allowedMimeType: this.allowedMimeType2,
      autoUpload: true,
      maxFileSize: this.maxFileSize
    });
    this.uploader2.onWhenAddingFileFailed = (item, filter, options) => this.onWhenAddingFileFailed(item, filter, options);
  }

  ngOnInit() {
    this.userModel = this.userService.userModel
    this.setForm();
    this.cd.detectChanges()
    this.staticTabs.tabs[3].disabled = true;
    this.staticTabs.tabs[4].disabled = true;
  }

  setForm(){
    this.formSaveData = this.fb.group({
      id13:  [null, Validators.compose([Validators.required,this.E02PND1009,this.E02PND1018,this.ID13,Validators.maxLength(13)])],
      name:  [null, Validators.compose([Validators.required,this.CheckCharacter])],
      branchType:   [null, Validators.compose([Validators.required])],
      branchNo: [null, Validators.compose([Validators.required])],
      month: [null, Validators.compose([Validators.required])],
      year: [null, Validators.compose([Validators.required])],
      fileName: [null, Validators.compose([])],
    })

    this.formTransferdata = this.fb.group({
      id13:  [null, Validators.compose([Validators.required,this.E02PND1009,this.E02PND1018,this.ID13,Validators.maxLength(13)])],
      name:  [null, Validators.compose([Validators.required,this.CheckCharacter])],
      branchType:   [null, Validators.compose([Validators.required])],
      branchNo: [null, Validators.compose([Validators.required])],
      month: [null, Validators.compose([Validators.required])],
      year: [null, Validators.compose([Validators.required])],
      fileName2: [null, Validators.compose([])],
    })
  }

  getFileName(){
     this.fileName = (this.uploader.queue.length > 0)? this.uploader.queue[0].file.name : '';
     this.formSaveData.controls['fileName'].setValue(this.fileName)
  }

  getFileName2(){
    this.fileName = (this.uploader2.queue.length > 0)? this.uploader2.queue[0].file.name : '';
    this.formTransferdata.controls['fileName2'].setValue(this.fileName)
 }

  onWhenAddingFileFailed(item: FileLikeObject, filter: any, options: any) {
      
    switch (filter.name) {
        case 'fileSize':
            this.resultUpload = ValidationMessageConfigTh.E02PND95007;
            this.openModal(this.templateUpload);
            break;
        case 'mimeType':
            this.resultUpload =(options.allowedMimeType[0] == "application/zip")? ValidationMessageConfigTh.E02PND1040 : ValidationMessageConfigTh.E02PND1041;
            this.openModal(this.templateUpload);
            break;
        default:
    }
  }

  openModal(templateUpload : TemplateRef<any>) {
    this.modalRef = this.modalService.show(templateUpload);
  }

  loadUserProfile(resp: any) {
    this.userProfile = resp
  }


  showModal(menu: TaxMenu) {
    this.setDataDefault();
    // this.pndFormFiling.reset();
    this.menuSelected = menu;

    switch (this.menuSelected.formCode) {

      case 'PND1':
        this.loadData(menu);
        break;
      case 'PND3':
        this.loadData(menu);
        break;
      default:
        this.loadData(menu);
        break;
    }


  }

  loadData(menu: TaxMenu) {
    menu.formCode = menu.formCode.replace('PND9091', 'PND90');
    // this.taxMenuService.getInquireFormCondition(menu.formCode, this.userService.userModel.nid).subscribe(data => {
    //   console.log('data ====> ', data);
    //   this.formData = data;
    //   // this.setData(this.formData);
    //   this.formCodeGlo = menu.formCode;
    //   this.dataIsReal = this.formData.listDraft
    //   this.setData();
    //   this.searchData();
    //   const deleteNavList = this.elRef.nativeElement.querySelectorAll('.nav-item.disabled');
    //   for (const deleteNav of deleteNavList) {
    //     if (deleteNav.children[0] && deleteNav.children[0].remove) {
    //       deleteNav.children[0].remove();
    //     }
    //   }
    //   this.modal.show();
    // });
    this.modal.show();
  }

  setData(){
    const year = [];
    const date = new Date
    const pipe = new DateLocalThPipe();
    const dateNow = pipe.transform(date);
    const startYear = 2561;
    const endYear = dateNow.substr(-4, 7).replace("/", '')
    for (let i = parseInt(endYear, 10); i >= startYear; i--) {
      year.push({ yearId: i, yearText: i })
    }
    this.listYear = year;
    this.listMonth = this.monthList.filter(function (result) {
      var month = result.id;
      return (month <= 12);
    });
  }

  onHideModal() {
    this.menuSelected = null;
    this.staticTabs.tabs[0].active = true;
    this.formSubmitAttempt = false;
  }

  onClickAdvancedSearch() {
    this.checkAdvancedSearch = !this.checkAdvancedSearch;
  }

  advancedSearch() {

  }

  onClickClear() {
    this.pndFormDraft.reset();
  }

  onClickTaxFilingOnline() {
   
  //  if ("" != this.formCodeGlo) {
  //     this.taxMenuService.getVersion(this.formCodeGlo, '', '').subscribe(res => {
  //       console.log('getVersion ', res);
  //       this.version = res.formVersion;
  //       this.navigateParam(res.programUri);
  //     });
  //   } else {
  //     this.navigateParam(this.menuSelected.path);
  //   }
    this.navigateParam('');
    this.modal.hide();
      
  }

  onClickTaxFilingOffline() {
    this.router.navigate([this.menuSelected.path], {});
    this.modal.hide();
  }

  searchData(reset: boolean = false) {
    if (reset) {
      this.pageIndex = 1;
    }
    try {
      this.taxMenuService.getInquireFormCondition(this.formCodeGlo, this.userService.userModel.nid).subscribe(data => {
        let result = data.listDraft;
        this.loading = (data.listDraft !== null) ? false : true;
        this.pndList = result.slice((this.pageIndex - 1) * this.pageSize, this.pageSize * this.pageIndex);
        this.dataTable = result.slice((this.pageIndex - 1) * this.pageSize, this.pageSize * this.pageIndex);
        let number = Object.keys(result).length;
        this.total = number;
      });
    } catch (error) { }

    setTimeout(() => {
      this.loading = false;
    }, 3000);
  }

  showModalDelete(item: VersionModel){
    console.log("draftNo:", item.draftNo)
    this.draftNo = item.draftNo;
    this.content = 'ท่านต้องการลบรายการแบบฉบับร่างนี้ ใช่หรือไม่ ?';
    this.showModalDeleteDraft = true;
   }

  removePND() {
    this.showModalDeleteDraft = false;
    this.taxMenuService.cancelTax(this.draftNo).subscribe(result => {
      console.log('result ==>',result)
        this.content = 'ลบข้อมูลแบบร่าง' +' '+ this.draftNo +' '+'รายการเรียบร้อย';
        this.showModalDeleteDraftSucess = true;
        this.searchData();
      
    })
  }

  editPND(item: VersionModel) {
    console.log("draftNo:", item);
    let params = {
      draftNo:item.draftNo,
      nid: this.userService.userModel.nid,
      tin: this.userService.userModel.tin,
      branchNo: item.branchNo,
      branchType: item.branchType,
      taxYear: item.taxYear,
      taxMonth: item.taxMonth,
    }
    console.log("params:", params);

    if ("" != this.formCodeGlo) {
      switch (this.menuSelected.formCode) {
        case 'PND3':
          this.router.navigate(['/tax/wht/pnd3', params], { skipLocationChange: true });
          break;

        default:
          this.navigateParam(item.uri, item)
          break;
      }
    }
    this.modal.hide();
  }

  onHidden() {
    this.showModalDeleteDraft = false;
    this.showModalDeleteDraftSucess = false;
}


  // public fileOverBase(e: any): void {
  //   this.hasBaseDropZoneOver = e;
  // }

  // public fileOverAnother(e: any): void {
  //   this.hasAnotherDropZoneOver = e;
  // }


  onSearchAdvanced() {
    this.onClickSearchAdvanced = !this.onClickSearchAdvanced;
    this.selectChoice = null;
    this.pndList = this.dataTable;
  }

  onSearch() {
    var dataSearch = this.dataTable;
    if (this.inputSearch) {
      this.pndList = dataSearch.filter(data => data.draftNo === this.prefix+this.inputSearch)
    }
    if (this.month && this.year) {
      this.pndList = dataSearch.filter(data => data.taxYear === this.year && data.taxMonth === this.month)
    }else if(this.year){
      this.pndList = (this.year  !== 'ค้นหาทั้งหมด')? dataSearch.filter(data => data.taxYear === this.year) : this.dataTable;
    }else if(this.month){
      this.pndList = (this.month !== 'ค้นหาทั้งหมด')? dataSearch.filter(data => data.taxMonth === this.month) : this.dataTable;
    }
  }

  onClearData() {
    this.inputSearch = null;
    this.month = null;
    this.year = null;
  }

  navigateParam(programUri, params?) {
    var value: TaxConditions;
    if (params) {
      func(params);
    }
    function func(change) {

      Object.keys(change).map(key => key + '=' + change[key])
    }
    value = {
    
    }

    Object.assign(value, params);
    console.log('params',value);    
    this.router.navigate(['/tax/wht/pnd1', value], { skipLocationChange: true });
    this.location.replaceState(programUri);
  }

  setDataDefault() {
    this.listBranchNoFiling = [];
    this.listBranchTypeUpload = [];
    this.listBranchNoUpload = [];
    this.liclistDraft = [];
    this.listTaxMonth = [];
    this.listTaxYear = [];
    this.listTaxMonthUpload = [];
    this.listTaxYearUpload = [];
    this.listTaxMonthCoppy = [];
    this.filingNoList = [];
    this.TaxYear = [];
  }

  checkCharacter(value){
    var re = new RegExp(/[^A-Za-zก-๙]/g)
    if (re.test(value)) {
      // this.formSaveData.controls["name"].reset();
    }
  }

  // validate field
   E02PND1009(control: AbstractControl) {
    if (control.value) {
    for (var i = 0, sum = 0; i < 12; i++) {

        sum += parseFloat(control.value.charAt(i)) * (13 - i);
    }
    if ((11 - sum % 11) % 10 != parseFloat(control.value.charAt(12))) {
        if (control.value.charAt(12)){
        return {
           E02PND1009: true
        };
    }}
  }
    return null;
  }

  // เลขประจำตัวผู้เสียภาษีอากรของผู้มีเงินได้จะต้องไม่ขึ้นต้นด้วย 0 หรือ 099 ยกเว้น 0991
  E02PND1018(control: AbstractControl){
    var re = new RegExp(/^099\d{0}[^1]/g)
    var te = new RegExp(/^0\d{0}[^9]/g)
      if(re.test(control.value)){

        return {E02PND1018: true};
      }
      if(te.test(control.value)){

        return {E02PND1018: true};
      }
      return null;
  }

  //validate id13 massage
  ID13(control: AbstractControl){
    const varlue = control.value;
      if(control.value != null && varlue.length > 0 && varlue.length < 13){
        return {id13: true};
      }
      return null;
  }

  CheckCharacter(control: AbstractControl){
    var re = new RegExp(/[^A-Za-zก-๙]/g)
      if(re.test(control.value)){
        return {invalidCharacter: true};
      }
      return null;
  }

}

export interface FieldConfig {
  name?: string;
  type?: string;
  value?: any;
  validations?: Validators[];
  children?: FieldConfig[];
}

export interface TaxConditions {
  coppyMonth?: string;
  coppyYear?: string;
  taxYear?: string;
  taxMonth?: string;
  copyFlag?;
  payDate?;
  filingNo?;
  draftNo?: string;
  formCode?: string;
  nid?;
  tin?;
  branchNo?;
  branchType?;
  version?;
}
