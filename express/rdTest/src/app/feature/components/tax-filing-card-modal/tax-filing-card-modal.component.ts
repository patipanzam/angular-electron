import { Component, OnInit, ViewChild, Input, OnDestroy } from '@angular/core';
import { TaxMenuModel, TaxMenu } from '../../../modules/tax/pages/tax-dashboard/models/tax-menu.model';
import { TaxFilingService } from '../../services/tax-filing.service';
import { UserService } from '../../../auth/user.service';
import { TaxFilingCard } from '../../../shared/models/tax-filing-card';
import { WhtTaxComponent } from "../../../feature/components/tax-filing-card-modal/wht-tax/wht-tax.component";
import { Subscription, Observable } from 'rxjs';


@Component({
    selector: 'ef-tax-filing-card-modal',
    templateUrl: './tax-filing-card-modal.component.html',
    styleUrls: ['./tax-filing-card-modal.component.scss']
})
export class TaxFilingCardModalComponent implements OnInit, OnDestroy {

    @Input('taxFilingList') taxFilingList: TaxFilingCard[];
    // ============= เพิ่มของ ภาษีหัก ณ ที่จ่าย WhtTaxComponent ============= //
    @ViewChild('wht') wht: WhtTaxComponent;
    @Input('taxMenu') taxMenu: TaxMenuModel;

    userData: any;
    filingEvent: Subscription;

    constructor(private tfService: TaxFilingService, private userService: UserService) {

    }

    ngOnInit() {
        console.log('lllll',this.taxMenu)
        this.filingEvent = this.tfService.getFilingEvent().subscribe(() => {
            this.show(this.tfService.group, this.tfService.menu);
            console.log('eeee',this.tfService.group,this.tfService.menu)
        });

    }

    ngOnDestroy() {
        this.filingEvent.unsubscribe();
    }

    show(group: TaxMenuModel, menu: TaxMenu) {
        console.log('showModal',menu)
        switch (menu.formCode) {
            // ============= เพิ่มของ ภาษีหัก ณ ที่จ่าย WhtTaxComponent ============= //
            case 'PND1':
                this.wht.showModal(menu);
                break;
            case 'PND1A':
                this.wht.showModal(menu);
                break;
            case 'PND2':
                this.wht.showModal(menu);
                break;
            case 'PND2A':
                this.wht.showModal(menu);
                break;
            case 'PND3':
                this.wht.showModal(menu);
                break;
            case 'PND3A':
                this.wht.showModal(menu);
                break;
            case 'PND53':
                this.wht.showModal(menu);
                break;
            // case 'ALI':
            //     this.wht.showModal(menu);
            //     break;
            // ============= เพิ่มของ ภาษีหัก ณ ที่จ่าย WhtTaxComponent ============= //

            default:
                // this.tfService.group = group;
                // this.tfService.menu = menu;
                // this.tfService.setFilingEvent(Math.random());
                break;
        }
    }
}
