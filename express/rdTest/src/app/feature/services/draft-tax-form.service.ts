import { Injectable } from "@angular/core";
import { DataTablePageable } from '../../shared/models/data-table-pageable';
import DataTableUtils from '../../shared/class/data-table-utils';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { DataTableResult } from '../../shared/models/data-table-result';

export type DraftSearchResult = {
    formCode?: string,
    filingTypeNameTh?: string,
    filingNo?: number,
    taxMonth?: number,
    draftRefNo?: number,
    updateDate?: string,
}

export type DraftSearchFilter = {
    branchNo?:number,
    formCode?:string,
}

@Injectable({
    providedIn: 'root'
})
export class DraftTaxFormService {
    constructor(
        private http: HttpClient,
    ){}
    getMyDrafts(_pageable: DataTablePageable, filter?:DraftSearchFilter): Promise<DataTableResult<DraftSearchResult[]>>{
        let httpParams = DataTableUtils.generateParams(_pageable);
        if (filter) for (let key in filter) {
            httpParams = httpParams.append(key, filter[key])
        }
        return this.http.get<DataTableResult<DraftSearchResult[]>>(environment.webEdgeServiceUrl + '/cit/tax/draft/myDrafts',{params: httpParams}).toPromise();
    }
}