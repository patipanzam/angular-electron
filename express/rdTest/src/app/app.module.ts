import 'reflect-metadata';
import '../polyfills';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule ,Injector} from '@angular/core';
import { FormsModule } from '@angular/forms';

import { HttpClientModule, HttpClient } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';

// NG Translate
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { ElectronService } from './providers/electron.service';

import { WebviewDirective } from './directives/webview.directive';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { SimpleNotificationsModule } from 'angular2-notifications';

import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { LangService } from './lang';
import { ServiceModule } from './shared/services/service.module';
import { SnotifyModule, SnotifyService, ToastDefaults } from 'ng-snotify';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

export let InjectorInstance: Injector;

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    WebviewDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    SharedModule,
    ServiceModule,
    AppRoutingModule,
    SimpleNotificationsModule.forRoot(),
    SnotifyModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    })
  ],
  providers: [ElectronService,LangService,
    SnotifyService,
    {
        provide: 'SnotifyToastConfig',
        useValue: ToastDefaults
    },],
  bootstrap: [AppComponent]
})
export class AppModule { 
  constructor(private injector: Injector) {
    InjectorInstance = this.injector;
  }
}
