export interface RegisterInfo {
    registerId: string;
    masterRegisterStatus: any;
    masterRegisterType: any;
    masterTypeTaxpayer: any;
    tin: string;
    referenceCode: string;
    otpRefCode: string;
    registerNo: string;
    telephoneNo: string;
    email: string;
    createDate: Date; 
    createBy: string;
    updateDate: Date;
    updateBy: string;
    faxNo: string;
    website: string;
    nid: string;
    password: string;
    registerInfoCommercial: any;
    registerInfoPerson: any;
}