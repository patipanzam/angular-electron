import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckRequestListComponent } from './check-request-list.component';

describe('CheckRequestListComponent', () => {
  let component: CheckRequestListComponent;
  let fixture: ComponentFixture<CheckRequestListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckRequestListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckRequestListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
