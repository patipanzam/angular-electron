import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VerifyRequestComponent,CheckRequestComponent,CheckRequestListComponent }  from './pages';

const routes: Routes = [
    {
        path: '',
        component: VerifyRequestComponent,
        data: {
            title: 'ตรวจสอบผลการลงทะเบียน'
        },
        children: [
            {
                path: '',
                component: CheckRequestComponent,
                pathMatch: 'full',
                data: {
                    title: 'ตรวจสอบผลการยื่นคำร้อง (ภ.อ.01/ภ.อ.09)'
                } 
            },
            {
                path: 'list/:refno',
                component: CheckRequestListComponent,
               
                data: {
                    title: 'ตรวจสอบผลการยื่นคำร้อง'
                } 
            },
        ]
        
    }
];  

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    declarations: [],
    exports: [RouterModule]
})
export class VerifyRequestRoutingModule { }
