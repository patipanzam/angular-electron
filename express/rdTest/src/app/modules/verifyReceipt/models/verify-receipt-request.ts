export interface VerifyReceiptRequest {

    receiptNo: string;
    totalAmount: number;
    nid: string;
    filingRefNo: string;
    receiptDate?: Date;
}