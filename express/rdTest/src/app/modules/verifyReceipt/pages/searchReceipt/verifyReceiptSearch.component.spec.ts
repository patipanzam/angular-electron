import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyReceiptSearchComponent } from './verifyReceiptSearch.component';

describe('TaxComponent', () => {
  let component: VerifyReceiptSearchComponent;
  let fixture: ComponentFixture<VerifyReceiptSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifyReceiptSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyReceiptSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
