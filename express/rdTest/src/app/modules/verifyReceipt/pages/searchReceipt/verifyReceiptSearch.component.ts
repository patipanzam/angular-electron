import { Component, OnInit, Input, ViewChild, ElementRef, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ReceiptService } from '../../services/receipt.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { VerifyReceiptRequest } from '../../models/verify-receipt-request';
@Component({
  selector: 'ef-verifyReceipt',
  templateUrl: './verifyReceiptSearch.component.html',
  styleUrls: ['./verifyReceiptSearch.component.scss']
})
export class VerifyReceiptSearchComponent implements OnInit {

  infoForm: FormGroup;
  nid: FormControl;
  refNo: FormControl;
  receiptDate: FormControl;
  receiptNo: FormControl;
  amount: FormControl;
  formSubmitAttempt: boolean = false;

  constructor(private modalService: BsModalService, private receiptService: ReceiptService, private route: ActivatedRoute, private fb: FormBuilder, private router: Router) {
    this.nid = this.fb.control('',Validators.required);
    this.refNo = this.fb.control('',Validators.required);
    this.receiptDate = this.fb.control('',Validators.required);
    this.receiptNo = this.fb.control('',Validators.required);
    this.amount = this.fb.control('', Validators.required);
    this.infoForm = this.fb.group({
      nid:this.nid,
      refNo:this.refNo,
      receiptDate:this.receiptDate,
      amount:this.amount,
      receiptNo:this.receiptNo
    });
  }
  ngOnInit() {
    
  }


  captchaSuccess(e){
    console.log(e);
  }
  captchaExpire(){
    console.log("expire");
  }

  request: VerifyReceiptRequest = {nid:'',filingRefNo:'',receiptDate:null,receiptNo:'',totalAmount:null};

  onSubmit(){
    this.formSubmitAttempt = true;
    if(this.infoForm.valid){
      this.request.nid = this.nid.value,
      this.request.filingRefNo=this.refNo.value,
      this.request.receiptDate=this.receiptDate.value,
      this.request.receiptNo=this.receiptNo.value,
      this.request.totalAmount=this.amount.value
      //console.log(request);
      this.receiptService.receiptLookupByData(this.request).then(resp=>{
        this.receiptService._data = resp;
        this.router.navigateByUrl('/verifyReceipt/Result');
      });
    }
  }
  close(){
    window.location.href = '/';
  }
}
