import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';
import { TaxRoutingModule } from './tax-routing.module';
import { TaxComponent } from './pages/tax-component/tax.component';
import { TaxDashboardComponent } from './pages/tax-dashboard/tax-dashboard.component';
import { InViewportModule } from '@thisissoon/angular-inviewport';
import { ScrollSpyModule } from '@thisissoon/angular-scrollspy';
import { FeatureModule } from '../../feature/feature.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FeatureModule,
    TaxRoutingModule,
    InViewportModule, 
    ScrollSpyModule.forRoot()
  ],
  declarations: [
    TaxComponent,
    TaxDashboardComponent,
  ]
})
export class TaxModule { }
