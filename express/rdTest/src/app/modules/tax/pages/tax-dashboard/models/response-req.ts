import { Response } from "./response";
import { GlobalErrors } from "./global-errors.model";

export interface ResponseReq<T> {
  globalErrors: GlobalErrors[],
  response: Response,
  data: T
}