import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../../../../../environments/environment';
import { UserService } from '../../../../../../app/auth/user.service';
import { QueryStringUtils } from '../../../../../modules/tax/shared/query-string-util';
import { ResponseReq } from '../models/response-req';

@Injectable()
export class TaxMenuService {

  constructor(private userService: UserService, private http: HttpClient) { }

  getTaxMemu(): Observable<any> {
    const _url = `${environment.profileUrl}/profile/inquiry-user-profile-taxforms`;
    const _body = {};

    return this.http.post<any>(_url, _body).pipe(
      map(response =>  response.data),
      map(response => {
        var returnResponse = [];
        response.forEach(array => {
          if (array.formGroupID === '1' && array.formGroupCode === 'PIT') {
            if (array.formCodeList.find(obj => obj.formCode === 'PND90' || obj.formCode === 'PND91')) {
              var returnResponsePit = {
                formGroupCode: array.formGroupCode,
                formGroupDesc: array.formGroupDesc,
                formGroupID: array.formGroupID,
                formCodeList: []
              };

              returnResponsePit.formCodeList.push({
                formCode: 'PND9091',
                formNameTh: 'แบบแสดงรายการภาษีเงินได้บุคคลธรรมดา สำหรับผู้มีเงินได้กรณีทั่วไป',
                formShortTh: 'ภ.ง.ด.90/91'
              });

              array.formCodeList.forEach(pitObj => {
                if (pitObj.formCode !== 'PND90' && pitObj.formCode !== 'PND91') {
                  returnResponsePit.formCodeList.push(pitObj);
                }
              });

              returnResponse.push(returnResponsePit);
            } else {
              returnResponse.push(array);
            }
          } else {
            returnResponse.push(array);
          }
        });
        return returnResponse;
      })
    );
  }

  public getTaxMemuJSON(): Observable<any> {
    return this.http.get("./assets/json/taxMenu.json")
  }

  getInquireFormCondition(formCode: string, nid: string): Observable<any> {
    const _url = `${environment.commonUrl}/common/inquire-form-condition/${formCode}/${nid}`;

    return this.http.get<any>(_url).pipe(
      map(response => {
        return response.data;
      })
    );
  }

  getVersion(formCode: string, dateFrom: string, month: string): Observable<any>{
    let dateFM = dateFrom;
    if(null != month) {
      dateFM = dateFM+month;
    }
    const _url = `${environment.commonUrl}/common/version-tax-form/?formCode=${formCode}&dateFrom=${dateFM}`;

    var data: any;
    // returnResponse = this.http.get<any>(_url);
    data = from(this.http.get<ResponseReq<any>>(_url).toPromise().then((res) => {

      return res.data;
    }).catch((err) =>{
      return err;
    }));
    return data;
    /* return this.http.get<any>(_url).pipe(
      map(response => {
        return response.data;
      })
    ); */
  }

  cancelTax(draftNo: string): Observable<any>{
    const _url = `${environment.commonUrl}/common/cancel-tax-draft/?draftNo=${draftNo}`;

    var data: any;
    data = from(this.http.get<ResponseReq<any>>(_url).toPromise().then((res) => {

    return res.data;
    }).catch((err) =>{
      return err;
    }));
    return data;
  }

  getFilingNo(nid:string, branchType:string,branchNo:string,taxYear:string, taxMonth:string): Observable<any> {
    const params = { nid : nid,branchType:branchType,branchNo:parseInt(branchNo, 10),taxYear:parseInt(taxYear, 10),taxMonth:parseInt(taxMonth, 10)}
    return this.http.post<ResponseReq<any>>(`http://10.11.2.24:8779/rd-wht-service/pnd1/v1/list-filing-no${QueryStringUtils.get(params)}`,{})
    .pipe(map(res => res.data));
  }

  inquireDraftInfo(formCode: string): Observable<any> {
    const _url = `${environment.commonUrl}/common/list-draft-info/${formCode}`;

    return this.http.get<any>(_url).pipe(
      map(response => {
        return response.data;
      })
    );
  }
}
