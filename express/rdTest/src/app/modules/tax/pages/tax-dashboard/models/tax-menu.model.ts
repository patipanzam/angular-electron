export interface TaxMenuModel {
    formGroupID: string;
    formGroupCode: string;
    formGroupDesc: string;
    formCodeList: TaxMenu[];
}

export class TaxMenu {
    formCode: string;
    formNameTh: string;
    formShortTh: string;
    path: string;
    taxMenuObject: TaxMenuObject;
}

export class TaxMenuObject {
    formShort: string;
    formName: string;
    listBranchType?: null;
    listTaxYear?: (ListTaxYearEntity)[] | null;
    listTaxMonth?: (ListTaxMonthEntity)[] | null;
    listDraft?: null;
}

export class ListTaxYearEntity {
    startTaxYear: number;
    endTaxYear: number;
}

export class ListTaxMonthEntity {
    startTaxMonth: number;
    endTaxMonth: number;
}
