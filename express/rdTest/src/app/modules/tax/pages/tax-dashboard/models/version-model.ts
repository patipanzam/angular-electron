export interface VersionModel {

    branchNo: string,
    branchType: string,
    calMethod: string,
    companyStatus: string,
    draftNo: string,
    endDate: string,
    filingNo: string,
    filingType: string,
    startDate: string,
    taxMonth: string,
    taxYear: string,
    updateDate: string,
    uri: string,
    formShort: string,
    formName: string

}

export function getVersionModelDefault() {
    return {
        branchNo: undefined,
        branchType: undefined,
        calMethod: undefined,
        companyStatus: undefined,
        draftNo: undefined,
        endDate: undefined,
        filingNo: undefined,
        filingType: undefined,
        startDate: undefined,
        taxMonth: undefined,
        taxYear: undefined,
        updateDate: undefined,
        uri: undefined,
        formShort: undefined,
        formName: undefined
    }
  }