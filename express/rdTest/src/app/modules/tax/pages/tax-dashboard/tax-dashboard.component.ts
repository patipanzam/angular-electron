import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { TaxComponent } from '../tax-component/tax.component';
import { TaxMenu, TaxMenuModel } from './models/tax-menu.model';
import { DOCUMENT } from '@angular/platform-browser';
import { TaxFilingService } from '../../../../feature/services/tax-filing.service';

@Component({
    selector: 'ef-tax-dashboard',
    templateUrl: './tax-dashboard.component.html',
    styleUrls: ['./tax-dashboard.component.scss']
})
export class TaxDashboardComponent implements OnInit {

    taxMenu: TaxMenuModel;
    userData: any;

    constructor(private routerActive: ActivatedRoute, private taxComponent: TaxComponent, private tfService: TaxFilingService) {
        this.taxMenu  = this.routerActive.snapshot.data['resolveData'].taxMenu;
        this.userData = undefined//this.routerActive.snapshot.data['userData'].userModel;
        console.log('TaxDashboardComponent',this.taxMenu);
        
    }

    ngOnInit() {
        // this.taxComponent.loadUserProfile(this.userData);
        // this.tfService.getFilingEvent().subscribe(() => {
        //     // event fire
        //     this.onSelectedMenu(this.tfService.group, this.tfService.menu);
        // });
    }

    onSelectedMenu(group: TaxMenuModel, menu: TaxMenu) {
        console.log('uuuu')
        menu.path = `/tax/${group.formGroupCode.toLowerCase()}/${menu.formCode.toLowerCase()}`
        this.taxComponent.showModal(menu);
    }

    scrollTo(section) {
        let pos = document.querySelector('#' + section).getBoundingClientRect().top + window.scrollY;
        pos = pos - 230;
        window.scrollTo({ top: pos, behavior: 'smooth' })
    }
}
