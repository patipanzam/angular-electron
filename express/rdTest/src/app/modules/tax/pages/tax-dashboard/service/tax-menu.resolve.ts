import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';

import { TaxMenuService } from './tax-menu.service';

@Injectable()
export class TaxMenuResolve implements Resolve<any> {

  constructor(private taxMenuService: TaxMenuService) { }

  resolve(route: ActivatedRouteSnapshot): Observable<any> {
    const taxMenu = this.taxMenuService.getTaxMemuJSON();
    console.log('oooooo',taxMenu)
    return forkJoin(taxMenu).pipe(map((res) => {
      return {
        taxMenu: res[0]
      };
    }));
  }
}
