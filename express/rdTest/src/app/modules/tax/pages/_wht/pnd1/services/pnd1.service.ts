import { Injectable ,ViewChild} from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

import * as Config from '../../../../../../config/constants';
import { HttpOptionsNoToken } from '../../../../../../shared/class/http-options-no-token';
import { map } from 'rxjs/operators';
import { ResponseReq } from '../../../../../../shared/models/response-req';
import { TitleName } from '../models/pnd1.model';
import { QueryStringUtils } from '../../../../../../modules/tax/shared/query-string-util';
import { environment } from '../../../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class Pnd1Service {

  //TypeTitle example
  private xType : number = 1;
  private version;

  constructor(private http: HttpClient) { }

  public getJSONAttach(): Observable<any> {
    return this.http.get("./assets/json/PND1_Attach-18012562.json")
  }

  public getJSONComplete(): Observable<any> {
    return this.http.get("./assets/json/PND1_Complete.json")
  }

  getTitleNames(): Observable<TitleName[]> {
    return this.http.get<ResponseReq<TitleName[]>>("./assets/json/titleName.json").pipe(map(resp=>resp.data));
  }

  getFormPnd1(params : {formCode?:string , nid?:string},version): Observable<any> {
    console.log('formversion',version)
    this.version = version;
    return this.http.post<ResponseReq<any>>(`http://10.11.2.24:8779/rd-wht-service/pnd1/${this.version}/data-tax-form-pnd1`,params)
    .pipe(map(res => res.data));
  }

  saveDraftPnd1(draft : object): Observable<any> {
    return this.http.post<ResponseReq<any>>(`http://10.11.2.24:8779/rd-wht-service/pnd1/${this.version}/record-tax-draft-pnd1`, draft)
    .pipe(map(res => res.data));
  }

  printPdf(type:string,pnd1Attach : object): Observable<any>{
    const params = { waterMark : type, language :'T'}
    return this.http.post(`http://10.11.2.24:8779/rd-wht-service/pnd1/${this.version}/print-attach-pnd1${QueryStringUtils.get(params)}`, pnd1Attach , { responseType : 'blob' as 'json'});
  }

  previewPdf(type:string,json : object): Observable<any>{
     const params = { waterMark : type, language :'T'}
    return this.http.post(`http://10.11.2.24:8779/rd-wht-service/pnd1/${this.version}/print-form-pnd1${QueryStringUtils.get(params)}`, json , { responseType: "arraybuffer" });
  }

  getDueDate(calTaxPND1 : object): Observable<any> {
    return this.http.post<ResponseReq<any>>(`http://10.11.2.24:8779/rd-wht-service/pnd1/${this.version}/calculate-tax-pnd1`, calTaxPND1)
    .pipe(map(res => res.data));
  }

  recordFormPnd1(recordForm : object): Observable<any> {
    return this.http.post<ResponseReq<any>>(`http://10.11.2.24:8779/rd-wht-service/pnd1/${this.version}/record-tax-form-pnd1`, recordForm)
    .pipe(map(res => res.data));
  }

  resultFormPnd1(recordForm : string): Observable<any> {
    const params = { refNo : recordForm}
    return this.http.post<ResponseReq<any>>(`http://10.11.2.24:8779/rd-wht-service/pnd1/${this.version}/result-tax-form-pnd1${QueryStringUtils.get(params)}`,{})
    .pipe(map(res => res.data));
  }

  sendPaymentInfo(refNo : any) {
    const params = { refNo : refNo }
    return this.http.get<ResponseReq<any>>(`${environment.commonUrl}/common/send-payment-info${QueryStringUtils.get(params)}`)
    .pipe(map(res => res.data));
  }
}
