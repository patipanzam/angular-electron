import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, FormBuilder ,Validators} from '@angular/forms';
import { Pnd1CustomValidator } from '../../../models/pnd1-custom-validator';

@Component({
  selector: 'pnd1-v1-step1',
  templateUrl: './pnd1-v1-step1.component.html',
  styleUrls: ['./pnd1-v1-step1.component.scss']
})
export class Pnd1V1Step1Component implements OnInit {
  @Input('formPnd1') formPnd1?: FormGroup;
  @Input('listFilingNo') listFilingNo?: any;
  @Input('filingNo') filingNo?: any;
  

  step1Form: FormGroup;
  formSubmitAttempt: Boolean;
  monthDisplay;
  massage_error:string = 'ครั้งที่ที่ต้องการยื่นแบบจะต้องมีข้อมูล';

  year1: FormControl;
  year1Selected: any;
  year1List = [
    {
      year1Value: '2560',
      year1Text: '2560'
    }, {
      year1Value: '2561',
      year1Text: '2561'
    }, {
      year1Value: '2562',
      year1Text: '2562'
    }]

    monthList = [
      { id: 1, code: '01', nameLocal: 'มกราคม' },
      { id: 2, code: '02', nameLocal: 'กุมภาพันธ์'},
      { id: 3, code: '03', nameLocal: 'มีนาคม'  },
      { id: 4, code: '04', nameLocal: 'เมษายน' },
      { id: 5, code: '05', nameLocal: 'พฤษภาคม'},
      { id: 6, code: '06', nameLocal: 'มิถุนายน' },
      { id: 7, code: '07', nameLocal: 'กรกฎาคม'},
      { id: 8, code: '08', nameLocal: 'สิงหาคม' },
      { id: 9, code: '09', nameLocal: 'กันยายน' },
      { id: 10, code: '10', nameLocal: 'ตุลาคม' },
      { id: 11, code: '11', nameLocal: 'พฤศจิกายน'},
      { id: 12, code: '12', nameLocal: 'ธันวาคม'},
    ];

  status1List = [
    {
      status1Value: '1',
      status1Text: 'เพิ่มเติม ครั้งที่ 1'
    }, {
      status1Value: '2',
      status1Text: 'เพิ่มเติม ครั้งที่ 2'
    }, {
      status1Value: '3',
      status1Text: 'เพิ่มเติม ครั้งที่ 3'
    }]

  constructor(private fb: FormBuilder) {
    this.step1Form = this.fb.group({
      year1: this.year1
    });
  }

  ngOnInit() {
    console.log(this.formPnd1.value.pnd1AttachForm);
    console.log(this.formPnd1.value.pnd1CompleteForm);
    var month = this.formPnd1.value.pnd1CompleteForm.formDetail.taxDetail.month;
    var monthName = this.monthList.filter(data => data.id === month)
    this.monthDisplay = (monthName.length > 0)?monthName[0]:{id: 0, code: "00", nameLocal: ""}
    if(this.formPnd1.get('pnd1CompleteForm.formDetail.taxDetail.filingType').value === 1){
      this.setValidate();
    }
  }

  setValidate(){
    this.formPnd1.get('pnd1CompleteForm.formDetail.taxDetail.filingNo').setValidators([Validators.required,Pnd1CustomValidator.E02PND1007(this.listFilingNo)]);
    this.formPnd1.get('pnd1CompleteForm.formDetail.taxDetail.filingNo').updateValueAndValidity();
  }

  FilingNo(){
    this.filingNo = 0;
  }

  onRefreshData() { }

  step1FormSubmit() {}

}
