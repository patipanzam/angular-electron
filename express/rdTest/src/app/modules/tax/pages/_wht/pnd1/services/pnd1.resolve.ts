import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';

import { Pnd1Service } from './pnd1.service';

@Injectable()
export class Pnd1Resolve implements Resolve<any> {

  public version;

  constructor(private pnd1Service: Pnd1Service) { }

  resolve(route: ActivatedRouteSnapshot): Observable<any> {
    const jsonAttach = this.pnd1Service.getJSONAttach();
    const jsonComplete = this.pnd1Service.getJSONComplete();
    this.version = 'v'+route.paramMap.get('version');

    var nid      = route.paramMap.get('nid');
    var tin      = route.paramMap.get('tin');
    var branchNo = parseInt(route.paramMap.get('branchNo'), 10);
    var branchType = route.paramMap.get('branchType');
    var year     = route.paramMap.get('taxYear');
    var month    = route.paramMap.get('taxMonth');
    var copyFlag = route.paramMap.get('copyFlag');
    var taxYear  = route.paramMap.get('coppyYear');
    var taxMonth = route.paramMap.get('coppyMonth');
    var filingNo = route.paramMap.get('filingNo');
    var payDate  = route.paramMap.get('payDate');
    var yearInt  = parseInt(year, 10);
    var monthInt = parseInt(month, 10);
    var taxYearInt = parseInt(taxYear, 10);
    var taxMonthInt = parseInt(taxMonth, 10);
    var draftNo = (route.paramMap.get('draftNo') != undefined)?route.paramMap.get('draftNo'):'';
    const params = {
      nid: nid,
      tin: tin,
      draftNo: draftNo,
      formCondition:{
        taxYear:yearInt,
        taxMonth:monthInt,
        copyFlag:copyFlag,
        branchNo: branchNo,
        branchType: branchType,
        copyCondition:{
          taxYear:taxYearInt,
          taxMonth:taxMonthInt,
          filingNo:filingNo,
          payDate:payDate,
        }
      }
    }
    console.log('===>>??',params)
    console.log('===>>xxx',route)
    const formPnd1 = this.pnd1Service.getFormPnd1(params,this.version);
    return forkJoin(jsonComplete,jsonAttach).pipe(map((res) => {
      return {
        formPnd1: res[0],
        jsonAttach: res[1]
      };
    }));
  }
}
