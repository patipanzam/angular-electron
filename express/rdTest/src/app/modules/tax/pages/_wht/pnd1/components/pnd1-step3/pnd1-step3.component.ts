import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'pnd1-step3',
  templateUrl: './pnd1-step3.component.html',
  styleUrls: ['./pnd1-step3.component.scss']
})
export class Pnd1Step3Component implements OnInit {
  @Input('formPnd1') formPnd1: FormGroup;
  @Input('pdfSrc') pdfSrc : any[];
  @Input('calculateRecordForm') calculateRecordForm?: any;
 
  dataSet: Array<{
    Name: string;
    Amount: number;
    TotalIncome: number;
    TotalTax: number;
  }> = [];
  msgStatus: string;
  viewPdf: boolean = true;
  page: number;
  pdfZoom: number = 1;
  totalPages: number;
  isLoaded: boolean = false;

  constructor() { }

  ngOnInit() {
   this.filterSummary();
   this.filingType();
 }

  filter(type){
    var result = this.formPnd1.get('pnd1CompleteForm.formDetail.taxDetail').value.detail.filter(x => x.type == type);
    var Detail = [
      {
        "type": "",
        "total": "",
        "income": "",
        "tax": ""
      }]
     return (result.length > 0)?result:Detail;
   }
 
  filterSummary(){
  // var result = this.formPnd1.get('pnd1CompleteForm.formDetail.taxDetail').value.detail;
  // var Total  = result.map(item => Number(item.total)).reduce((a, b) => a + b);
  // var Income = result.map(item => Number(item.income)).reduce((a, b) => a + b);
  // var Tax    = result.map(item => Number(item.tax)).reduce((a, b) => a + b);
  // var Summary = [
  //   {
  //     "Total": Total,
  //     "Income":Income,
  //     "Tax": Tax,
  //   }]
  //  return Summary;
 }

 afterLoadComplete(pdfData: any) {
  this.totalPages = pdfData.numPages;
  this.isLoaded = true;
}

nextPage() {
  this.page++;
}

zoom_in() {
  this.pdfZoom = this.pdfZoom + 0.25;
}

zoom_out() {
  if (this.pdfZoom > 1) {
     this.pdfZoom = this.pdfZoom - 0.25;
  }
}

prevPage() {
  this.page--;
}

filingType() {
  let x = this.formPnd1.value.pnd1CompleteForm.formDetail.taxDetail.filingType
  if (x == 0) {
    this.msgStatus = 'ยื่นปกติ';
  } else {
    this.msgStatus = 'ยื่นเพิ่มเติม ครั้งที่' + " " + this.formPnd1.value.pnd1CompleteForm.formDetail.taxDetail.filingNo;
  }
}

genPdfForm() {
  this.viewPdf = !this.viewPdf;
}

}

