import { Component, OnInit, Input, TemplateRef, ViewChild, ElementRef } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FileUploader, FileLikeObject } from 'ng2-file-upload/ng2-file-upload';
import { ValidationMessageConfig } from '../../../../../../../config/validation-message';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';

@Component({
  selector: 'ef-pnd1-step4',
  templateUrl: './pnd1-step4.component.html',
  styleUrls: ['./pnd1-step4.component.scss']
})
export class Pnd1Step4Component implements OnInit {

  @Input('formPnd1') formPnd1?: FormGroup;
  @Input('resultRecordForm') resultRecordForm?: any;
  @ViewChild('templateUpload') templateUpload : any;
  
  modalRef : BsModalRef;
  resultUpload : string;
  ref_no : string;
  allowedMimeType = ['image/png', 'image/gif' , 'image/jpeg' , 'image/bmp' , 'application/pdf',]
  maxFileSize = 2 * 1024 * 1024;
  uploader: FileUploader;
  uploader2: FileUploader;

  constructor(private modalService : BsModalService) {
    this.uploader = new FileUploader({
      url: 'https://evening-anchorage-3159.herokuapp.com/api/',
      allowedMimeType: this.allowedMimeType,
      autoUpload: true,
      maxFileSize: this.maxFileSize
    });
    this.uploader.onWhenAddingFileFailed = (item, filter, options) => this.onWhenAddingFileFailed(item, filter, options);

    this.uploader2 = new FileUploader({
      url: 'https://evening-anchorage-3159.herokuapp.com/api/',
      allowedMimeType: this.allowedMimeType,
      autoUpload: true,
      maxFileSize: this.maxFileSize
    });
    this.uploader2.onWhenAddingFileFailed = (item, filter, options) => this.onWhenAddingFileFailed(item, filter, options);
  }

  ngOnInit() {
  }

  onWhenAddingFileFailed(item: FileLikeObject, filter: any, options: any) {
    switch (filter.name) {
        case 'fileSize':
            // this.resultUpload = ValidationMessageConfig.E02PND95007;
            this.openModal(this.templateUpload);
            console.log("filesize error");
            break;
        case 'mimeType':
            // this.resultUpload = ValidationMessageConfig.E02PND95006;
            this.openModal(this.templateUpload);
            console.log("type error");
            break;
        default:
            console.log("error not found")
    }
  }

  openModal(templateUpload : TemplateRef<any>) {
    this.modalRef = this.modalService.show(templateUpload);
  }
}
