import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabsModule } from 'ngx-bootstrap';

import { SharedModule } from '../../../../shared/shared.module';
import { WhtRoutingModule } from './wht-routing.module';
import { TaxBreadcrumbModule } from '../tax-breadcrumb/tax-breadcrumb.module';

import { Pnd1Component } from './pnd1/pnd1.component';
import { Pnd1Step1Component } from './pnd1/components/pnd1-step1/pnd1-step1.component';
import { Pnd1Step2Component } from './pnd1/components/pnd1-step2/pnd1-step2.component';
import { Pnd1Step3Component } from './pnd1/components/pnd1-step3/pnd1-step3.component';
import { Pnd1Step4Component } from './pnd1/components/pnd1-step4/pnd1-step4.component';

//pnd1 version1
import { Pnd1V1Component } from './pnd1/v1/pnd1-v1.component';
import { Pnd1V1Step1Component } from './pnd1/v1/components/pnd1-v1-step1/pnd1-v1-step1.component';
import { Pnd1V1Step2Component } from './pnd1/v1/components/pnd1-v1-step2/pnd1-v1-step2.component';
import { Pnd1V1Step3Component } from './pnd1/v1/components/pnd1-v1-step3/pnd1-v1-step3.component';




@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    TaxBreadcrumbModule,
    WhtRoutingModule,
    TabsModule
  ],
  declarations: [
    Pnd1Component,
    Pnd1Step1Component,
    Pnd1Step2Component,
    Pnd1Step3Component,
    Pnd1Step4Component,

    Pnd1V1Component,
    Pnd1V1Step1Component,
    Pnd1V1Step2Component,
    Pnd1V1Step3Component,
    
  ]
})
export class WhtModule { }
