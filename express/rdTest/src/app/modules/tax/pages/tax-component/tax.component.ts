import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { ModalDirective, TabsetComponent } from 'ngx-bootstrap';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { TaxMenu } from '../tax-dashboard/models/tax-menu.model';
import { TaxMenuService } from '../tax-dashboard/service/tax-menu.service';
import { UserService, UserModel } from '../../../../auth/user.service';
import { VersionModel } from '../../../../modules/tax/pages/tax-dashboard/models/version-model';

@Component({
  selector: 'ef-tax',
  templateUrl: './tax.component.html',
  styleUrls: ['./tax.component.scss']
})

export class TaxComponent implements OnInit {
  @ViewChild(ModalDirective) modal: ModalDirective;
  @ViewChild('childModal') childModal: ModalDirective;
  @ViewChild('staticTabs') staticTabs: TabsetComponent;
  @ViewChild('payDateEle') payDateEle: ElementRef;
  menuSelected: TaxMenu;
  formData: any;
  pndList: any = [];
  dataIsReal: any;
  pndListLoading: boolean;
  pageIndex = 1;
  pageSize = 5;
  total = 1;
  loading = true;

  public checkType: boolean;
  public checkBranch: boolean;
  public checkMonth: boolean;
  public checkCoppyDraft: boolean = false;
  public coppyDraft : boolean;
  public checkAdvancedSearch:boolean;
  public checkTaCode:boolean;
  public onClickSearchAdvanced:boolean;
  public checkFlag:boolean;
  public currentDate;

  public formCodeGlo: string;
  public formSubmitAttempt: boolean;

  public typeFileConFig = ".xml,.zip"

  chkChecked: boolean = false;
  chkCheckedSup: boolean = false;
  chkCheckedDefault: boolean = false;
  chkAdvanced: boolean = false;

  mookupDataDraft = [
    { Id13:'157040099474',titleName:'นาย',firstName:'รวบรวม',lastName: 'ร่วมมือ'},
    { Id13:'355509850588',titleName:'นาง',firstName:'ดำรง',lastName: 'คงยืน'},
    { Id13:'099276882643',titleName:'นางสาว',firstName:'กรรกนก',lastName: 'ชื่นบาน'},
  ];
  
  monthList = [
    { id: 1, code: '01', nameLocal: 'มกราคม' },
    { id: 2, code: '02', nameLocal: 'กุมภาพันธ์'},
    { id: 3, code: '03', nameLocal: 'มีนาคม'  },
    { id: 4, code: '04', nameLocal: 'เมษายน' },
    { id: 5, code: '05', nameLocal: 'พฤษภาคม'},
    { id: 6, code: '06', nameLocal: 'มิถุนายน' },
    { id: 7, code: '07', nameLocal: 'กรกฎาคม'},
    { id: 8, code: '08', nameLocal: 'สิงหาคม' },
    { id: 9, code: '09', nameLocal: 'กันยายน' },
    { id: 10, code: '10', nameLocal: 'ตุลาคม' },
    { id: 11, code: '11', nameLocal: 'พฤศจิกายน'},
    { id: 12, code: '12', nameLocal: 'ธันวาคม'},
  ];

  
  uploader:FileUploader = new FileUploader({url: 'https://evening-anchorage-3159.herokuapp.com/api/'});

  public hasBaseDropZoneOver:boolean = false;
  public hasAnotherDropZoneOver:boolean = false;
  userModel: any = {};
  userProfile: any = {};
  dataPND: any;

  
 public listBranchType =[];
 public listBranchNo = [];
 public liclistDraft = [];
 public listTaxMonth = [];
 public listTaxYear =  [];
 public listTaxMonthCoppy =[];
 public filingNoList =[];
 public TaxYear = [];

  private pndFormFiling: FormGroup = this.fb.group({
      branchType: this.fb.control(null)
    , branchNo: this.fb.control(null)
    , year: this.fb.control(null, Validators.required)
    , month: this.fb.control(null)
  });
  
  private pndFormFilingCoppy: FormGroup = this.fb.group({
    copyFlag:this.fb.control(null)
  ,year: this.fb.control(null)
  , month: this.fb.control(null)
  , filingNo: this.fb.control(null)
  , payDate: this.fb.control(null)
  });

  private pndFormUpload: FormGroup = this.fb.group({
      branchType: this.fb.control(null)
    , branchNo: this.fb.control(null)
    , year: this.fb.control(null, Validators.required)
    , month: this.fb.control(null)
  });

  private pndFormDraft: FormGroup = this.fb.group({
    year: this.fb.control(null, Validators.required)
  });

  constructor(
    private router: Router,
    private elRef: ElementRef,
    private taxMenuService: TaxMenuService,
    private userService: UserService,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private routerActive: ActivatedRoute,
    private location: Location,
  ) {
  
    this.checkTaCode = (this.userService.userModel.taCode === "")? true : false;
    // console.log(this.userService.userModel)
  }

  ngOnInit() {
    this.userModel = this.userService.userModel
    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
         console.log('ImageUpload:uploaded:', item, status, response);
        //  alert('File uploaded successfully');
        //  this.onClickTaxFilingOnline();
     };
     this.cd.detectChanges()
     if(this.checkTaCode === true){
      this.staticTabs.tabs[3].disabled = true;
      this.staticTabs.tabs[4].disabled = true;
    }
  }

  loadUserProfile(resp: any) {
    this.userProfile = resp
    if(this.userProfile.taxpayerNameInfo.sbtFlag !== "" && this.userProfile.taxpayerNameInfo.sbtFlag === "Y" || this.userProfile.taxpayerNameInfo.vatFlag !== "" && this.userProfile.taxpayerNameInfo.vatFlag === "Y" || this.userProfile.taxpayerNameInfo.whtFlag !== "" && this.userProfile.taxpayerNameInfo.whtFlag !== "Y"){
      this.checkFlag = true
      this.pndFormFiling.controls["branchType"].setValidators([Validators.required]);
      this.pndFormFiling.controls["branchNo"].setValidators([Validators.required]);
    }else{
      this.checkFlag = false
      this.pndFormFiling.controls['branchType'].clearValidators()
      this.pndFormFiling.controls['branchNo'].clearValidators()
      this.pndFormFiling.controls['branchType'].updateValueAndValidity()
      this.pndFormFiling.controls['branchNo'].updateValueAndValidity()
    }
  }
  

  showModal(menu: TaxMenu) {
    this.pndFormFiling.reset();
    this.menuSelected = menu;
    var re = new RegExp(/PND9/g)
      if(re.test(menu.formCode)){
       this.checkType   = false;
       this.checkBranch = false;
       this.checkMonth  = false;
       this.coppyDraft  = false;
       this.pndFormFiling.controls['month'].clearValidators()
       this.pndFormFiling.controls['month'].updateValueAndValidity()
      }else{
        if (this.checkFlag === true) {
          this.checkType = true;
          this.checkBranch = true;
        }
        this.checkMonth = true;
        this.coppyDraft = true;
        this.pndFormFiling.controls["month"].setValidators([Validators.required]);
      }
    menu.formCode = menu.formCode.replace('PND9091', 'PND90');
    this.taxMenuService.getInquireFormCondition(menu.formCode,this.userService.userModel.nid).subscribe(data => {
      console.log('data ====> ', data);
      this.formData = data;
      // this.currentDate = data.currentDate
      this.setData(this.formData);
      this.formCodeGlo = menu.formCode;
      this.dataIsReal = this.formData.listDraft
      this.searchData();
      const deleteNavList = this.elRef.nativeElement.querySelectorAll('.nav-item.disabled');
      for (const deleteNav of deleteNavList) {
        if (deleteNav.children[0] && deleteNav.children[0].remove) {
          deleteNav.children[0].remove();
        }
      }
      this.modal.show();
    });
  }

  

  setData(formData){
    this.listBranchType = this.formData.listBranchType;
    for(let i = formData.listTaxYear[0].endTaxYear; i >= formData.listTaxYear[0].startTaxYear; i--) {

       this.TaxYear.push({yearId: i, yearText: i})    
    }
    this.listTaxYear = this.TaxYear;
  }

  getBranchNo(){
    const branchType = this.pndFormFiling.controls["branchType"].value;
    const listBranchNo = this.listBranchType.filter(data => data.branchType === branchType);
    this.listBranchNo = listBranchNo[0].listBranchNoObject;
     
  }

  getMonth(){
    const year = this.pndFormFiling.controls["year"].value;
    const startMonth = (this.formData.listTaxMonth.length > 0)? this.formData.listTaxMonth[0].startTaxMonth : 0 ;
    const endMonth   = (this.formData.listTaxMonth.length > 0)? this.formData.listTaxMonth[0].endTaxMonth : 0 ;
    const startYear = (this.formData.listTaxYear.length > 0)? this.formData.listTaxYear[0].startTaxYear : 0 ;
    const endYear   = (this.formData.listTaxYear.length > 0)? this.formData.listTaxYear[0].endTaxYear : 0 ;
    if (year) {
      if (year === startYear) {
        this.listTaxMonth = this.monthList.filter(function (result) {
          var month = result.id;
          return (month >= startMonth && month <= 12);
        });
      } else if (year === endYear) {
        this.listTaxMonth = this.monthList.filter(function (result) {
          var month = result.id;
          return (month <= 12 && month <= endMonth);
        });
      } else {
        this.listTaxMonth = this.monthList;
      }
    }
  }

  getMonthF() {
    const year = this.pndFormFilingCoppy.controls["year"].value;
    const startMonth = (this.formData.listTaxMonth.length > 0) ? this.formData.listTaxMonth[0].startTaxMonth : 0;
    const endMonth = (this.formData.listTaxMonth.length > 0) ? this.formData.listTaxMonth[0].endTaxMonth : 0;
    const startYear = (this.formData.listTaxYear.length > 0) ? this.formData.listTaxYear[0].startTaxYear : 0;
    const endYear = (this.formData.listTaxYear.length > 0) ? this.formData.listTaxYear[0].endTaxYear : 0;
    if (year) {
      if (year === startYear) {
        this.listTaxMonthCoppy = this.monthList.filter(function (result) {
          var month = result.id;
          return (month >= startMonth && month <= 12);
        });
      } else if (year === endYear) {
        this.listTaxMonthCoppy = this.monthList.filter(function (result) {
          var month = result.id;
          return (month <= 12 && month <= endMonth);
        });
      } else {
        this.listTaxMonthCoppy = this.monthList;
      }
    }
  }

  onHideModal() {
    this.menuSelected = null;
    this.staticTabs.tabs[0].active = true;
    this.formSubmitAttempt = false;
  }

  onClickCoppDraft(){
    this.checkCoppyDraft = !this.checkCoppyDraft;
    if( this.checkCoppyDraft === true){
      this.pndFormFilingCoppy.controls["year"].setValidators([Validators.required]);
      this.pndFormFilingCoppy.controls["month"].setValidators([Validators.required]);
      this.pndFormFilingCoppy.controls["filingNo"].setValidators([Validators.required]);
      this.pndFormFilingCoppy.controls["payDate"].setValidators([Validators.required]);
    }else{
      this.pndFormFilingCoppy.controls['year'].clearValidators()
      this.pndFormFilingCoppy.controls['month'].clearValidators()
      this.pndFormFilingCoppy.controls['filingNo'].clearValidators()
      this.pndFormFilingCoppy.controls['payDate'].clearValidators()
      this.pndFormFilingCoppy.controls['year'].updateValueAndValidity()
      this.pndFormFilingCoppy.controls['month'].updateValueAndValidity()
      this.pndFormFilingCoppy.controls['filingNo'].updateValueAndValidity()
      this.pndFormFilingCoppy.controls['payDate'].updateValueAndValidity()
    }
  }
  onClickAdvancedSearch(){
    this.checkAdvancedSearch = !this.checkAdvancedSearch;
  }

  advancedSearch(){
    
  }
  
  onClickClear(){
    this.pndFormDraft.reset();
  }

  onClickTaxFilingOnline() {
    console.log(this.checkCoppyDraft)
    let year = this.pndFormFiling.controls["year"].value;
    let month = this.pndFormFiling.controls["month"].value;
    if (this.pndFormFiling.valid == true && this.pndFormFilingCoppy.valid == true) {
      if ("" != this.formCodeGlo) {
        this.taxMenuService.getVersion(this.formCodeGlo, year, month).subscribe(res => {
          console.log('getVersion ===>',res);
          this.navigateParam(res.programUri);
        });
      } else {
        this.navigateParam(this.menuSelected.path);
      }
      this.modal.hide();
    } else {
      this.formSubmitAttempt = true;
    }
  }

  onClickTaxFilingOffline() {
    this.router.navigate([this.menuSelected.path], {});
    this.modal.hide();
  }

  createForm(fieldArray: FieldConfig[]): FormGroup {
    let group = this.fb.group({});
    fieldArray.forEach(field => {
      if (field.type && field.type.length > 0) {
        this.createFormGroup(field, group)
      } else {
        this.createFormControl(field, group);
      }
    });
    return group;
  }

  private createFormGroup(field: FieldConfig, group: FormGroup) {
    if (field.type == 'object' && field.children && field.children.length > 0) {
      group.addControl(field.name, this.createForm(field.children));
    } else if(field.type == 'array') {
      group.addControl(field.name, this.fb.array([]));
    }
    return group;
  }

  private createFormControl(field: FieldConfig, group: FormGroup) {
    const control = this.fb.control(
      field.value,
      this.bindValidations(field.validations || [])
    );
    return group.addControl(field.name, control);
  }

  private bindValidations(validations: any) {
    if (validations.length > 0) {
      const validList = [];
      validations.forEach(valid => {
        validList.push(valid);
      });
      return Validators.compose(validList);
    }
    return null;
  }

  getFilingNo(){
     const nid = this.userModel.nid;
     const branchType = this.pndFormFiling.controls["branchType"].value;
     const branchNo = this.pndFormFiling.controls["branchNo"].value;
     const taxMonth = this.pndFormFilingCoppy.controls["month"].value;
     const taxYear = this.pndFormFilingCoppy.controls["year"].value;
     this.taxMenuService.getFilingNo(nid,branchType,branchNo,taxYear,taxMonth).subscribe(res => {
      var listFilingNo = [];
      if(res){
        res.listFilingNo.forEach(function (value, index) {
          listFilingNo.push({id:value})
        })
        this.filingNoList = listFilingNo;
      }
    });
  
  }
  

  searchData(reset: boolean = false) {
    if (reset) {
      this.pageIndex = 1;
    }
    try {
      let result = this.formData.listDraft;
      this.loading = (this.formData.listDraft !== null)? false : true ;
      this.pndList = result.slice((this.pageIndex - 1) * this.pageSize, this.pageSize * this.pageIndex);
      let number = Object.keys(result).length;
      this.total = number;
    } catch (error) { }

    setTimeout(() => {
      this.loading = false;
    }, 3000);
  }

  removePND(item: VersionModel) {
    console.log("draftNo:", item.draftNo)
    this.taxMenuService.cancelTax(item.draftNo);
  }

  editPND(item: VersionModel) {
    console.log("draftNo:", item.draftNo);
    if("" != this.formCodeGlo){
      // this.router.navigate([item.uri, { draftNo: item.draftNo , yearDraft: item.taxYear}] , { skipLocationChange : true});
      // this.navigateParam(item.uri, item);
      this.router.navigate([item.uri ,item],{ skipLocationChange : true});
      this.location.replaceState(item.uri);
    }
    this.modal.hide();
  }


  public fileOverBase(e:any):void {
    this.hasBaseDropZoneOver = e;
  }

  public fileOverAnother(e:any):void {
    this.hasAnotherDropZoneOver = e;
  }

  
  onSearchAdvanced() {
    this.onClickSearchAdvanced = !this.onClickSearchAdvanced;
  }

  onSearch(){}

  onClearData(){}

  onClickFiling(){
    this.checkTaCode = true;
  }
  navigateParam(programUri, params?){
    this.modal.hide();
    var value: TaxConditions;
    if (params) {
      func(params);
    }
    function func(change) {
  
      Object.keys(change).map(key => key + '=' + change[key])
    }
    var flag = this.pndFormFilingCoppy.controls["copyFlag"].value;
    var copyFlag = (flag === true)? 'Y' : 'N';
    value = {
      // formCode:this.formCodeGlo,
      nid:this.userService.userModel.nid,
      tin:this.userService.userModel.tin,
      branchNo: this.pndFormFiling.controls["branchNo"].value,
      branchType: this.pndFormFiling.controls["branchType"].value,
      month: this.pndFormFiling.controls["month"].value,
      year: this.pndFormFiling.controls["year"].value,
      taxYear: this.pndFormFilingCoppy.controls["year"].value,
      taxMonth: this.pndFormFilingCoppy.controls["month"].value,
      copyFlag: copyFlag,
      filingNo: this.pndFormFilingCoppy.controls["filingNo"].value,
      payDate: this.pndFormFilingCoppy.controls["payDate"].value,
      currentDate:this.currentDate
    }
  
    Object.assign(value,params);
    this.router.navigate([programUri ,value],{ skipLocationChange : true});
    this.location.replaceState(programUri);
  }

}

export interface FieldConfig {
  name?: string;
  type?: string;
  value?: any;
  validations?: Validators[];
  children?: FieldConfig[];
}

export interface TaxConditions {
  month?:string;
  year?:string;
  taxYear?:string;
  taxMonth?:string;
  copyFlag?;
  payDate?;
  filingNo?;
  draftNo?:string;
  formCode?:string;
  nid?;
  tin?;
  branchNo?;
  branchType?;
  currentDate?;
}
