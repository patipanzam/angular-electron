import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Province } from '../../../shared/models/province.model';
import { District } from '../../../shared/models/district.model';
import { SubDistrict } from '../../../shared/models/sub-district.model';
import { Postal } from '../../../shared/models/postal.model';
import { environment } from '../../../../environments/environment';

@Injectable({
    providedIn: 'root'
})

export class ProvinceService {
    private _province: Province[];
    private _district: District[];
    private _subDistrict: SubDistrict[];
    private _postal: Postal[];

    constructor(private http: HttpClient) { }

    public get province(): Province[] {
        return this._province;
    }

    async getAllMprovice(): Promise<Province[]> {
        if (!this._province) {
            this._province = await this.http.get<Province[]>(`${environment.masterUrl}/master/dopa/province/u`).toPromise();
        }
        return Promise.resolve(this._province);
    }

    public get district(): District[] {
        return this._district;
    }

    public get subDistrict(): SubDistrict[] {
        return this._subDistrict;
    }

    public get postal(): Postal[] {
        return this._postal;
    }

    async getDistrict(_provinceCode: any): Promise<District[]> {
        this._district = await this.http.get<District[]>(`${environment.masterUrl}/master/dopa/province/` + _provinceCode + `/districtbyprovinceCode/u`).toPromise();
        return Promise.resolve(this._district);
    }


    async getSubDistrict(_districtCode: any): Promise<SubDistrict[]> {
        this._subDistrict = await this.http.get<SubDistrict[]>(`${environment.masterUrl}/master/dopa/district/` + _districtCode + `/subdistrictbydistrictCode/u`).toPromise();
        return Promise.resolve(this._subDistrict);
    }

    async getZipCode(_subdistrictCode: any): Promise<Postal[]> {
        let subdistrictCode =  (_subdistrictCode!=null)?_subdistrictCode:0;
        this._postal = await this.http.get<Postal[]>(`${environment.masterUrl}/master/dopa/subdistrict/` + subdistrictCode + `/postalbysubdistrictCode/u`).toPromise();
        return Promise.resolve(this._postal);
    }

    findProvinceId(_code: number): Province {
        return this._province.find(p => p.provinceCode === _code);
    }

    findDistrictById(_code: number): District {
        return this._district.find(p => p.districtCode === _code);
    }

    findSubDistrictById(_code: number): SubDistrict {
        return this._subDistrict.find(p => p.subDistrictCode === _code);
    }

    findZipCodeById(_code: number): Postal {
        return this._postal.find(p => p.postalId === _code);
    }
}
