import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'currency'
})
export class CurrencyPipe implements PipeTransform {

    transform(value: any): string {
        if (value) {
            let num = parseFloat(value.valueOf());
            return (num).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
        }
        return null;
    }

}
