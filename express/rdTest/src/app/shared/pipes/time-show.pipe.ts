import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'timeShow'
})
export class TimeShowPipe implements PipeTransform {

  transform(value: any, format?: string): string {
    if (value) {
      let time = (format) ? moment(value, format) : moment(value);
      return time.format('HH:mm');
    }
    return null;
  }

}
