import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
import { LOCAL_LANG_KEY } from '../../config/constants';

@Pipe({
  name: 'dateTimeShow'
})
export class DateTimeShowPipe implements PipeTransform {

    constructor() {

    }

    transform(value: any, targetFormat: string = 'DD/MM/YYYY', format?: string): string {

        if (value) {
            let lang = localStorage.getItem(LOCAL_LANG_KEY);
            let date = (format) ? moment(value, format).locale(lang) : moment(value).locale(lang);
            let output: string;

            if (targetFormat && lang == 'th') {
                targetFormat = targetFormat.replace(/YYYY/g, 'BBBB');
                targetFormat = targetFormat.replace(/YY/g, 'BB');
            }

            if (targetFormat) {
                output = date.format(targetFormat);
                if (lang == 'th') {
                    let year = date.toDate().getFullYear();
                    output = output.replace(/BBBB/g, (year + 543).toString());
                    output = output.replace(/BB/g, ((year + 543).toString()).substr((year + 543).toString().length -2));
                }
            }

            return output;
        }
        return null;
    }

}
