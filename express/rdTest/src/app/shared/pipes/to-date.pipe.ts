/*
date : DD, month: MM, year: YYYY 
e.g.
      pip Date str : 13/02/2019, ptt: DD/MM/YYYY
      pip Date str : 13-02-2019, ptt: DD-MM-YYYY
      pip Date str : 13022019, ptt: DDMMYYYY
      pip Date str : 20190212, ptt: YYYYMMDD
      pip Date str : 25620214, ptt: YYYYMMDD
      pip Date str : 02142562, ptt: MMDDYYYY
      pip Date str : 15/04/2562, ptt: DD/MM/YYYY
      pip Date str : 2019-03-05T12:08:56.235, ptt: YYYY-MM-DDTHH:mm:ss.SSS
*/
import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
import { formatDate } from '@angular/common';
import { isInteger } from 'ng-zorro-antd';

@Pipe({
  name: 'toDate'
})
export class ToDatePipe implements PipeTransform {

  YYYY: string = "YYYY";
  MM: string = "MM";
  DD: string = "DD";

  transform(value: any, args?: any): Date {
    if (!args) {
      args = "DD/MM/YYYY";
    }
    var toDate = undefined;

    if (value instanceof Date) {
      return value;
    } if (typeof value == "string") {
      value = this.convertDatePs(value, args);
      try{
        toDate = moment.parseZone(value, args, 'th', true).toDate();
        toDate = this.toUTC(toDate);
      }catch(error) {
        toDate = moment.parseZone(this.zeroDate(value, args), args, 'th', true).toDate();
      }
    } else {
      toDate = value;
    }


    if (("Invalid Date"==toDate)) {
    // if ((toDate instanceof Date)) {
      toDate = value;
    }

    return toDate;
  }

  zeroDate(value: string, args: string):string{
    // convert to dayOne
    var year = value.substring(args.indexOf(this.YYYY), args.indexOf(this.YYYY) + 4);
    var month = value.substring(args.indexOf(this.MM), args.indexOf(this.MM) + 2);
    var day = value.substring(args.indexOf(this.DD), args.indexOf(this.DD) + 2);

    if ("00"==month) {
      month = "01";
    }

    if ("00"==day) {
      day = "01";
    }

    var newDatePtt = args;
 
    newDatePtt = newDatePtt.replace(this.YYYY, year);
    newDatePtt = newDatePtt.replace(this.DD, day);
    newDatePtt = newDatePtt.replace(this.MM, month);
    console.log("newDatePtt:", newDatePtt);
    return newDatePtt;
  }

  convertDatePs(value: string, args: string) {
    // console.log("value:", value);
    // console.log("args:", args);
    var year = value.substring(args.indexOf(this.YYYY), args.indexOf(this.YYYY) + 4);
    // console.log("convertDatePs:", year);
    value = value.replace(year, this.utcYear(year));
    // console.log("convertDatePs:", value);
    return value;
  }

  utcYear(strYear: string): string {
    var cYear = ""
    if (this.chkPS(strYear)) {
      cYear =  (parseInt(strYear) - 543).toString();
    } else {
      cYear = strYear;
    }
    return cYear;
  }

  chkPS(yStr): boolean {
    const YEAR_PS_REGEXP = /([2][4-8]\d\d)/g;
    return YEAR_PS_REGEXP.test(yStr);
  }

  toUTC(date) {
    return new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
}

}
