import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'userName'
})
export class UserNamePipe implements PipeTransform {

  transform(val: any, args?: any): any {
    if(val){
      val = val.substr(0,1)+"-"+val.substr(1,4)+"-"+val.substr(5,5)+"-"+val.substr(10,2)+"-"+val.substr(12,1);
      return val;
    }
    return "";
 
  }

}
