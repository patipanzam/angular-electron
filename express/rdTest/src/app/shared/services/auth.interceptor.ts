import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

import { AuthService } from '../../auth/auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(public auth: AuthService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let headers = request.headers;

    if (headers.get('noToken') !== 'noToken') {
      headers = headers.set('Authorization', `Bearer ${this.auth.getToken()}`);
    } else if (request.headers.get('noToken') === 'noToken') {
      headers = headers.delete('Authorization').delete('noToken').delete('notoken');
    }

    const newReq = request.clone({ headers: headers });
    return next.handle(newReq);
  }
}
