import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';
import { GeneralModal } from '../models/general-modal';

@Injectable({
    providedIn: 'root'
})
export class GeneralModalService {

    private _modalEvent: BehaviorSubject<GeneralModal> = new BehaviorSubject<GeneralModal>(null);

    constructor() { }

    modalEvent() {
        return this._modalEvent.asObservable();
    }

    show(generalModal: GeneralModal) {
        this._modalEvent.next(generalModal);
    }

    hide() {
        this._modalEvent.next(null);
    }

}
