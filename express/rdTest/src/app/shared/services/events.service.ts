import { Injectable } from '@angular/core';
import { Subject, from, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class EventsService {
    
    listeners: Object = {};
    eventsSubject: Subject<any> = new Subject();
    events: Observable<any>;

    constructor() {
        this.events = from(this.eventsSubject);
        this.events.subscribe(({name, args}) => {
            if (this.listeners[name]) {
                for (let listener of this.listeners[name]) {
                    listener(...args);
                }
            }
        });
    }

    on(name, listener) {
        if (!this.listeners[name]) {
            this.listeners[name] = [];
        }

        this.listeners[name].push(listener);
    }

    off(name, listener) {
        this.listeners[name] = this.listeners[name].filter(x => x != listener);
    }

    broadcast(name, ...args) {
        this.eventsSubject.next({
            name,
            args
        });
    }
}
