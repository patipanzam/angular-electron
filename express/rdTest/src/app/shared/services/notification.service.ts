import { Injectable } from '@angular/core';
import polling, { IOptions } from 'rx-polling';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Notification } from '../models/notification';
import { EventsService } from './events.service';
import { Subscription } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class NotificationService {

    private _notificationNewUrl: string = environment.webEdgeServiceUrl + '/cit/notification/new';
    notificationSubscription: Subscription;

    constructor(private http: HttpClient, private eventsService: EventsService) { }

    pollingNotification() {
        const request$ = this.http.get<Notification[]>(this._notificationNewUrl, {
            headers: { ignoreLoadingBar: '' }
        });
        const options: IOptions = {
            interval: 60000,
            attempts: 9,
            backoffStrategy: 'exponential',
            exponentialUnit: 1000, // 1 second
            randomRange: [1000, 10000],
        };

        this.notificationSubscription = polling(request$, options).subscribe((data) => {
            for (let noti of data) {
                this.eventsService.broadcast(noti.event, noti);
            }
        }, (error) => {
            this.stopPollingNotification();
        });
    }

    stopPollingNotification() {
        this.notificationSubscription.unsubscribe();
    }
}
