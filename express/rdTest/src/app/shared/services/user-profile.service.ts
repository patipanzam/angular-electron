import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class UserProfileService {

	private _getUserProfileUrl: string = environment.webEdgeServiceUrl + '/user/profile';
	private _getUserProfileDetailUrl: string = environment.webEdgeServiceUrl + '/user/profile/detail';
	constructor(private http: HttpClient) { }

	getUserProfile(): Observable<any> {
		return this.http.get<any>(this._getUserProfileUrl);
	}

	getUserProfileDetail(): Observable<any> {
		return this.http.get<any>(this._getUserProfileDetailUrl);
	}
	

}
