import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, empty } from 'rxjs';
import { ConnectionService } from 'ng-connection-service';

@Injectable()
export class ConnectionServiceInterceptor implements HttpInterceptor {
  private isConnected: Boolean = true;

  constructor(private connectionService: ConnectionService) {
    this.connectionService.monitor().subscribe(isConnected => {
      this.isConnected = isConnected;
    });
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!this.isConnected && request.url.includes('http')) {
      request = request.clone({
        url: '- - url offlene - -'
      });

      console.log('ConnectionService detector offline !!');

      /*

        Process offline

      */

      return empty();
    }

    return next.handle(request);
  }
}
