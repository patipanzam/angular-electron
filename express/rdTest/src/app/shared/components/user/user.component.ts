import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../auth/auth.service';
import { UserProfileService } from '../../../shared/services/user-profile.service';

@Component({
	selector: 'ef-user',
	templateUrl: './user.component.html',
	styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

	userProfile: any = {};
	constructor(public authService: AuthService, private userProfileService: UserProfileService) {

	}

	ngOnInit() {
		// this.userProfileService.getUserProfile().subscribe((res) => {
		// 	this.userProfile = res;
		// });
	}

}
