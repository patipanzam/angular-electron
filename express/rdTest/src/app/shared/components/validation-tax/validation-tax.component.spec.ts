import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidationTaxComponent } from './validation-tax.component';

describe('ValidationTaxComponent', () => {
  let component: ValidationTaxComponent;
  let fixture: ComponentFixture<ValidationTaxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValidationTaxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidationTaxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
