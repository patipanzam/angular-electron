import { Component, OnInit, Input } from '@angular/core';
import { AbstractControl } from '../../../../../node_modules/@angular/forms';

@Component({
    selector: 'ef-label',
    templateUrl: './label.component.html',
    styleUrls: ['./label.component.scss']
})
export class LabelComponent implements OnInit {

    @Input() required: boolean = false;

    constructor() { }

    ngOnInit() {

    }

}
