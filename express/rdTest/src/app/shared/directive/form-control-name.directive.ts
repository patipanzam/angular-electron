import { Directive, HostBinding, Optional, Input } from '@angular/core';
import { ControlContainer } from '@angular/forms';
import { MaskService } from 'ngx-mask';

@Directive({
  selector: 'label[ef-formControlName], span[ef-formControlName]',
  exportAs: 'ef-formControlName'
})
export class EfFormControlNameDirective {
  @Input('ef-formControlName') efFormControlName: string;

  constructor(@Optional() private parent: ControlContainer) { }

  @HostBinding('textContent')
  get controlValue() {
    const isArray = this.efFormControlName.split(',');
    if (isArray.length > 1) {
      let value = '';
      isArray.forEach(name => {
        value += this.parent.control.get(name.trim()).value + ' ';
      });
      return value;
    } else {
      if (this.parent && this.parent.control.get(this.efFormControlName)) {
        return this.parent.control.get(this.efFormControlName).value;
      } else {
        return '';
      }
    }
  }
}

@Directive({
  selector: 'label[ef-formControlName-mask][mask], span[ef-formControlName-mask][mask]',
  exportAs: 'ef-formControlName'
})
export class EfFormControlNameWithMaskDirective {
  @Input('ef-formControlName-mask') efFormControlName: string;
  @Input('mask') mask: string;

  constructor(@Optional() private parent: ControlContainer, private maskService: MaskService) { }

  @HostBinding('textContent')
  get controlValue() {
    if (this.parent && this.parent.control.get(this.efFormControlName)) {
      if (this.mask) {
        return this.maskService.applyMask(this.parent.control.get(this.efFormControlName).value, this.mask);
      }
      return this.parent.control.get(this.efFormControlName).value;
    } else {
      return '';
    }
  }
}

// @Directive({
//   selector: 'label[ef-formControlName-pipe][pipe], span[ef-formControlName-pipe][pipe]',
//   exportAs: 'ef-formControlName'
// })
// export class EfFormControlNameWithPipeDirective {
//   @Input('ef-formControlName-pipe') efFormControlName: string;
//   @Input('pipe') pipe: string;

//   constructor(
//     @Optional() private parent: ControlContainer,
//     private vcRef: ViewContainerRef,
//     private compiler: Compiler
//   ) { }

//   ngAfterViewInit() {
//     const data = this.parent.control.get(this.efFormControlName).value;
//     const pipe = this.pipe;

//     @Component({ template: '{{ data | ' + pipe + '}}' })
//     class DynamicComponent { @Input() public data: any; };
//     const tmpModule = NgModule({ imports: [BrowserModule], declarations: [DynamicComponent] })(class { });

//     this.compiler.compileModuleAndAllComponentsAsync(tmpModule)
//       .then(({ ngModuleFactory, componentFactories }) => {
//         const compFactory = componentFactories.find(x => x.componentType === DynamicComponent);
//         const injector = ReflectiveInjector.fromResolvedProviders([], this.vcRef.injector);
//         const cmpRef = this.vcRef.createComponent(compFactory, 0, injector, []);
//         cmpRef.instance.data = data;
//       });
//   }
// }
