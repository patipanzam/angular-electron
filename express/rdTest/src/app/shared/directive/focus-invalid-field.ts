import { Directive, HostListener, ElementRef } from '@angular/core';

@Directive({
    selector: '[focusInvalidField]'
})
export class FocusInvalidFieldDirective {

    constructor(private el: ElementRef) { }

    @HostListener('submit')
    onFormSubmit() {
        setTimeout(() => {
            const hasErrorEle = this.el.nativeElement.querySelectorAll('.has-error');
            if (hasErrorEle.length > 0) {
                const input = hasErrorEle[0].querySelector('input');
                input.scrollIntoView({ 
                    behavior: 'smooth' 
                });
                input.focus();
            }
        }, 0);
    }
}