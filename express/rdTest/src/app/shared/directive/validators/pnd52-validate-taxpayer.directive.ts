import { Directive } from '@angular/core';
import { Validator, NG_VALIDATORS, AbstractControl, ValidationErrors } from '@angular/forms';

@Directive({
    selector: '[efPnd52ValidateTaxpayer]',
    providers: [
        {
            provide: NG_VALIDATORS, 
            useExisting: Pnd52ValidateTaxpayerDirective, 
            multi: true 
        }
    ]
})
export class Pnd52ValidateTaxpayerDirective implements Validator {
    
    constructor() { }

    validate(control: AbstractControl): ValidationErrors {
        return Pnd52ValidateTaxpayerDirective.validateTaxpayer(control);
    }

    static validateTaxpayer(control: AbstractControl): ValidationErrors {
        if (control.value && control.value != '') {
            for (var i = 0, sum = 0; i < 12; i++) {
                sum += parseFloat(control.value.charAt(i)) * (13 - i);
            }
            if ((11 - sum % 11) % 10 != parseFloat(control.value.charAt(12))) {
                return {
                    E02PND52008: true
                };
            }
        }
        return null;
    }

}
