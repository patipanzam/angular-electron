import { Directive, ElementRef, HostListener, OnInit, Input, Optional, Output, EventEmitter } from '@angular/core';
import { ControlContainer } from '@angular/forms';

import { NotificationValidationMassageService } from '../services/notification-validation-massage.service';
// import { Pnd90CustomValidator } from 'src/app/modules/tax/pages/_pit/pnd90/custom-validators/pnd9091-validators';

@Directive({
  selector: '[efId13Field]',
  exportAs: 'efId13Field',
  providers: []
})
export class Id13FieldDirective implements OnInit {
  @Input('formControlName') valueFormControlName: string;
  @Output('onSuccess') _success: EventEmitter<any> = new EventEmitter<any>();
  @Output('onFail') _fail: EventEmitter<any> = new EventEmitter<any>();
  @Output('onClear') _clear: EventEmitter<any> = new EventEmitter<any>();

  private _oldValue: string = null;

  constructor(private el: ElementRef, @Optional() private parent: ControlContainer, private notificationValidationMassageService: NotificationValidationMassageService) { }

  ngOnInit() { }

  @HostListener('focus', ['$event.target.value'])
  onFocus(value: string) {
    if (value) {
      this._oldValue = value.split('-').join('');
    } else {
      this._oldValue = null;
    }
  }

  @HostListener('blur', ['$event.target.value']) onBlur(value: string) {
    if (value) {
      let value13: string = value.split('-').join('');
    //   if (this._oldValue != value13) {
    //     if (value13.length == 13) {
    //       if (Pnd90CustomValidator.isValidId13(value13)) {
    //         this._oldValue = null;
    //         this._success.emit(value13);
    //       } else {
    //         this.onFail();
    //         this.notificationValidationMassageService.notiMassageError('taxNoInvalid');
    //       }
    //     } else {
    //       this.onFail();
    //       this.notificationValidationMassageService.notiMassageError('id13');
    //     }
    //   }
    // } else if (this._oldValue) {
    //   this.reset();
    //   this._clear.emit();
    }
  }

  private onFail() {
    this.reset();
    this.el.nativeElement.value = '';
    this.el.nativeElement.focus();

    this._fail.emit();
  }

  private reset() {
    if (this.parent.control.get(this.valueFormControlName)) {
      this.parent.control.get(this.valueFormControlName).reset();
    }

    this._oldValue = null;
  }

}
