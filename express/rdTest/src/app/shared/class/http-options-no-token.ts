import { HttpHeaders } from "@angular/common/http";


export class HttpOptionsNoToken {

  public static httpOptions() {
    const _httpOptions = {
      headers: new HttpHeaders({
        'noToken': 'noToken'
      })
    };
    return _httpOptions;
  }
  public static httpRespTypeBlobOptions() {
    const _httpOptions = {
      headers: new HttpHeaders({
        'noToken': 'noToken'
      }),
      responseType : 'blob' as 'json'
    };
    _httpOptions['observe']='response';
    return _httpOptions;
  }
}
