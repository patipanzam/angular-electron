export interface District{
    districtCode: any,
    districtNameTh: any,
    districtNameEn: any
}

export function getDistrictDefault() {
    return {
        districtCode: undefined,
        districtNameTh: undefined,
        districtNameEn: undefined
    }
}