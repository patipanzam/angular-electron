export interface GlobalErrors {
    errorCode : string,
    errorMessage :  string,
    errorStackTrace : string
}