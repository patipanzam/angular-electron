interface GeneralModalButton {
    text: string;
    role?: 'button' | 'close';
    class?: string; 
    cb?: Function;
}

export interface GeneralModal {
    content: string;
    contentPosition?: 'left' | 'center' | 'right';
    buttons: [GeneralModalButton];
    ignoreBackdropClick?: boolean;
}