export interface Question {
    questionId: number,
    questionNameTh: string,
    disabled: boolean
}

export function getQuestionDefault() {
    return {
        questionId: undefined,
        questionNameTh: undefined,
        disabled: undefined
    }
}