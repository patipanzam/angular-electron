export interface SubDistrict{
    subDistrictCode: any,
    subDistrictNameTh: any,
    subDistrictNameEn: any
}

export function getSubDistrictDefault() {
    return {
        subDistrictCode: undefined,
        subDistrictNameTh: undefined,
        subDistrictNameEn: undefined
    }
}