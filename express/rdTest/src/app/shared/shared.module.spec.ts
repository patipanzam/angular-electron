import { SharedModule } from './shared.module';

describe('SharedModule', () => {
  let sharedModule: SharedModule;

  beforeEach(() => {
    sharedModule = new SharedModule(null, undefined, undefined);
  });

  it('should create an instance', () => {
    expect(sharedModule).toBeTruthy();
  });
});
