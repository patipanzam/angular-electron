import { Injectable, Optional } from '@angular/core';
import { th_TH, en_US } from 'ng-zorro-antd';
import { BsLocaleService } from 'ngx-bootstrap';

@Injectable()
export class LangService {

    currentLang: string;

    constructor() { 
        this.currentLang = this.initialLang();
    }

    initialLang(): string {
        let lang = localStorage.getItem('lang');
        if (!lang) {
            lang = 'th';
            localStorage.setItem('lang', lang);
        }
        return lang;
    }

    getLanguageForNz() {
        switch (this.currentLang) {
            case 'th':
                return th_TH;
            default:
                return en_US;
        }
    }

    getLanguageForNgSelect(key: string): string {
        if (key == 'notFoundText') {
            let value;
            switch (this.currentLang) {
                case 'th':
                    value = 'ไม่พบข้อมูล'
                    break;
                default:
                    value = 'No match found.'
                    break;
            }
            return value;
        } else if (key == 'loadingText') {
            let value;
            switch (this.currentLang) {
                case 'th':
                    value = 'กำลังค้นหา'
                    break;
                default:
                    value = 'Searching...'
                    break;
            }
            return value;
        }
        return '';
    }
}
