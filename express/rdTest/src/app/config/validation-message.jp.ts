export const ValidationMessageConfigJp = {
    min: '気持ちいい {0}',
    max: '気持ちいい {0}',
    required: '指定してください',
    requiredTrue: '指定してください',
    email: '気持ちいい',
    minlength: '指定してください {0} 気持ちいい',
    maxlength: '指定してください {0} 気持ちいい',
    pattern: '指定してください',
    taxNoInvalid: '指定してください/気持ちいい',
    passwordMismatch: '気持ちいい',
    invalidOtp: '指定してください OTP 気持ちいい'
}