module.exports = () => {
	var express = require('express');
	var app = express();
	const java = require('java');
	// parser
	var bodyParser = require('body-parser');
	app.use(bodyParser.json());

	// logger
	var morgan = require('morgan');
	app.use(morgan('dev'));

	// db
	// var db_infos = require('./db_infos');
	// var db = require('mongoose');

	// Angular
	app.use('/', express.static(__dirname + "/rdTest/dist"));

	// API routes
		// only for example
		// require('./api/routes/todo.routes.js')(app); 
	// var jasper = require('node-jasper')({
	// 	path:'./lib/jasperreports-5.6.1',
	// 	reports: {
	// 		print_form: {
	// 			// jasper:'./lib/PND1.jasper',
	// 			jasper:'./lib/report1.jasper',
	// 			conn: 'in_memory_json'
	// 		}
	// 	}
	// 	});	

	app.listen(3000, function () {
	  console.log('listening on port 3000...');
	});
}