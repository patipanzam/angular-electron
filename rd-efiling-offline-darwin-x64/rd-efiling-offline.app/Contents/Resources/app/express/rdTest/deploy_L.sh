#!/bin/bash
node --max_old_space_size=8192 node_modules/@angular/cli/bin/ng build --base-href=/rd-efiling-web/ --prod --build-optimizer
cd dist && tar czvf ../rd-efiling-web.tgz * && cd ..
scp rd-efiling-web.tgz rd-web:/opt/tomcat-efiling-web/webapps/
rm -f rd-efiling-web.tgz
ssh rd-web "sh /opt/tomcat-efiling-web/shell/./deploy_S.sh"

