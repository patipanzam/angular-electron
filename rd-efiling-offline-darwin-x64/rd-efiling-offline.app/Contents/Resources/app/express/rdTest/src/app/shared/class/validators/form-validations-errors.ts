import { Renderer2 } from '@angular/core';
import { ViewportScroller } from '@angular/common';
import { AbstractControl, FormGroup, ValidationErrors } from '@angular/forms';
import { InjectorInstance } from '../../../app.module';
import { SnotifyService } from 'ng-snotify';

export interface AllValidationErrors {
    controlName: string;
    errorKey: string;
    errorValue: any;
}

export interface FormGroupControls {
    [key: string]: AbstractControl;
}

export function AutoFocusErrorControl(formGroup: FormGroup, renderer: Renderer2, scroller: ViewportScroller ) {
    for (let key in formGroup.controls) {
        let targetCtrl = formGroup.get(key);
        if(targetCtrl.invalid) {
            let nativeElement = renderer.selectRootElement("[formControlName='"+key+"']", true);
            if(nativeElement){
                let rect = nativeElement.getBoundingClientRect();
                let scrollPos = scroller.getScrollPosition();
                scroller.scrollToPosition([0, rect.bottom + scrollPos[1] - 300]);

                let elementType = nativeElement.tagName.toLowerCase();
                if(elementType=="input") {
                    nativeElement.focus();
                }
                break;
            }
        }
    }
}
export function AutoFocusByErrorClass(renderer: Renderer2, scroller: ViewportScroller ) {
    try{
        let nativeElement = renderer.selectRootElement(".has-error", true);
        if(nativeElement){
            let rect = nativeElement.getBoundingClientRect();
            let scrollPos = scroller.getScrollPosition();
            scroller.scrollToPosition([0, rect.bottom + scrollPos[1] - 300]);

            let nativeInput = renderer.selectRootElement(".has-error input", true);
            if(nativeInput) {
                nativeInput.focus();
            }
        }
    }catch(ex){
        console.log("Something wrong!", ex);
    }
    
}

export async function getFormValidationErrors(formGroup: FormGroup, includeSelf: boolean = true): Promise<AllValidationErrors[]> {

    let errors: AllValidationErrors[] = [];

    if (includeSelf && formGroup.errors) {
        if (formGroup.errors !== null) {
            for (let keyError in formGroup.errors) {
                errors.push({
                    controlName: keyError,
                    errorKey: keyError,
                    errorValue: formGroup.errors[keyError]
                });
            }
        }
    }

    let controls: FormGroupControls = formGroup.controls;

    for (let key in controls) {
        const control = controls[key];
        if (control instanceof FormGroup) {
            let error = await getFormValidationErrors(control, false);
            errors = errors.concat(error);
        }
        const controlErrors: ValidationErrors = controls[key].errors;
        if (controlErrors !== null) {
            for (let keyError in controlErrors) {
                errors.push({
                    controlName: key,
                    errorKey: keyError,
                    errorValue: controlErrors[keyError]
                });
            }
        }
    }

    const response = await Promise.all(errors);

    displayErrorMessage(response);
    

    return Promise.resolve(response);
}

export function clearFormValidationErrors() {
    const snotifyService = InjectorInstance.get<SnotifyService>(SnotifyService);
    snotifyService.clear();
}

export function savePaymentValidationErrors() {
    const snotifyService = InjectorInstance.get<SnotifyService>(SnotifyService);
    let errorMsg: string = '';
    snotifyService.clear();
    snotifyService.error("ไม่สามารถรวมรายการชำระคราวเดียวกันได้", {
        timeout: 0,
        closeOnClick: true,
        bodyMaxLength: 1000
    });
    return errorMsg;
}

export function checkCancelPaymentValidationErrors() {
    const snotifyService = InjectorInstance.get<SnotifyService>(SnotifyService);
    let errorMsg: string = '';
    snotifyService.clear();
    snotifyService.error("ต้องมีรายการคงเหลือมากกว่า 1 รายการ", {
        timeout: 0,
        closeOnClick: true,
        bodyMaxLength: 1000
    });
    return errorMsg;
}

function displayErrorMessage(errorArray: AllValidationErrors[]): string {

    const snotifyService = InjectorInstance.get<SnotifyService>(SnotifyService);

    let errorMsg: string = '';

    for (let error of errorArray) {
        if (error) {
            if (error.errorKey != 'required' && error.errorKey != 'pattern') {
                if (error.errorKey == 'bsDate') {
                    Object.keys(error.errorValue).forEach(function (key) {
                        switch (key) {
                            case 'invalid':
                                if (errorMsg == '') {
                                    errorMsg += ' - รูปแบบวันที่ไม่ถูกต้อง';
                                } else {
                                    errorMsg += '\n - รูปแบบวันที่ไม่ถูกต้อง';
                                }
                                break;
                        }
                    });
                } else {
                    if (errorMsg == '') {
                        errorMsg += ' - ' + error.errorValue;
                    } else {
                        errorMsg += '\n - ' + error.errorValue;
                    }
                }
            }
        }
    }

    if (errorMsg != '') {
        snotifyService.clear();
        snotifyService.error(errorMsg, {
            timeout: 0,
            closeOnClick: true,
            bodyMaxLength: 1000
        });
    }


    return errorMsg;
}
