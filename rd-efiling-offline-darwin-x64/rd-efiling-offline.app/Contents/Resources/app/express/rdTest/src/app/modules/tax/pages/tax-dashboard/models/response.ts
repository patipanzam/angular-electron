export interface Response{
  responseCode: number,
  responseDesc: string
}