import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { TaxFilingCardModalComponent } from '../tax-filing-card-modal/tax-filing-card-modal.component';
import { TaxMenuModel, TaxMenu } from '../../../modules/tax/pages/tax-dashboard/models/tax-menu.model';
import { TaxFilingCard } from '../../../shared/models/tax-filing-card';
import { TaxFilingService } from '../../services/tax-filing.service';

@Component({
    selector: 'ef-tax-filing-card',
    templateUrl: './tax-filing-card.component.html',
    styleUrls: ['./tax-filing-card.component.scss']
})
export class TaxFilingCardComponent implements OnInit {

    @Input('taxFilingList') taxFilingList: TaxFilingCard[];
    @Input('other') other: boolean = false;
    @Input('taxMenu') taxMenu: TaxMenuModel;

    constructor(private tfService: TaxFilingService) { }

    ngOnInit() {
        console.log('taxFilingList',this.taxFilingList)
    }

    filing(menu: TaxMenu) {
        console.log('menu',menu)
        this.tfService.group = this.taxMenu;
        this.tfService.menu = menu;
        this.tfService.setFilingEvent(Math.random());
    }

}
