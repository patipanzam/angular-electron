import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ITaxBreadcrumb } from '../tax-breadcrumb/service/tax-breadcrumb.service';
import { TaxBreadcrumb4StepService } from './service/tax-breadcrumb-4step.service';

@Component({
  selector: 'ef-tax-breadcrumb-4step',
  templateUrl: './tax-breadcrumb-4step.component.html',
  styleUrls: ['./tax-breadcrumb-4step.component.scss']
})
export class TaxBreadcrumb4StepComponent implements OnInit {

  stepper$: Observable<number>;
  breadcrumb: ITaxBreadcrumb[];

  constructor(private taxBreadcrumbService: TaxBreadcrumb4StepService) {
    this.stepper$ = this.taxBreadcrumbService.stepper;
    this.breadcrumb = this.taxBreadcrumbService.breadcrumb;
  }

  ngOnInit() {
  }

}
