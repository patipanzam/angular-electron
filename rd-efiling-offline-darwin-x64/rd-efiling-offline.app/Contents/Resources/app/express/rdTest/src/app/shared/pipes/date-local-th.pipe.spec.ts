import { DateLocalThPipe } from './date-local-th.pipe';

describe('DateLocalThPipe', () => {
  let pipe : DateLocalThPipe;

  var dateTest = new Date();
  dateTest.setDate(14);
  dateTest.setMonth(1);
  dateTest.setFullYear(2019);

  dateTest.setHours(15);
  dateTest.setMinutes(35);
  dateTest.setMilliseconds(0);

  it('create an instance', () => {
    // const pipe = new DateLocalThPipe();
  // const pipe = new DateLocalThPipe();
  pipe = new DateLocalThPipe();
    expect(pipe).toBeTruthy();
  });

  /*full full-short  full-long short long numeric*/
  
  it('pip Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest);
    expect(filtered).toEqual('14/02/2562');
  });

  it('pip "numericxx" Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, "numericxx");
    console.log(filtered);
    expect(filtered).toEqual('14/02/2562');
  });

  it('pip ["numericxx", null] Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, ["numericxx", null]);
    console.log(filtered);
    expect(filtered).toEqual('14/02/2562');
  });

  it('pip ["numericxx", "SSDd"] Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, ["numericxx", "SSDd"]);
    console.log(filtered);
    expect(filtered).toEqual('14/02/2562');
  });

  it('pip "full" Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, "full");
    console.log(filtered);
    expect(filtered).toEqual('วันพฤหัสบดีที่ 14 กุมภาพันธ์ 2562');
  });

  it('pip "full-long" Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, "full-long");
    console.log(filtered);
    expect(filtered).toEqual('วันพฤหัสบดีที่ 14 กุมภาพันธ์ 2562');
  });

  it('pip "full-short" Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, "full-short");
    console.log(filtered);
    expect(filtered).toEqual('พฤ. 14 ก.พ. 2562');
  });

  it('pip "full-short" Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, "full-short");
    console.log(filtered);
    expect(filtered).toEqual('พฤ. 14 ก.พ. 2562');
  });

  it('pip "short" Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, "short");
    console.log(filtered);
    expect(filtered).toEqual('14 ก.พ. 2562');
  });

  it('pip "numeric" Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, "numeric");
    console.log(filtered);
    expect(filtered).toEqual('14/02/2562');
  });


  // --------------


  it('pip ["full"] Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, ["full"]);
    console.log(filtered);
    expect(filtered).toEqual('วันพฤหัสบดีที่ 14 กุมภาพันธ์ 2562');
  });

  it('pip ["full-long"] Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, ["full-long"]);
    console.log(filtered);
    expect(filtered).toEqual('วันพฤหัสบดีที่ 14 กุมภาพันธ์ 2562');
  });

  it('pip ["full-short"] Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, ["full-short"]);
    console.log(filtered);
    expect(filtered).toEqual('พฤ. 14 ก.พ. 2562');
  });

  it('pip ["full-short"] Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, ["full-short"]);
    console.log(filtered);
    expect(filtered).toEqual('พฤ. 14 ก.พ. 2562');
  });

  it('pip ["short"] Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, ["short"]);
    console.log(filtered);
    expect(filtered).toEqual('14 ก.พ. 2562');
  });

  it('pip ["numeric"] Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, ["numeric"]);
    console.log(filtered);
    expect(filtered).toEqual('14/02/2562');
  });

// ------

  it('pip ["full"] Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, ["full"]);
    console.log(filtered);
    expect(filtered).toEqual('วันพฤหัสบดีที่ 14 กุมภาพันธ์ 2562');
  });


  /* 
  test hide function
  hide-weekday, hide-year, hide-month, hide-day
  */

  it('pip ["hide-weekday"] Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, ["full","hide-weekday"]);
    console.log(filtered);
    expect(filtered).toEqual('14 กุมภาพันธ์ 2562');
  });

  it('pip ["full","hide-year"] Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, ["full","hide-year"]);
    console.log(filtered);
    expect(filtered).toEqual('วันพฤหัสบดีที่ 14 กุมภาพันธ์');
  });

  it('pip ["full","hide-month"] Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, ["full","hide-month"]);
    console.log(filtered);
    expect(filtered).toEqual('พ.ศ. 2562 วันพฤหัสบดี 14');
  });

  it('pip ["full","hide-day"] Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, ["full","hide-day"]);
    console.log(filtered);
    expect(filtered).toEqual('กุมภาพันธ์ 2562 วันพฤหัสบดี'); 
  });

  it('pip ["full", "hide-weekday", "hide-day"] Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, ["full", "hide-weekday", "hide-day"]);
    console.log(filtered);
    expect(filtered).toEqual('กุมภาพันธ์ 2562'); 
  });


  it('pip ["short","numeric", "time-th"] Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, ['short','numeric', 'time-th']);
    console.log(filtered);
    expect(filtered).toEqual('14/02/2562 เวลา 15:35 น.'); 
  });




});
