import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';

// import { ManagementGenaralInfoService } from 'src/app/modules/account-management/services/management-genaral-info.service';

@Injectable()
export class UserResolve implements Resolve<any> {

  constructor(
    // private managementGenaralInfoService: ManagementGenaralInfoService
    ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<any> {
    const userModel =  undefined//this.managementGenaralInfoService.inquiryUserProfileCommon();
    return forkJoin(userModel).pipe(map((res) => {
      return {
        userModel: res[0]
      };
    }));
  }
}