import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageDocumentViewerComponent } from './image-document-viewer.component';

describe('ImageDocumentViewerComponent', () => {
  let component: ImageDocumentViewerComponent;
  let fixture: ComponentFixture<ImageDocumentViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImageDocumentViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageDocumentViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
