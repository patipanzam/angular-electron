import { Injectable } from '@angular/core';
import { TaxMenuModel, TaxMenu } from '../../modules/tax/pages/tax-dashboard/models/tax-menu.model';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class TaxFilingService {

    private _group: TaxMenuModel;
    private _menu: TaxMenu;
    private _filingEvent: Subject<any> = new Subject<any>();
    
    constructor() { }

    getFilingEvent() {
        return this._filingEvent.asObservable();
    }

    setFilingEvent(value: any) {
        this._filingEvent.next(value);
    }

	public get group(): TaxMenuModel {
		return this._group;
	}

	public set group(value: TaxMenuModel) {
		this._group = value;
    }
    
	public get menu(): TaxMenu {
		return this._menu;
	}

	public set menu(value: TaxMenu) {
		this._menu = value;
	}



}
