import { Injectable } from '@angular/core';
import { FormGroup, ValidationErrors } from '@angular/forms';
import { SnotifyService, SnotifyToastConfig } from 'ng-snotify';

import { ValidationMessageService } from '../components/validation-message/validation-message.service';

@Injectable({
  providedIn: 'root'
})
export class NotificationValidationMassageService {

  private _config: SnotifyToastConfig = {
    timeout: 5000,
    showProgressBar: false
  };

  constructor(
    private snotifyService: SnotifyService,
    private validationMessageService: ValidationMessageService
  ) { }

  getFormValidationErrors(form: FormGroup) {
    let listError = []
    Object.keys(form.controls).forEach(key => {
      const controlErrors: ValidationErrors = form.get(key).errors;
      if (controlErrors != null) {
        Object.keys(controlErrors).forEach(keyError => listError.push({ fild: key, keyError: keyError }));
      }
    });
    this.notiMassageError(listError[0].keyError);
  }

  notiMassageError(errorType: string, ...params: string[]) {
    let errorMessageActual = this.validationMessageService.getErrorMsg(errorType);
    let _content = (errorMessageActual) ? this.stringFormat(errorMessageActual, params) : `${errorType}`;
    this.snotifyService.clear();

    /* FIXME: fix for error common */
    let errorTypeCommon = ['min', 'max', 'min_day', 'max_day', 'required', 'requiredTrue', 'requiredSelect', 'email', 'id13', 'minlength', 'maxlength', 'pattern', 'taxNoInvalid', 'passwordMismatch', 'invalidOtp', 'bsDate'];
    if (errorTypeCommon.includes(errorType)) { errorType = 'E' + errorType; }
    /* FIXME: end fix for error common */

    this.alert(errorType, _content);
  }

  notiMassageErrorServer(errorCode: string, errorDesc: string) {
    this.alert(errorCode, errorDesc);
  }

  private alert(errorCode: string, errorDesc: string) {
    this.snotifyService.clear();
    switch (errorCode.charAt(0).toLocaleUpperCase()) {
      case 'E':
        this.snotifyService.error(errorDesc, this._config);
        break;
      case 'W':
        this.snotifyService.warning(errorDesc, this._config);
        break;
      case 'I':
        this.snotifyService.info(errorDesc, this._config);
        break;
      default:
        this.snotifyService.success(errorDesc, this._config);
        break;
    }
  }

  private stringFormat(str: string, args: string[]) {
    return str.replace(/{(\d+)}/g, (match, number) => {
      return typeof args[number] != 'undefined' ? args[number] : match;
    });
  }

  /* FIXME: deprecated */
  success(title: string, content: string) {
    this.snotifyService.success(content, title);
  }

  /* FIXME: deprecated */
  warn(title: string, content: string) {
    this.snotifyService.warning(content, title);
  }
}
