import { NgModule, Optional, SkipSelf, ErrorHandler } from '@angular/core';
import { CommonModule } from '@angular/common';
import { throwIfAlreadyLoaded } from './module-import-guard';
import { NavComponent } from './nav/nav.component';
import { SharedModule } from '../shared/shared.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { RouterModule } from '../../../node_modules/@angular/router';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
import { ErrorsHandler } from './class/errors-handler';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { ServerErrorsInterceptor } from './class/server-errors-interceptor';
import { MainComponent } from './main/main.component';

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        SharedModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        SimpleNotificationsModule.forRoot({
            position: ['top', 'right'],
            preventDuplicates: true
        }),
    ],
    exports: [
        NavComponent,
        TranslateModule,
        LoadingBarHttpClientModule,
        LoadingBarRouterModule,
        SimpleNotificationsModule,
        MainComponent,
    ],
    declarations: [NavComponent, MainComponent],
    providers: [
        {
            provide: ErrorHandler,
            useClass: ErrorsHandler
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ServerErrorsInterceptor,
            multi: true
        },
    ]
})
export class CoreModule {

    constructor( @Optional() @SkipSelf() parentModule: CoreModule) {
        throwIfAlreadyLoaded(parentModule, 'CoreModule');
    }

}

export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
