import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from "@angular/common/http";
import { Observable, TimeoutError } from "rxjs";
import { Injectable, Injector } from "@angular/core";
import { timeout, catchError, retry } from "rxjs/operators";
import { throwError } from "rxjs";
import { NotificationsService, NotificationType } from "angular2-notifications";
import { HTTP_TIME_OUT } from "../../config/constants";
import { GlobalErrors } from "../../shared/models/global-errors.model";
import { environment } from '../../../environments/environment';

@Injectable()
export class ServerErrorsInterceptor  implements HttpInterceptor {

    private _notificationService: NotificationsService;

    constructor(private injector: Injector) {

    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        return next.handle(request).pipe(
            timeout(environment.httpTimeout),
            // retry(environment.retryAttempt),
            catchError((err) => {
                if (err instanceof HttpErrorResponse) {
                    switch (err.status) {
                        case 400:
                        // break;
                        case 404:
                        case 500:
                        case 504:
                        default:
                            this.showError(err);
                    }
                } else if (err instanceof TimeoutError) {
                    this.showError(err);
                }
                return throwError(err);
            })
        );
    }

    showError(error: HttpErrorResponse | TimeoutError) {
        console.log(">error:", error);
        this._notificationService = this.injector.get(NotificationsService);
        if (error instanceof HttpErrorResponse)  {
            if(error.error && error.error.globalErrors && error.error.globalErrors.length > 0){
                const globalErrors: GlobalErrors = error.error.globalErrors[0];
                this._notificationService.warn(globalErrors.errorCode, globalErrors.errorMessage);
            }
            else if (error.error instanceof Blob) {
                this.parseErrorBlob(error).subscribe(data=>{},err=>{
                    if(err.globalErrors){
                        const globalErrors: GlobalErrors = err.globalErrors[0];
                        this._notificationService.warn(globalErrors.errorCode, globalErrors.errorMessage);
                    }
                });
            }
            else if(error.error && error.error['error'] && error.error['error'] == "invalid_grant"){
                this._notificationService.warn('', 'ชื่อผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง โปรดตรวจสอบ');
            }
            else{
                this._notificationService.create(error.status, error.message);
            }
        } else if (error instanceof TimeoutError) {
            this._notificationService.create(error.name, error.message);
        }
    }

    parseErrorBlob(err: HttpErrorResponse): Observable<any> {
        const reader: FileReader = new FileReader();
    
        const obs = Observable.create((observer: any) => {
          reader.onloadend = (e) => {
            observer.error(JSON.parse(reader.result.toString()));
            observer.complete();
          }
        });
        reader.readAsText(err.error);
        return obs;
      } 
}
