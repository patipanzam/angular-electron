import { DataTableState } from "../models/data-table-state";
import { DataTablePageable } from "../models/data-table-pageable";
import { HttpParams } from "@angular/common/http";

export default class DataTableUtils {
    static saveState(key: string, params: any, pageable: DataTablePageable) {
        let state: DataTableState = {
            params: params,
            pageable: pageable
        };
        sessionStorage.setItem(key, JSON.stringify(state));
    }

    static getState(key: string): DataTableState {
        let data = sessionStorage.getItem(key)
        if (data) {
            return JSON.parse(data) as DataTableState;
        }
        return null;
    }

    static clearState(key: string) {
        sessionStorage.removeItem(key);
    }

    static sort(map: any, key: string, value: string) {
        for (const mk in map) {
            map[mk] = (mk === key ? value : null);
        }
        return map;
    }

    static getPageable(): DataTablePageable {
        return {
            pageIndex: 1,
            pageSize: 10,
            total: 0,
            loading: false,
            sortValue: null,
            sortKey: null
        };
    }

    static generateParams(pageable: DataTablePageable): HttpParams {
        let httpParams = new HttpParams()
        .set("page", pageable.pageIndex)
        .set("results", pageable.pageSize);
        if (pageable.sortKey && pageable.sortValue) {
            httpParams = httpParams.append('sortField', pageable.sortKey);
            httpParams = httpParams.append('sortOrder', pageable.sortValue);
        }
        return httpParams;
    }
}