/*
    full full-short full-long short long numeric 
    nonPipe: Thu Feb 14 2019 18:33:50 GMT+0700 (Indochina Time) 
    pipe : 14/2/2562 
    pipe 'full' : วันพฤหัสบดีที่ 14 กุมภาพันธ์ 2562 
    pipe 'short' : 14 ก.พ. 2562 
    pipe 'long' : 14 กุมภาพันธ์ 2562 
    pipe 'numeric': 14/2/2562 

    pipe ['full'] : วันพฤหัสบดีที่ 14 กุมภาพันธ์ 2562 
    pipe ['full-long'] : วันพฤหัสบดีที่ 14 กุมภาพันธ์ 2562 
    pipe ['full-short'] : พฤ. 14 ก.พ. 2562 
    pipe ['numeric'] : 14/2/2562 
    pipe ['long'] : 14 กุมภาพันธ์ 2562 
    pipe ['short'] : 14 ก.พ. 2562 

    pipe ['full-short'] : พฤ. 14 ก.พ. 2562 
    pipe ['full-short', 'numeric'] : พฤ. 14/2/2562 
    pipe ['full-short', 'long'] : วันพฤหัสบดีที่ 14 กุมภาพันธ์ 2562 

    pipe ['full', 'numeric'] : วันพฤหัสบดี 14/2/2562 
    pipe ['full', 'long'] : วันพฤหัสบดีที่ 14 กุมภาพันธ์ 2562 
    pipe ['full', 'short'] : พฤ. 14 ก.พ. 2562 

----------- e.g. -------------------
    nonPipe: {{birthDate.value}} <br>
    pipe          : {{ birthDate.value | dateLocalTh }} <br>
    pipe 'full'   : {{ birthDate.value | dateLocalTh:'full' }} <br>
    pipe 'short'  : {{ birthDate.value | dateLocalTh:'short' }} <br>
    pipe 'long'   : {{ birthDate.value | dateLocalTh:'long' }} <br>
    pipe 'numeric': {{ birthDate.value | dateLocalTh:'numeric' }} <br>
<br>
    pipe ['full']       : {{ birthDate.value | dateLocalTh: ['full'] }} <br>
    pipe ['full-long']  : {{ birthDate.value | dateLocalTh: ['full-long'] }} <br>
    pipe ['full-short'] : {{ birthDate.value | dateLocalTh: ['full-short'] }} <br>
    pipe ['numeric']    : {{ birthDate.value | dateLocalTh: ['numeric'] }} <br>
    pipe ['long']       : {{ birthDate.value | dateLocalTh: ['long'] }} <br>
    pipe ['short']      : {{ birthDate.value | dateLocalTh: ['short'] }} <br>
<br>
    pipe ['full-short']     : {{ birthDate.value | dateLocalTh: ['full-short'] }} <br>
    pipe ['full-short', 'numeric']  : {{ birthDate.value | dateLocalTh: ['full-short', 'numeric'] }} <br>
    pipe ['full-short', 'long']     : {{ birthDate.value | dateLocalTh: ['full-short', 'long'] }} <br>
<br>
    pipe ['full', 'numeric']  : {{ birthDate.value | dateLocalTh: ['full', 'numeric'] }} <br>
    pipe ['full', 'long']     : {{ birthDate.value | dateLocalTh: ['full', 'long'] }} <br>
    pipe ['full', 'short']     : {{ birthDate.value | dateLocalTh: ['full', 'short'] }} <br>


  **ps. e.g comboo <br>

  testDate :string = "2019/11/22"; ==> 
  testDate :string = "2562/11/22"; ==>
    {{testDate | toDate:'YYYY/MM/DD' | dateLocalTh: ['numeric'] }} <br>
    {{testDate | toDate:'YYYY-MM-DD' | dateLocalTh: ['short','numeric'] }} <br>
  output
    22/11/2562 

    hide-weekday, hide-year, hide-month, hide-day

  testDate :string = "2019/11/22T15:35:000
  {{testDate | toDate:'YYYY-MM-DDTHH:mm:ss.SSS' | dateLocalTh: ['short','numeric', 'time-th'] }}

  output
    14/02/2562 เวลา 15:35 น.


*/
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateLocalTh'
})
export class DateLocalThPipe implements PipeTransform {

  // transform(value: any, args?: any): any {
  transform(value: any, args?: any): any {


    // console.log("args:", args);
    var options = this.genOptions(args);

    if (value instanceof Date) {
      if (!args || args.length == 0) {
        options = { year: 'numeric', month: '2-digit', day: '2-digit' };
      }

      try {
        var date = value;

        if (this.toArgs(args).find(data => data == 'time-th')) {
          /* DataTimeM0de */
          return date.toLocaleDateString("th", options) + " เวลา " + date.toLocaleTimeString("th", { hour: '2-digit', minute: '2-digit' }) + " น.";
        } else {
          /* DataM0de */
          return date.toLocaleDateString("th", options);
        }

      } catch (error) {
        return value;
      }

    }

    return value;
  }

  toArgs(args?: any) {
    if (args instanceof Array) {

    } else if (args) {
      args = [args];
    } else {
      args = [];
    }

    return args;

  }

  genOptions(args?: any) {

    args = this.toArgs(args);

    // full full-short  full-long short long-numeric
    var options = { year: 'numeric', month: '2-digit', day: '2-digit' };

    var weekday = undefined;

    if (args.find((data) => data == 'full') || args.find((data) => data == 'full-long')) {
      // options = { weekday: 'long', year: '2-digit', month: 'long', day: '2-digit' };
      options['weekday'] = 'long';
      options['month'] = 'long';

    }
    if (args.find((data) => data == 'full-short')) {
      // options = { weekday: 'short', year: '2-digit', month: 'short', day: '2-digit' };
      options['weekday'] = 'short';
      options['month'] = 'short';
    }

    if (args.find((data) => data == 'short')) {
      options['month'] = 'short';
      weekday = 'short';
    }

    if (args.find((data) => data == 'long')) {
      options['month'] = 'long';
      weekday = 'long';
    }

    if (args.find((data) => data == 'numeric')) {
      options['month'] = '2-digit';
    }
    if (args.find((data) => data == 'numeric')) {
      options['month'] = '2-digit';
    }

    if (options['weekday'] && weekday) {
      options['weekday'] = weekday;
    }

    /* 
    hide function
    hide-weekday, hide-year, hide-month, hide-day
    */

    if (args.find((data) => data == 'hide-weekday')) {
      delete options['weekday'];
    }
    if (args.find((data) => data == 'hide-year')) {
      delete options['year'];
    }
    if (args.find((data) => data == 'hide-month')) {
      delete options['month'];
    }
    if (args.find((data) => data == 'hide-day')) {
      delete options['day'];
    }

    return options;

  }


}
