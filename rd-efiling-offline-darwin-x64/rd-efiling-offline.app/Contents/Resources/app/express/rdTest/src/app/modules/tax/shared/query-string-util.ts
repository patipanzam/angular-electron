export class QueryStringUtils {
  public static get(params): string {
    return (params) ? `?${Object.keys(params).map(key => key + '=' + params[key]).join('&')}` : '';
  }
}
