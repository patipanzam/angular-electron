/* Pnd1CompleteForm */
export class Pnd1CompleteForm {
  DocDetail: DocDetailComplete;
  FromDetail: FromDetailComplete;
  Process: ProcessComplete;

  constructor() {
    this.DocDetail = new DocDetailComplete();
    this.FromDetail = new FromDetailComplete();
    this.Process = new ProcessComplete();
  }
}

export class DocDetailComplete {
  Code: string;
  Version: string;
}

export class FromDetailComplete {
  TaxPayerInfo: TaxPayerInfoComplete;
  TaxDetail: TaxDetailComplete;
  Payment: PaymentComplete;
}

export class TaxPayerInfoComplete {
  Id: string;
  BranchNo: string;
  BranchType: string;
  JurName: string;
  TaxAddress: TaxAddressComplete;
}

export class TaxAddressComplete {
  Blog: string;
  Room: string;
  Floor: string;
  Village: string;
  AddNo: string;
  Moo: string;
  Soi: string;
  Junc: string;
  Street: string;
  Tamb: string;
  Amph: string;
  Prov: string;
  Post: string;
}

export class TaxDetailComplete {
  RefNo: string;
  Year: string;
  Month: string;
  FilingType: string;
  FilingNo: string;
  LTO: string;
  HomeOffice: string;
  TotNum: string;
  TotIncome: string;
  NetTax: string;
  Summary: SummaryComplete;
}

export class SummaryComplete {
  Detail?: (DetailEntityComplete)[] | null;
}

export class DetailEntityComplete {
  Type: string;
  Total: string;
  Income: string;
  Tax: string;
  DocNo?: string | null;
  DocDate?: string | null;
}

export class PaymentComplete {
  Surcharge: string;
  Penalty: string;
  TotTax: string;
  IsRound: string;
  TaxAmt: string;
  Criminal: string;
  TotPay: string;
}

export class ProcessComplete {
  GenBill: string;
  ReceiveNo: string;
  ReceiveDate: string;
  PaidAmount: string;
  UID: string;
  DLN: string;
}

/* Pnd1AttachForm */
export class Pnd1AttachForm {
   RdForm: RdForm;
}
export interface RdForm {
  DocDetail: DocDetail;
  AttachDetail: AttachDetail;
  FormAttach: FormAttach;
}
export interface DocDetail {
  Code: string;
  Version: string;
}
export interface AttachDetail {
  TaxPayerInfo: TaxPayerInfo;
  TaxDetail: TaxDetail;
  PrintDetail: PrintDetail;
}
export interface TaxPayerInfo {
  Id13: string;
  BranNo: number;
}
export interface TaxDetail {
  RefNo: string;
}
export interface PrintDetail {
  NumPerPage: number;
}
export interface FormAttach {
  AttDet?: (AttDetEntity)[] | null;
}
export interface AttDetEntity {
  Type: string;
  AttData?: (AttDataEntity)[] | null;
}
export interface AttDataEntity {
  Seq: number;
  Id13: string;
  Id10: string;
  Tit: string;
  Fname: string;
  Mname: string;
  Lname: string;
  Date: string;
  Pay: number;
  Tax: number;
  Con: number;
}

export class TitleName {
  titleCode : string;
  titleName : string;
}
