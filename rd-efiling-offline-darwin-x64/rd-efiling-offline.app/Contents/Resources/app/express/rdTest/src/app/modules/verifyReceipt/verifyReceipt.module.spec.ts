import { VerifyReceiptModule } from './verifyReceipt.module';

describe('VerifyReceiptModule', () => {
  let verifyReceiptModule: VerifyReceiptModule;

  beforeEach(() => {
    verifyReceiptModule = new VerifyReceiptModule();
  });

  it('should create an instance', () => {
    expect(verifyReceiptModule).toBeTruthy();
  });
});
