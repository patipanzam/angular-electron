export interface Notification {
    event: string;
    data: string;
    createDate: any;
    formCode: string;
    message: string;
    nid: string;
    notificationId: number;
}