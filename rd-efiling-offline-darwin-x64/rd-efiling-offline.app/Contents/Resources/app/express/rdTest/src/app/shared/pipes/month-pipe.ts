import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'month' })
export class MonthPipe implements PipeTransform {

    monthList = [
        { id: 1, code: '01', nameLocal: 'มกราคม' },
        { id: 2, code: '02', nameLocal: 'กุมภาพันธ์' },
        { id: 3, code: '03', nameLocal: 'มีนาคม' },
        { id: 4, code: '04', nameLocal: 'เมษายน' },
        { id: 5, code: '05', nameLocal: 'พฤษภาคม' },
        { id: 6, code: '06', nameLocal: 'มิถุนายน' },
        { id: 7, code: '07', nameLocal: 'กรกฎาคม' },
        { id: 8, code: '08', nameLocal: 'สิงหาคม' },
        { id: 9, code: '09', nameLocal: 'กันยายน' },
        { id: 10, code: '10', nameLocal: 'ตุลาคม' },
        { id: 11, code: '11', nameLocal: 'พฤศจิกายน' },
        { id: 12, code: '12', nameLocal: 'ธันวาคม' },
      ];

    transform(value: any, args?: any): any {
       
        const option = this.monthList.filter(data => data.id === parseInt(value, 10));
        const result = (option.length > 0)? option[0].nameLocal : '';
       
        return result;
      }
}