export interface Receipt {
    verifyStatus?: String;
    receiptStatus?: String;
    receiptNo?: String;
    totalAmount?: String;
    receiptTypeNameTh?: String;
    receiptTypeNameEn?: String;
    nid?: String;
    branchNo?: String;
    firstName?: String;
    lastName?: String;
    formType?: String;
    filingRefNo?: String;
    receiptDate?: Date;
    taxMonth?: String;
    taxYear?: String;
}