import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'wsDateShow'
})
export class WsDateShowPipe implements PipeTransform {

    transform(value: any, format?: string): string {
        if (value) {
            let date = (format) ? moment(value, format) : moment(value);
            return date.format('DD/MM/') + (date.year() + 543);
        }
        return null;
    }

}
