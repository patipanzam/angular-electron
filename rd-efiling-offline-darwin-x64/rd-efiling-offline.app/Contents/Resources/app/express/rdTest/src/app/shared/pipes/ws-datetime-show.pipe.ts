import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'wsDatetimeShow'
})
export class WsDatetimeShowPipe implements PipeTransform {

  transform(value: any, format?: any): any {
    if (value) {
      let date = (format) ? moment(value,format) : moment(value);
      let time = date.format('HH:mm:ss');
      return date.format('DD/MM/') + (date.year() + 543)+' '+time;
    }
    return null;
  }

}
