import { trigger, animate, style, group, query, transition, sequence, animateChild } from '@angular/animations';

export const routerTransition = trigger('routerTransition', [
	transition('* <=> *', [
		query(':enter, :leave',
			style({ position: 'fixed', opacity: 1 }),
			{ optional: true }
		),
		group([
			query(':enter', [
				style({ opacity: 0 }),
				animate('0.3s', style({ opacity: 1 }))
			], { optional: true }),
			query(':leave', [
				style({ opacity: 1 }),
				animate('0.3s', style({ opacity: 0 }))
			], { optional: true }),
		])
	])
]);

export const slideTransition = trigger('slideTransition', [
	transition('* => *', [
		query(':enter, :leave', style({ position: 'fixed', width: '100%' }), { optional: true }),
		query(':enter', style({ transform: 'translateX(100%)' }), { optional: true }),
		sequence([
			query(':leave', animateChild(), { optional: true }),
			group([
				query(':leave', [
					style({ transform: 'translateX(0%)' }),
					animate('500ms ease', style({ transform: 'translateX(-100%)' }))
				], { optional: true }),
				query(':enter', [
					style({ transform: 'translateX(100%)' }),
					animate('500ms ease',
						style({ transform: 'translateX(0%)' })),
				], { optional: true }),
			]),
			query(':enter', animateChild(), { optional: true }),
		])
	])
]);

export const fadeAnimation = trigger('fadeAnimation', [
	transition('* => *', [
		query(
			':enter',
			[style({ opacity: 0 })],
			{ optional: true }
		),
		query(
			':leave',
			[style({ opacity: 1 }), animate('0.3s', style({ opacity: 0 }))],
			{ optional: true }
		),
		query(
			':enter',
			[style({ opacity: 0 }), animate('0.3s', style({ opacity: 1 }))],
			{ optional: true }
		)
	])
]);