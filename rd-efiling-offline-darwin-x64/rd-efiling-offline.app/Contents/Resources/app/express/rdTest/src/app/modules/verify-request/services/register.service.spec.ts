import { TestBed } from '@angular/core/testing';

import { RegisterVerifyService } from './register.service';

describe('RegisterVerifyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RegisterVerifyService = TestBed.get(RegisterVerifyService);
    expect(service).toBeTruthy();
  });
});
