import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VerifyRequestComponent } from './pages/verify-request/verify-request.component';
import { VerifyRequestRoutingModule } from './verify-request-routing.module';
import { CheckRequestComponent } from './pages/check-request/check-request.component';
import { SharedModule } from '../../shared/shared.module';
import { NgxCaptchaModule } from 'ngx-captcha';
import { environment } from '../../../environments/environment';
import { CheckRequestListComponent } from './pages/check-request-list/check-request-list.component';
import { RegisterVerifyService } from './services';

@NgModule({
  imports: [
    CommonModule,
    VerifyRequestRoutingModule,
    SharedModule,
    NgxCaptchaModule.forRoot({
			reCaptcha2SiteKey: environment.recaptchaSiteKey
        })
  ],

  declarations: [VerifyRequestComponent, CheckRequestComponent, CheckRequestListComponent],
  providers: [
    RegisterVerifyService
]
})
export class VerifyRequestModule { }
