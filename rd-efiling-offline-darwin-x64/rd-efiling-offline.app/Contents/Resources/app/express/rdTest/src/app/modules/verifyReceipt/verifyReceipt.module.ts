import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VerifyReceiptComponent } from './pages/verifyReceipt/verifyReceipt.component';
import { VerifyReceiptSearchComponent } from './pages/searchReceipt/verifyReceiptSearch.component'
import { VerifyReceiptRoutingModule } from './verifyReceipt-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { ReceiptService} from './services/receipt.service';
import { NgxCaptchaModule } from 'ngx-captcha';
import { environment } from '../../../environments/environment.prod';
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    VerifyReceiptRoutingModule,
    NgxCaptchaModule .forRoot({
      reCaptcha2SiteKey: environment.recaptchaSiteKey
  }),
  ],
  declarations: [
    VerifyReceiptComponent
    ,VerifyReceiptSearchComponent
  ],
  providers:[ReceiptService]
})
export class VerifyReceiptModule { }
