import { Injectable } from '@angular/core';

@Injectable()
export class UserService {

  private _userModel?: UserModel;
  get userModel(): UserModel {
    if (!this._userModel) {
      this._userModel = new UserModel();
    }

    return this._userModel;
  }

  private _username: String;
  get username(): String {
    return this._username;
  }

  private _authorities: String[] = [];
  get authorities(): String[] {
    return this._authorities;
  }

  setJwtObject(jwtObject: any) {
    this._username = jwtObject.user_name;
    this._authorities = jwtObject.authorities;
    this._userModel = new UserModel(jwtObject);
  }
}

export interface User {
  username: string;
  password: string;
}

export class UserModel {
  userId: string;
  userName: string;
  nid: string;
  tin: string;
  txpName: string;
  companyName: string;
  vatBranchNo: string;
  sbtBranchNo: string;
  whtBranchNo: string;
  registerType: string;
  registerFlag: string;
  level: string;
  typeTaxPayerId: string;
  vatFlag: string;
  sbtFlag: string;
  taCode: string;
  paCode: string;
  taxPayerGroup: string;
  accountName: string;
  whtFlag: string;

  constructor(jwtObject?: any) {
    this.userId = jwtObject ? jwtObject['01'] : '';
    this.userName = jwtObject ? jwtObject['02'] : '';
    this.nid = jwtObject ? jwtObject['03'] : '';
    this.tin = jwtObject ? jwtObject['04'] : '';
    this.txpName = jwtObject ? jwtObject['05'] : '';
    this.companyName = jwtObject ? jwtObject['06'] : '';
    this.vatBranchNo = jwtObject ? jwtObject['07'] : '';
    this.sbtBranchNo = jwtObject ? jwtObject['08'] : '';
    this.whtBranchNo = jwtObject ? jwtObject['09'] : '';
    this.registerType = jwtObject ? jwtObject['10'] : '';
    this.registerFlag = jwtObject ? jwtObject['11'] : '';
    this.level = jwtObject ? jwtObject['12'] : 'x';
    this.typeTaxPayerId = jwtObject ? jwtObject['13'] : '';
    this.vatFlag = jwtObject ? jwtObject['14'] : '';
    this.sbtFlag = jwtObject ? jwtObject['15'] : '';
    this.taCode = jwtObject ? jwtObject['16'] : '';
    this.paCode = jwtObject ? jwtObject['17'] : '';
    this.taxPayerGroup = jwtObject ? jwtObject['18'] : '';
    this.accountName = jwtObject ? jwtObject['19'] : '';
    this.whtFlag = jwtObject ? jwtObject['20'] : '';
  }
}
