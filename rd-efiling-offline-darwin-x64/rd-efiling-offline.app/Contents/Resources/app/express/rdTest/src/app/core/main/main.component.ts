import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

// import { MainMenuModel } from 'src/app/modules/home/models/main-menu.model';

declare var $: any;

@Component({
  selector: 'ef-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  homeMenu: MainMenuModel[];

  constructor(private routerActive: ActivatedRoute) {
    this.homeMenu = [];
    if (this.routerActive.snapshot.data['resolveData']) {
      this.homeMenu = this.routerActive.snapshot.data['resolveData'].homeMenu;
      console.log('homeMenu',this.homeMenu)
    }
  }

  ngOnInit() {
    $('#click-user').click(() => {
      $('#box-usermanage').toggle();
    });
  }
}

export class MainMenuModel {
  menuId: number;
  menuNameEn: string;
  menuNameTh: string;
  activeStatus: string;
  parentId: number;
  levelNo: number;
  linkUrl: string;
  orderNo: number;
  userLevelMin: string;
  createBy: number;
  createDate: string;
  updateBy: number;
  updateDate: string;
  isActive: boolean = false;
}