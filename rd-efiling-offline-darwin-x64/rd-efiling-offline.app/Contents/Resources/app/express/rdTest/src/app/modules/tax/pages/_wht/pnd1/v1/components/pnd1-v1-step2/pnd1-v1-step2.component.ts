import { Component, OnInit, Input,Output, ViewChild, ElementRef,EventEmitter,TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder ,Validators,AbstractControl, ValidatorFn} from '@angular/forms';
import { TitleName } from '../../../models/pnd1.model';
import { Pnd1Service } from '../../../services/pnd1.service';
import { TaxComponent } from '../../../../../tax-component/tax.component';
import { Pnd1AttachFormFieldInputConfig } from '../../../models/pnd1-fieldconfig.model';
import { DateLocalThPipe } from '../../../../../../../../shared/pipes/date-local-th.pipe';
import { Pnd1CustomValidator } from '../../../models/pnd1-custom-validator';
import * as moment from 'moment';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { DecimalPipe } from '@angular/common';
import { BsDatepickerConfig, BsDatepickerViewMode } from 'ngx-bootstrap/datepicker';

@Component({
  selector: 'pnd1-v1-step2',
  templateUrl: './pnd1-v1-step2.component.html',
  styleUrls: ['./pnd1-v1-step2.component.scss'],
  providers : [DecimalPipe]
})
export class Pnd1V1Step2Component implements OnInit {
  @Input('formPnd1') formPnd1: FormGroup;
  @Input('formInputPnd1') formInputPnd1:any;
  @Output() onClickPrintDraft = new EventEmitter();
  @ViewChild('id13IncomeEle') id13IncomeEle :ElementRef;

  modalClearData: BsModalRef;
  modalDeleteAllData: BsModalRef;
  payModal: BsModalRef;
  msgBody : string;
  alertHeader:string;

  formPnd1Input: FormGroup;
  formSearch: FormGroup;
  formSubmitAttempt: boolean;

  Con = [
    {id:1,name: 'หัก ณ ที่จ่าย'},
    {id:2,name: 'ออกให้ตลอดไป'},
    {id:3,name: 'ออกให้ครั้งเดียว'}
  ];

  titleOption : TitleName[];
  tabValue = [];
  tabSelected = null;
  checkDelete : boolean = false;
  searchAll :boolean = false;
  inputSearch;
  checkDate :boolean = false;
  checkFirstName:boolean = false;
  checkSurName:boolean = false;
  checkIncome:boolean = false;
  checkTex:boolean = false;

  payError:boolean = false;
  taxError:boolean = false;

  dateValue;
  massage_error_firstName:string;
  massage_error_surName:string;
  massage_error_date:string;

  
  control?: AbstractControl

  /* Table config */
  allChecked = false;
  indeterminate = false;
  pageIndex = 1;
  pageSize = 10;
  total = 1;
  dataSet = [];
  loading = true;
  sortValue = '';
  sortKey = '';
  dataInput: any;
  dataTab = 0;
  tabType;
  dataDelete = [];
  dataSearch = [];
  dataSummary = [];

  constructor(private pnd1Service: Pnd1Service,private taxComponent: TaxComponent,private fb: FormBuilder,private modalService: BsModalService ,private decPipe : DecimalPipe) {
    this.tabValue.push(
      {
        tabNo: 1,
        tabName:'401N',
        tabTitle: 'เงินได้ตามมาตรา 40(1)\nเงินเดือน ค่าจ้าง ฯลฯ กรณีทั่วไป',
        tabDesc: 'ใบแนบ ภ.ง.ด.1 เงินได้ตามมาตรา 40(1) เงินเดือน ค่าจ้าง ฯลฯ กรณีทั่วไป',
      }, {
        tabNo: 2,
        tabName:'401N',
        tabTitle: 'เงินได้ตามมาตรา 40(1) (2)\nกรณีนายจ้างจ่ายให้ครั้งเดียว\nเพราะเหตุออกจากงาน',
        tabDesc: 'เงินได้ตามมาตรา 40(1) (2) กรณีนายจ้างจ่ายให้ครั้งเดียว เพราะเหตุออกจากงาน'
      }, {
        tabNo: 3,
        tabName:'402',
        tabTitle: 'เงินได้ตามมาตรา 40(2)\nกรณีผู้รับเงินได้เป็นผู้อยู่ในประเทศไทย',
        tabDesc: 'เงินได้ตามมาตรา 40(2) กรณีผู้รับเงินได้เป็นผู้อยู่ในประเทศไทย'
      }, {
        tabNo: 4,
        tabName:'402N',
        tabTitle: 'เงินได้ตามมาตรา 40(2)\nกรณีผู้รับเงินได้มิได้เป็นผู้อยู่\nในประเทศไทย',
        tabDesc: 'เงินได้ตามมาตรา 40(2) กรณีผู้รับเงินได้มิได้เป็นผู้อยู่ในประเทศไทย'
      }
    );

    // this.formPnd1Input = this.fb.group({
    //   pnd1AttachInputForm: this.taxComponent.createForm(Pnd1AttachFormFieldInputConfig)
    // });
    // this.setForm();
  }

  ngOnInit() {
    this.setForm();
    this.searchData();
    this.getData();
    if(this.formInputPnd1 !== undefined){
      this.formPnd1Input = this.formInputPnd1;
    }
  }

  getData(){
    this.titleOption=[];
    this.pnd1Service.getTitleNames().subscribe(data => {
      this.titleOption = [...data];
   });
  }

  setForm(){
    this.formPnd1Input = this.fb.group({
      seq:   [null, Validators.compose([])],
      id13:  [null, Validators.compose([Validators.required,Pnd1CustomValidator.ID13,Validators.maxLength(13)])],
      id10:  [null, Validators.compose([])],
      titleName:   [null, Validators.compose([Validators.required])],
      titleCode: [null, Validators.compose([])],
      firstName: [null, Validators.compose([Validators.required])],
      surName: [null, Validators.compose([Validators.required])],
      midName: [null, Validators.compose([])],
      date:  [null, Validators.compose([Validators.required])],
      pay:   [null, Validators.compose([Validators.required])],
      tax:   [null, Validators.compose([Validators.required])],
      con:   [null, Validators.compose([Validators.required])],
    })
    this.formSearch = this.fb.group({
      inputSearch:   [null, Validators.compose([Validators.maxLength(13),Pnd1CustomValidator.ID13,Pnd1CustomValidator.E02PND1009])],
    })
    this.setValidator();
    this.checkValidate();
  }
  
  setValidator(){
    this.formPnd1Input.controls.id13.setValidators([Validators.required,Pnd1CustomValidator.E02PND1009,Pnd1CustomValidator.E02PND1018,Pnd1CustomValidator.ID13,Validators.maxLength(13)]);
    this.formPnd1Input.controls.date.setValidators([Validators.required,Pnd1CustomValidator.E02PND1001,Pnd1CustomValidator.E02PND1013(this.formPnd1.value.pnd1CompleteForm.formDetail.taxDetail.month),Pnd1CustomValidator.E02PND1015(this.formPnd1.value.pnd1CompleteForm.formDetail.taxDetail.year)]);
    this.formPnd1Input.controls.id13.updateValueAndValidity();
    this.formPnd1Input.controls.date.updateValueAndValidity();
  }

  setValidator1(){
    this.dataInput = this.formPnd1Input.value;
    this.formPnd1Input.controls.pay.setValidators([Validators.required,Pnd1CustomValidator.E02PND1026,Pnd1CustomValidator.E02PND1027(this.dataInput.tax),Pnd1CustomValidator.E02PND1031(this.dataInput.tax)]);
    this.formPnd1Input.controls.tax.setValidators([Validators.required,Pnd1CustomValidator.E02PND1029,Pnd1CustomValidator.E02PND1030(this.dataInput.pay),Pnd1CustomValidator.E02PND1031(this.dataInput.pay)]);
    this.formPnd1Input.controls.pay.updateValueAndValidity();
    this.formPnd1Input.controls.tax.updateValueAndValidity();
  }

  setValidator2(){
    this.dataInput = this.formPnd1Input.value;
    this.formPnd1Input.controls.pay.setValidators([Validators.required,Pnd1CustomValidator.E02PND1028,Pnd1CustomValidator.E02PND1031(this.dataInput.tax)]);
    this.formPnd1Input.controls.tax.setValidators([Validators.required,Pnd1CustomValidator.E02PND1029,Pnd1CustomValidator.E02PND1031(this.dataInput.pay)]);
    this.formPnd1Input.controls.pay.updateValueAndValidity();
    this.formPnd1Input.controls.tax.updateValueAndValidity();
  }

  checkValidate(){
    const checkType = this.formPnd1.get('pnd1CompleteForm.formDetail.taxDetail.filingType').value
    if(checkType === 0){
      this.setValidator1();
    }else{
      this.setValidator2();
    }
  }

  focusFirstName(){
   this.checkFirstName =  false;
  }

  focusSurName(){
   this.checkSurName =  false;
  }

  focusDate(){
   this.checkDate = false;
  }

  changeDate(){
   if(this.formPnd1Input.controls["date"].value == "Invalid Date"){
      this.massage_error_date = "รูปแบบวันที่ไม่ถูกต้อง";
      this.formPnd1Input.controls["date"].reset();
      this.checkDate = true;
   }
  }

  checkInputId13(value: string){
     if(value.length === 17){
       this.setAutoData();
     }
  }
  
  setAutoData(){
    const dataFilter = [];
    const data = this.formPnd1.get('pnd1AttachForm.formAttach.attDet').value.filter(x => x.type != this.tabType);
    data.forEach(option => {
      option.attData.forEach(data => {
        dataFilter.push(data)
      });
    });
    this.dataInput = this.formPnd1Input.value;
    const result = dataFilter.filter(x => x.id13 == this.dataInput.id13)
    if (result.length > 0) {
      const data = result[0];
      this.formPnd1Input.controls["titleName"].setValue(data.titleName);
      this.formPnd1Input.controls["firstName"].setValue(data.firstName);
      this.formPnd1Input.controls["surName"].setValue(data.surName);
      this.formPnd1Input.controls["midName"].setValue(data.midName);
    }
  }

  checkInputTaxPay(value,templateTaxPay){
    if(this.formPnd1.get('pnd1CompleteForm.formDetail.taxDetail.filingType').value === 1 && parseInt(value, 10) === 0){
       this.showModalTaxPay(templateTaxPay)
    }
    this.payError = true;
  }

  checkInputTax(){
    this.taxError = true;
  }

  setDataTaxPay(value){
    if(value == 0){
      this.formPnd1Input.controls["pay"].reset();
    }
    this.payModal.hide();
  }

  checkCharacter(value) {
    var re = new RegExp(/[A-Za-zก-๙]/g)
    if (re.test(value)) {
      this.checkFirstName =  false;
    }else if(value === ""){
      this.checkFirstName =  false;
    }else{
      this.massage_error_firstName = "ชื่อของผู้มีเงินได้ ไม่สามารถมีอักขระพิเศษ หรือตัวเลขได้ ";
      this.checkFirstName =  true;
      this.formPnd1Input.controls["firstName"].reset();
    }
  }

  checkCharacter1(value) {
    var re = new RegExp(/[A-Za-zก-๙]/g)
    if (re.test(value)) {
      this.checkSurName =  false;
    }else if(value === ""){
      this.checkSurName =  false;
    }else{
      this.massage_error_surName = "ชื่อสกุลของผู้มีเงินได้ ไม่สามารถมีอักขระพิเศษ หรือตัวเลขได้ ";
      this.checkSurName =  true;
      this.formPnd1Input.controls["surName"].reset();
    }
  }

  checkCharacter2(value) {
    var re = new RegExp(/[A-Za-zก-๙]/g)
    if (re.test(value)) {

    }else{
      this.formPnd1Input.controls["midName"].reset();
    }
  }

  sort(sort: { key: string, value: string }): void {
    this.sortKey = sort.key;
    this.sortValue = sort.value;
    this.dataSet.reverse()
    this.searchData()
    


  }

  searchData(reset: boolean = false, tab: number = 0): void {
    var tab01 = '401N';
    var tab02 = '4012';
    var tab03 = '402I';
    var tab04 = '402E';
    this.dataTab = tab;
    this.tabType = (this.dataTab == 0) ? tab01 : 0 | Number(this.dataTab == 1) ? tab02 : 0 | Number(this.dataTab == 2) ? tab03 : 0 | Number(this.dataTab == 3) ? tab04 : 0;
    if (reset) {
      this.pageIndex = 1;
    }
    this.loading = true;
   if(this.sortValue === ''){
    try {
      this.loading = false;
      var result = this.formPnd1.get('pnd1AttachForm.formAttach.attDet').value.filter(x => x.type == this.tabType);
      var dataResult = (result.length > 0) ? result[0].attData : result;
      this.dataSet = dataResult.slice((this.pageIndex - 1) * this.pageSize, this.pageSize * this.pageIndex);
      this.dataSearch = this.dataSet;
      var number = Object.keys(dataResult).length;
      this.total = number;
      this.setValidatorId13();
    } catch (error) { }
    try {
      var result = this.formPnd1.get('pnd1AttachForm.formAttach.attDet').value.filter(x => x.type == this.tabType)[0].attData;
      var Total = result.length;
      var Income = result.map(item => Number(item.pay)).reduce((a, b) => a + b);
      var Tax = result.map(item => Number(item.tax)).reduce((a, b) => a + b);
      this.dataSummary = [{ Total: Total, Income: Income, Tax: Tax }]
      var numberIncom  = new String(this.dataSummary[0].Income)
      var numberTax    = new String(this.dataSummary[0].Tax)  
      this.checkIncome = (numberIncom.length > 10)? true : false;
      this.checkTex    = (numberTax.length > 10)? true : false;

    } catch (error) {
      this.dataSummary = [{ Total: '-', Income: '-', Tax: '-' }]
    }
    try {
      var total = [];
      this.formPnd1.get('pnd1AttachForm.formAttach.attDet').value.forEach(option => {
        option.attData.forEach(data => {
          total.push(data)
        });
      });
      var  totIncome = total.map(item => Number(item.pay)).reduce((a, b) => a + b);
      var  number = Object.keys(total).length;
      this.formPnd1.get('pnd1CompleteForm.formDetail.taxDetail.totIncome').setValue(totIncome);
      this.formPnd1.get('pnd1CompleteForm.formDetail.taxDetail.totNum').setValue(number);
    } catch (error) {}
   }else{
     try {
       this.loading = false;
       var result = this.formPnd1.get('pnd1AttachForm.formAttach.attDet').value.filter(x => x.type == this.tabType);
       var dataResult = (result.length > 0) ? result[0].attData : result;
       var results = dataResult.slice((this.pageIndex - 1) * this.pageSize, this.pageSize * this.pageIndex);
       if (this.sortValue === "ascend") {
         if (this.sortKey === 'firstName') {
          const data = results.sort(function (a, b) {
             return a.firstName.localeCompare(b.firstName);
           });
          const dataset = data.map((item,index) => { return {...item,seq:index + 1}})
          this.dataSet  = dataset;
         
        } else if (this.sortKey === 'date') {

          const data = results.sort(function (a, b) {
             return a.date.localeCompare(b.date);
           });
          const dataset = data.map((item,index) => {return {...item,seq:index + 1}})
          this.dataSet  = dataset;

         } else {
           const data = results.sort((a, b) => a[this.sortKey] - b[this.sortKey]);
           const dataset = data.map((item,index) => {return {...item,seq:index + 1}})
           this.dataSet  = dataset;
         }

       } else if (this.sortValue === "descend") {
         if (this.sortKey === 'firstName') {

           const data = results.sort(function (a, b) {
             return b.firstName.localeCompare(a.firstName);
           });
           const dataset = data.map((item,index) => {return {...item,seq:index + 1}})
           this.dataSet  = dataset;

         } else if (this.sortKey === 'date') {
          const data = results.sort(function (a, b) {
             return b.date.localeCompare(a.date);
           });
           const dataset = data.map((item,index) => {return {...item, seq:index + 1}})
           this.dataSet  = dataset;

         } else {
          const data = results.sort((a, b) => b[this.sortKey] - a[this.sortKey]);
          const dataset = data.map((item,index) => {return {...item,seq:index + 1}})
          this.dataSet  = dataset;
         }

       }
       this.sortKey = '';
       this.sortValue = '';
       this.dataSearch = this.dataSet;
       var number = Object.keys(dataResult).length;
       this.total = number;
     } catch (error) { }
   }
  }

  setValidatorId13(){
    this.formPnd1Input.controls.id13.setValidators([Validators.required,Pnd1CustomValidator.E02PND1009,Pnd1CustomValidator.E02PND1018,Pnd1CustomValidator.E02PND1012(this.dataSet),Pnd1CustomValidator.ID13,Validators.maxLength(13)]);
    this.formPnd1Input.controls.id13.updateValueAndValidity();
  }

  checkAll(value: boolean): void {
    this.dataDelete = [];
    this.checkDelete = (value == true)? true : false;
    this.dataSet.forEach(data => data.checked = value);
    if(value == true){
      this.dataSet.forEach((v, k) => {
        this.dataDelete.push(v.id13)
      })
    }else{
     this.dataDelete = [];
    }
    console.log(this.dataDelete)
    // this.refreshStatus(0);
  }

  refreshStatus(value): void {
    var  index = this.dataDelete.filter(obj => obj == value);
    if(index.length > 0){
      var  indexs = this.dataDelete.findIndex(obj => obj == value);
      this.dataDelete.splice(indexs, 1);
    }else{
      this.dataDelete.push(value)
    }
    const allChecked   = this.dataSet.every(value => value.checked === true);
    const allUnChecked = this.dataSet.every(value => !value.checked);
    this.allChecked    = allChecked;
    this.indeterminate = (!allChecked) && (!allUnChecked);
    this.checkDelete   = (this.dataDelete.length > 0)? true : false;
  }

  refreshStatus2(): void {
    this.checkDelete   = (this.dataDelete.length > 0)? true : false;
    this.indeterminate = false;
    this.allChecked    = false;
  }

  onSelectedTab(index: number) {
    this.tabSelected = index;
    this.searchData(true,index);
    this.formPnd1Input.reset();
    this.formSubmitAttempt = false;
    var numberIncom  = new String(this.dataSummary[0].Income)
    var numberTax    = new String(this.dataSummary[0].Tax)  
    this.checkIncome = (numberIncom.length > 10)? true : false;
    this.checkTex    = (numberTax.length > 10)? true : false;
    this.payError    = false;
    this.taxError    = false;
  }

  setData1Form(jsonData) {
    console.log('====>>>',jsonData.date)
    var newDate = new Date(jsonData.date);
       jsonData.date = newDate;
       jsonData.pay  = this.decPipe.transform(jsonData.pay, '1.2-2') //parseFloat(jsonData.pay).toFixed(2);
       jsonData.tax  = this.decPipe.transform(jsonData.tax, '1.2-2') //parseFloat(jsonData.tax).toFixed(2);
       jsonData.con  = parseInt(jsonData.con, 10)
    this.formPnd1Input.setValue(jsonData);
  }

  onSubmit() {
    this.dataInput = this.formPnd1Input.value;
    this.dataInput.midName = (this.dataInput.midName === null)? "" : this.dataInput.midName;
    // this.dataInput.id10 = this.formPnd1.value.pnd1CompleteForm.formDetail.taxPayerInfo.id10;
    this.dataInput.date =  moment(this.formPnd1Input.controls['date'].value).format('YYYY-MM-DD');
    this.dataInput.pay  = parseFloat(this.formPnd1Input.controls['pay'].value).toFixed(2);
    this.dataInput.tax  = parseFloat(this.formPnd1Input.controls['tax'].value).toFixed(2);
    console.log('===>',this.dataInput)
    if (this.formPnd1Input.dirty) {
      if (this.formPnd1Input.valid == true) {
        const results = this.titleOption.filter(data => data.titleName === this.dataInput.titleName)
        this.dataInput.titleCode = results[0].titleCode;
        let result = this.formPnd1.get('pnd1AttachForm.formAttach.attDet').value.filter(x => x.type == this.tabType);
        if (result.length > 0) {
          if (this.dataInput.seq == null) {
            this.dataInput.seq = this.formPnd1.get('pnd1AttachForm.formAttach.attDet').value[this.dataTab].attData.length + 1;
            this.formPnd1.get('pnd1AttachForm.formAttach.attDet').value[this.dataTab].attData.push(this.dataInput);
            this.refreshData();
          } else {
            this.formPnd1.get('pnd1AttachForm.formAttach.attDet').value[this.dataTab].attData[this.dataInput.seq - 1] = this.dataInput;
            this.refreshData();
          }
        } else {
          this.dataInput.seq = 1;
          this.formPnd1.get('pnd1AttachForm.formAttach.attDet').value.push({ type: this.tabType, attData: [this.dataInput] })
          this.refreshData();
        }
        this.formPnd1Input.reset();
        this.payError = false;
        this.taxError = false;
        this.formSubmitAttempt = false;
      }else{
         this.formSubmitAttempt = true;
         this.payError = true;
        this.taxError = true;
      }
    }
  }

  refreshData(){
    try {
      let result = this.formPnd1.get('pnd1AttachForm.formAttach.attDet').value.filter(x => x.type == this.tabType);
      var dataResult = (result.length > 0) ? result[0].attData : result;
      this.dataSet = dataResult.slice((this.pageIndex - 1) * this.pageSize, this.pageSize * this.pageIndex);
      this.dataSearch = this.dataSet;
      var  number = Object.keys(dataResult).length;
      this.total  = number;
      this.loading = false;
      this.setValidatorId13();
    } catch (error) { }
    try {
      var result = this.formPnd1.get('pnd1AttachForm.formAttach.attDet').value.filter(x => x.type == this.tabType)[0].attData;
      var Total = result.length;
      var Income = result.map(item => Number(item.pay)).reduce((a, b) => a + b);
      var Tax = result.map(item => Number(item.tax)).reduce((a, b) => a + b);
      this.dataSummary = [{ Total: Total, Income: Income, Tax: Tax }]
      var numberIncom  = new String(this.dataSummary[0].Income)
      var numberTax    = new String(this.dataSummary[0].Tax)  
      this.checkIncome = (numberIncom.length > 10)? true : false;
      this.checkTex    = (numberTax.length > 10)? true : false;
      this.formPnd1.get('pnd1CompleteForm.formDetail.taxDetail.totIncome').setValue(Income);
      this.formPnd1.get('pnd1CompleteForm.formDetail.taxDetail.totNum').setValue(Total);
    } catch (error) {
      this.dataSummary = [{ Total: '-', Income: '-', Tax: '-' }]
    }
    try {
      var total = [];
      this.formPnd1.get('pnd1AttachForm.formAttach.attDet').value.forEach(option => {
        option.attData.forEach(data => {
          total.push(data)
        });
      });
      var  totIncome = total.map(item => Number(item.pay)).reduce((a, b) => a + b);
      var  number = Object.keys(total).length;
      this.formPnd1.get('pnd1CompleteForm.formDetail.taxDetail.totIncome').setValue(totIncome);
      this.formPnd1.get('pnd1CompleteForm.formDetail.taxDetail.totNum').setValue(number);
    } catch (error) {}
  }

  showModalClearData(templateClearData : TemplateRef<any>){
    this.alertHeader = 'แจ้งเตือน';
    this.msgBody = 'ท่านต้องการล้างข้อมูลที่กรอกไว้ ใช่หรือไม่ ?';
    this.modalClearData = this.modalService.show(templateClearData);
    console.log('forminput',this.formPnd1Input.dirty)
  }

  showModalDeleteAllData(templateDeleteAllData : TemplateRef<any>){
    this.alertHeader = 'แจ้งเตือน';
    this.msgBody = 'ท่านต้องการลบรายการทั้งหมดที่เลือก ใช่หรือไม่?';
    this.modalDeleteAllData = this.modalService.show(templateDeleteAllData);
  }

  showModalTaxPay(templateTaxPay: TemplateRef<any>){
    this.alertHeader = 'แจ้งเตือน';
    this.msgBody = 'จำนวนเงินได้ที่จ่ายครั้งเท่ากับ 0.00 หมายถึงไม่มีเงินได้ ท่านต้องการที่จะดำเนินการต่อหรือไม่ ?';
    this.payModal = this.modalService.show(templateTaxPay);
  }

  onClearData() {
    this.formPnd1Input.reset();
    this.setValidatorId13();
    this.modalClearData.hide();
    this.payError = false;
    this.taxError = false;
    this.formSubmitAttempt = false;
  }

  onSearch() {
    const inputSearch = this.formSearch.controls["inputSearch"].value;
    if(this.formSearch.valid === true && inputSearch !== null){
      var dataSearch = this.dataSearch.filter(ojb => ojb.id13 == inputSearch)
      this.dataSet = dataSearch;
      this.searchAll = true;
      this.formSearch.controls["inputSearch"].setValue(null);
    }else{
      this.dataSet = this.dataSearch;
      this.searchAll = false;
      this.formSearch.controls["inputSearch"].setValue(null);
    }
  }

  onEdit(id13: any) {
    this.formPnd1Input.controls.id13.setValidators([Validators.required,Pnd1CustomValidator.E02PND1009,Pnd1CustomValidator.E02PND1018,Pnd1CustomValidator.ID13,Validators.maxLength(13)]);
    this.formPnd1Input.controls.id13.updateValueAndValidity();
    const result = this.dataSet.filter(data => data.id13 === id13);
     if(result.length > 0){
      this.setData1Form(result[0]);
     }
  }

  onDelete(value) {
    if(value != 0){
       var  index = this.formPnd1.get('pnd1AttachForm.formAttach.attDet').value.filter(x => x.type == this.tabType)[0].attData.findIndex(obj => obj.id13 == value);
       this.formPnd1.get('pnd1AttachForm.formAttach.attDet').value.filter(x => x.type == this.tabType)[0].attData.splice(index, 1);
       this.formPnd1.get('pnd1AttachForm.formAttach.attDet').value.filter(x => x.type == this.tabType)[0].attData = this.formPnd1.get('pnd1AttachForm.formAttach.attDet').value.filter(x => x.type == this.tabType)[0].attData.map((item,index) => {
        return {
          ...item,
          seq:index + 1
        }
      })
      // this.searchData();
      this.refreshData();
      this.refreshStatus2();
    }else{
      // this.dataDelete.sort();
      this.dataDelete.forEach((v, k) => {
        var  index = this.dataSet.findIndex(obj => obj.id13 == v);
        console.log(index);
        var  number = Object.keys(this.dataDelete).length;
        this.formPnd1.get('pnd1AttachForm.formAttach.attDet').value.filter(x => x.type == this.tabType)[0].attData.splice(index, number);
        this.dataDelete = [];
      })
      this.formPnd1.get('pnd1AttachForm.formAttach.attDet').value.filter(x => x.type == this.tabType)[0].attData = this.formPnd1.get('pnd1AttachForm.formAttach.attDet').value.filter(x => x.type == this.tabType)[0].attData.map((item,index) => {
        return {
          ...item,
          seq:index + 1
        }
      })
      // this.searchData();
      this.modalDeleteAllData.hide();
      this.loading = true;
      this.refreshData();
      this.refreshStatus2();
    }
    // this.refreshStatus(0);
  }

  onUpdateData(){}

  onPrint() {
    this.onClickPrintDraft.emit('P')
  }

  /* private focusElement(_formGroup: FormGroup) {
    for (const field in _formGroup.controls) {
        if (_formGroup.get(field).invalid) {
            switch (field) {
                case 'moduleNameTh':
                    this.infoId.nativeElement.focus();
                    break;
            }
            return;
        }
    }
  } */
}
