export interface ServerErrors {
    code?: number;
    fieldErrors?: [ErrorDetails];
    globalErrors?: [ErrorDetails];
    stateTracking?: string;
}

interface ErrorDetails {
    errorCode?: string;
    errorMessage?: string;
    field?: string;
    rejectedValue?: any;
}