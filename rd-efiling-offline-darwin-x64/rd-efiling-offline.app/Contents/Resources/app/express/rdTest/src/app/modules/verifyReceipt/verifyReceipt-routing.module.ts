import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VerifyReceiptComponent } from './pages/verifyReceipt/verifyReceipt.component';
import { VerifyReceiptSearchComponent } from './pages/searchReceipt/verifyReceiptSearch.component';

const routes: Routes = [
    {
        path: ':otpRef',
        component: VerifyReceiptComponent,
        data: {
            title: 'ยื่นแบบ'
        }
    },
    {
        path: '',
        component: VerifyReceiptSearchComponent,
        data: {
            title: 'ยื่นแบบ'
        }

    }
];  

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    declarations: [],
    exports: [RouterModule]
})
export class VerifyReceiptRoutingModule { }
