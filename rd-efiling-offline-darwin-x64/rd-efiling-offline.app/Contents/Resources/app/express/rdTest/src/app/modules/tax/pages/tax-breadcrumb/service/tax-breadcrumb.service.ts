import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TaxBreadcrumbService {

  private _stepper: BehaviorSubject<number> = new BehaviorSubject<number>(1);
  private _breadcrumb: ITaxBreadcrumb[] = [];

  breadcrumb$ = new BehaviorSubject<ITaxBreadcrumb[]>([]);

  constructor() { }

  public getThisStepperValue(): number {
    return this._stepper.value;
  }

  public get stepper(): Observable<number> {
    return this._stepper.asObservable();
  }

  public updateStepper(_step: number) {
    this._stepper.next(_step);
  }

  public nextStep() {
    if (this._stepper.value < this._breadcrumb.length) {
      this._stepper.next(this._stepper.value + 1);
    }
  }

  public backStep() {
    if (this._stepper.value > 1) {
      this._stepper.next(this._stepper.value - 1);
    }
  }

  public get breadcrumb(): ITaxBreadcrumb[] {
    return this._breadcrumb;
  }

  public set breadcrumb(breadcrumb: ITaxBreadcrumb[]) {
    this._breadcrumb = breadcrumb;
    this.breadcrumb$.next(this._breadcrumb)
  }

  public nextStepAndSkipStep(skip : number) {
    if (this._stepper.value < this._breadcrumb.length) {
      this._stepper.next(this._stepper.value + skip);
    }
  }

  public backStepAndSkipStep(skip : number) {
    if (this._stepper.value > 1) {
      this._stepper.next(this._stepper.value - skip);
    }
  }
}

export interface ITaxBreadcrumb {
  title: string;
  icon: string;
  stepper: number;
}
