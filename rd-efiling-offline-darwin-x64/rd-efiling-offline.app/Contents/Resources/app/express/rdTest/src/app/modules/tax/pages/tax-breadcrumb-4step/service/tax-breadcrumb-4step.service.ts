import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { ITaxBreadcrumb } from '../../tax-breadcrumb/service/tax-breadcrumb.service';

@Injectable()
export class TaxBreadcrumb4StepService {

  private _stepper: BehaviorSubject<number> = new BehaviorSubject<number>(1);
  private _breadcrumb: ITaxBreadcrumb[] = [];

  constructor() { }

  public getThisStepperValue(): number {
    return this._stepper.value;
  }

  public get stepper(): Observable<number> {
    return this._stepper.asObservable();
  }

  public updateStepper(_step: number) {
    this._stepper.next(_step);
  }

  public nextStep() {
    if (this._stepper.value < this._breadcrumb.length) {
      this._stepper.next(this._stepper.value + 1);
    }
  }

  public backStep() {
    if (this._stepper.value > 1) {
      this._stepper.next(this._stepper.value - 1);
    }
  }

  public get breadcrumb(): ITaxBreadcrumb[] {
    return this._breadcrumb;
  }

  public set breadcrumb(breadcrumb: ITaxBreadcrumb[]) {
    this._breadcrumb = breadcrumb;
  }
}
