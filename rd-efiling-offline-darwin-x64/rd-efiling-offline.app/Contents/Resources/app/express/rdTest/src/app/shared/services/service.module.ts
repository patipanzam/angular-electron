import { NgModule } from '@angular/core';
import { HttpModule, JsonpModule } from '@angular/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtModule } from '@auth0/angular-jwt';

import { AuthService } from '../../auth/auth.service';
import { AuthGuard } from '../../auth/auth.guard';
import { UserService } from '../../auth/user.service';

import { RefreshTokenInterceptor } from './refresh-token.interceptor';
import { ConnectionServiceInterceptor } from './connection-service.interceptor';
import { TranslateInterceptor } from './translate.interceptor';
import { AuthInterceptor } from './auth.interceptor';

import * as Config from '../../config/constants';

export function tokenGetter() {
  return localStorage.getItem(Config.TOKEN_KEY);
}

@NgModule({
  imports: [
    HttpModule,
    JsonpModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: [],
        blacklistedRoutes: []
      }
    })
  ],
  providers: [
    AuthService,
    AuthGuard,
    UserService,{
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }, {
      provide: HTTP_INTERCEPTORS,
      useClass: TranslateInterceptor,
      multi: true
    }, {
      provide: HTTP_INTERCEPTORS,
      useClass: RefreshTokenInterceptor,
      multi: true
    }, {
      provide: HTTP_INTERCEPTORS,
      useClass: ConnectionServiceInterceptor,
      multi: true
    }
  ]
})
export class ServiceModule { }
