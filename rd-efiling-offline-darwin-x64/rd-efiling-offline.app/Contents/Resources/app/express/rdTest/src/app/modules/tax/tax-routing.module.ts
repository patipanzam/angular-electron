import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TaxComponent } from './pages/tax-component/tax.component';
import { TaxDashboardComponent } from './pages/tax-dashboard/tax-dashboard.component';
import { TaxMenuResolve } from './pages/tax-dashboard/service/tax-menu.resolve';
import { UserResolve } from './pages/tax-dashboard/service/tax.user.resolve';
import { TaxMenuService } from './pages/tax-dashboard/service/tax-menu.service';
import { CommonSelectService } from './services/common-select.service';

const routes: Routes = [{
  path: '',
  component: TaxComponent,
  children: [{
    path: '',
    pathMatch: 'full',
    redirectTo: 'dashboard'
  }, {
    path: 'dashboard',
    data: { title: 'ยื่นแบบ' },
    resolve: { resolveData: TaxMenuResolve},
    component: TaxDashboardComponent
  },{
    path: 'wht',
    loadChildren: './pages/_wht/wht.module#WhtModule'
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
  providers: [TaxMenuService, UserResolve, TaxMenuResolve , CommonSelectService]
})
export class TaxRoutingModule { }
