import * as moment from 'moment';
import { AbstractControl, ValidatorFn } from '@angular/forms';

export function endDateNotLessThanStartDate(start: AbstractControl): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        return control.value < start.value ? { 'endDateLessThanStartDate': true } : null;
    };
}

export function E02PP36002(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        let start = control.get('startDate');
        let end = control.get('endDate');
        if (start && end) {
            let endTarget: Date = moment(start.value).add(12, 'months').endOf('month').toDate();
            return end.value >= endTarget ? { 'E02PP36002': true } : null;
        }
        return null;
    };
}