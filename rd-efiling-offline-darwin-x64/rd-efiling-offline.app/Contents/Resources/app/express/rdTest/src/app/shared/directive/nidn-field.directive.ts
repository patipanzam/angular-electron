import { Directive, ElementRef, HostListener, OnInit, Output, EventEmitter, Input, Optional } from '@angular/core';
import { ControlContainer } from '@angular/forms';

@Directive({
  selector: '[efNidnField]',
  exportAs: 'nidnfield',
  providers: []
})
export class NidnFieldDirective implements OnInit {
  @Output('onFail') onFail: EventEmitter<any> = new EventEmitter<any>();
  @Input('value') value: number = null;
  @Input('formControlName') valueFormControlName: string;

  constructor(private el: ElementRef, @Optional() private parent: ControlContainer) { }

  ngOnInit() { }

  @HostListener('blur', ['$event.target.value']) onBlur(value: string) {
    if (value) {
      if(value.length != 17) {
        if (this.parent.control.get(this.valueFormControlName)) {
          this.parent.control.get(this.valueFormControlName).reset();
          this.parent.control.get(this.valueFormControlName).markAsDirty();
        }
        this.el.nativeElement.value = '';
        this.onFail.emit();
      }
    }
  }

}
