import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { ValidationMessageConfig } from '../../../config/validation-message';
import { ValidationMessageConfigTh } from '../../../config/validation-message.th';
import { ValidationMessageConfigJp } from '../../../config/validation-message.jp';

@Injectable({
  providedIn: 'root'
})

export class ValidationMessageService {

  _LANG = { TH: 'th', EN: 'en', JP: 'jp' };

  constructor(private translateService: TranslateService) { }

  getErrorMsg(errorType: string): string {
    const defaultLang = this.translateService.getDefaultLang();
    var validationMessage = undefined;

    switch (defaultLang) {
      case this._LANG.TH:
        validationMessage = ValidationMessageConfigTh;
        break;

      case this._LANG.JP:
        validationMessage = ValidationMessageConfigJp;
        break;

      case this._LANG.EN:
        validationMessage = ValidationMessageConfig;
        break;

      default:
        validationMessage = ValidationMessageConfig;
        break;
    }
    return validationMessage[errorType];
  }
}
