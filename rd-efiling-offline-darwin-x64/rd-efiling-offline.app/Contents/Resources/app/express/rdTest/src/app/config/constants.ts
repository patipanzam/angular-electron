import { environment } from '../../environments/environment';

export const LOCAL_LANG_KEY = 'localLanguage';

export const MENU_KEY = 'efmenu';

export const VERIFY_OTP_TIMER = 15;

export const PASSWORD_REGEX = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,20}$/;
export const TEXT_FIELD_REGEX = /^[^\^|]+$/;

export const HTTP_TIME_OUT = 10000; // millisecond

export const TOKEN_KEY = 'token';
export const REFRESH_TOKEN_KEY = 'refresh_token';

export const API_EPAYMENT = environment.epaymentUrl;
// export const API_EPAYMENT = 'http://10.11.2.25:8770/rd-epayment-service/epay/';
//export const API_EPAYMENT = 'http://localhost:8770/epay/';

export const MONTH_LIST = [
  { value: '01', name: 'มกราคม' },
  { value: '02', name: 'กุมภาพันธ์' },
  { value: '03', name: 'มีนาคม' },
  { value: '04', name: 'เมษายน' },
  { value: '05', name: 'พฤษภาคม' },
  { value: '06', name: 'มิถุนายน' },
  { value: '07', name: 'กรกฏาคม' },
  { value: '08', name: 'สิงหาคม' },
  { value: '09', name: 'กันยายน' },
  { value: '10', name: 'ตุลาคม' },
  { value: '11', name: 'พฤศจิกายน' },
  { value: '12', name: 'ธันวาคม' }
];

export const AUTH_KEY = '123';

// profile
export const API_PROFILE_URL = environment.profileUrl;
// export const API_PROFILE_URL = `http://10.11.2.26:8773/rd-profile-service`;
//export const API_PROFILE_URL = `http://localhost:8773`;

// master
export const API_MASTER_URL = environment.masterUrl;
// export const API_MASTER_URL = `http://10.11.2.24:8789/rd-master-service`;

//register
export const API_REGISTER_URL = environment.registerUrl;
// export const API_REGISTER_URL = `http://10.11.2.26:8772/rd-register-service`;
// export const API_REGISTER_URL = `http://localhost:8772/rd-register-service`;

//objectstorage - mongoDB
export const API_OBJECT_STORAGE_URL = environment.objectStorageUrl;
//export const API_OBJECT_STORAGE_URL = `http://10.11.2.25:8771/rd-object-storage-service`;
