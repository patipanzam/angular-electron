import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

// import { MainMenuModel } from 'src/app/modules/home/models/main-menu.model';
// import { PaymentService } from '../../modules/e-payment/services/payment.service';
import { NotificationService } from '../../../app/shared/services/notification.service';

@Component({
  selector: 'ef-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit, OnDestroy {

  @Input('menuList') menuList: MainMenuModel[];

  // uuid: string = this.paymentService.uuid;

  constructor(
    // private paymentService: PaymentService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private notificationService: NotificationService
  ) {
    this.activatedRoute.queryParams.subscribe(params => {
      // this.paymentService.uuid = params['this.uuid'];
    });
  }

  ngOnInit() {
    this.setActiveMenu();
    // this.notificationService.pollingNotification();
  }

  ngOnDestroy() {
    // this.notificationService.stopPollingNotification();
  }

  onSelectedMenu(menu: MainMenuModel) {
    this.setActiveMenu(menu);

    console.log('onSelectedMenu -> ', menu);
    this.router.navigate([menu.linkUrl]);
  }

  private setActiveMenu(menu?: MainMenuModel) {
    if (menu) {
      this.menuList.map(value => value.isActive = false);
      menu.isActive = true;
    } else {
      this.menuList.map((menu) => {
        if (menu.linkUrl === '/'.concat(this.router.url.split('/')[1])) {
          menu.isActive = true;
        }
      });
    }
  }
}


export class MainMenuModel {
  menuId: number;
  menuNameEn: string;
  menuNameTh: string;
  activeStatus: string;
  parentId: number;
  levelNo: number;
  linkUrl: string;
  orderNo: number;
  userLevelMin: string;
  createBy: number;
  createDate: string;
  updateBy: number;
  updateDate: string;
  isActive: boolean = false;
}