import { FormGroup, FormBuilder, Validators, AbstractControl, Validator, ValidatorFn, FormControl } from '@angular/forms';
import { DateLocalThPipe } from '../../../../../../shared/pipes/date-local-th.pipe';

export class Pnd1CustomValidator {

  /** check validator Id13 */

  // ตรวจสอบ Digit โดยใช้ตัวเลขตัวสุดท้าย
  static E02PND1009(control: AbstractControl) {
    if (control.value) {
    for (var i = 0, sum = 0; i < 12; i++) {

        sum += parseFloat(control.value.charAt(i)) * (13 - i);
    }
    if ((11 - sum % 11) % 10 != parseFloat(control.value.charAt(12))) {
        if (control.value.charAt(12)){
        return {
           E02PND1009: true
        };
    }}
  }
    return null;
  }

  // เลขประจำตัวผู้เสียภาษีอากรของผู้มีเงินได้จะต้องไม่ขึ้นต้นด้วย 0 หรือ 099 ยกเว้น 0991
  static E02PND1018(control: AbstractControl){
    var re = new RegExp(/^099\d{0}[^1]/g)
    var te = new RegExp(/^0\d{0}[^9]/g)
      if(re.test(control.value)){

        return {E02PND1018: true};
      }
      if(te.test(control.value)){

        return {E02PND1018: true};
      }
      return null;
  }

  //validate id13 massage
  static ID13(control: AbstractControl){
    const varlue = control.value;
      if(control.value != null && varlue.length > 0 && varlue.length < 13){
        return {id13: true};
      }
      return null;
  }

  // เลขประจำตัวผู้เสียภาษีของผู้มีหน้าที่หัก ณ ที่จ่าย สามารถซ้ำกับเลขประจำตัวผู้เสียภาษีของผู้ถูกหักภาษี (ผู้มีเงินได้)
  // static E02PND1012(control: AbstractControl):any{
  //   var value: string = control.value;
  //   return null;
  // }


  // เลขประจำตัวผู้เสียภาษีของผู้มีหน้าที่หัก ณ ที่จ่าย ไม่สามารถซ้ำกับเลขประจำตัวผู้เสียภาษีของผู้ถูกหักภาษี (ผู้มีเงินได้)
  static I02PND1002(control: AbstractControl):any{
    var value: string = control.value;
    return null;
  }

  //เลขประจำตัวผู้เสียภาษีของผู้มีเงินได้ไม่สามารถซ้ำกันได้ ในประเภทเงินได้เดียวกัน
  static E02PND1012(s : any): ValidatorFn{
    return (control: AbstractControl): { [key: string]: boolean } | null => {
      var some = [];
      s.forEach(data => {
        some.push(data.id13)
      });
      var even = function (element) {
        return element === control.value;
      };
      var result = some.some(even)
      if (control.value !== undefined && result === true) {
        return { E02PND1012: true };
      }
      return null;
    };
  }

  //วัน-เดือน-ปี ที่จ่ายเงินได้ จะต้องไม่มากกว่าวันที่เข้าทำรายการ
  static E02PND1001(control: AbstractControl) {
    var date = new Date
    var pipe = new DateLocalThPipe();
    var dateNow = pipe.transform(date);
    var dateSelect = pipe.transform(control.value);
    if (dateSelect != null) {
      var day1 = dateNow.substr(0, 2).replace("/", '')
      var day2 = dateSelect.substr(0, 2).replace("/", '')
      var month1 = dateNow.substr(-7, 2).replace("/", '')
      var month2 = dateSelect.substr(-7, 2).replace("/", '')
      var year1 = dateNow.substr(-4, 7).replace("/", '')
      var year2 = dateSelect.substr(-4, 7).replace("/", '')
    }
    if (parseInt(year2, 10) < parseInt(year1, 10)) {
      return null;
    } else if (parseInt(year2, 10) === parseInt(year1, 10)) {
      if (parseInt(month2, 10) >= parseInt(month1, 10) && parseInt(day2, 10) > parseInt(day1, 10)) {
        return { E02PND1001: true };
      } else {
        return null;
      }
    } else  if(parseInt(year2, 10) > parseInt(year1, 10)){
      return { E02PND1001: true };
    }
    return null;
  }

  //ระบบตรวจสอบเดือนที่จ่ายจะต้องตรงกับเดือนภาษีที่รายการ
  static E02PND1013(s : number): ValidatorFn{
    return (control: AbstractControl): { [key: string]: boolean } | null => {
      var pipe = new DateLocalThPipe();
      var date1 = pipe.transform(control.value);
      var newMonth = '';
      if(date1 != null){
        newMonth = (date1 != "Invalid Date")?date1.substr(-7, 2).replace("/",'') : s;
      }
      if (control.value !== undefined &&(parseInt(newMonth, 10) !== s)){
          return { E02PND1013: true };
      }
      return null;
    };
  }

  //ระบบตรวจสอบปีที่จ่ายจะต้องตรงกับปีภาษีที่รายการ
  static E02PND1015(s : number): ValidatorFn{
    return (control: AbstractControl): { [key: string]: boolean } | null => {
      var pipe = new DateLocalThPipe();
      var date1 = pipe.transform(control.value);
      var year = '';
      if(date1 != null){
        year = (date1 != "Invalid Date")?date1.substr(-4,7).replace("/",'') : s;
      }
      if (control.value !== undefined && control.value !== "Invalid Date" &&(parseInt(year, 10) !== s)){
          return { E02PND1015: true };
      }
      return null;
    };
  }

  //กรณีเป็นแบบยื่นปกติ จำนวนเงินได้จะต้องมีค่ามากกว่า 0.00
  static E02PND1026(control: AbstractControl){
    if(parseInt(control.value, 10) < 0){
      return {E02PND1026: true};
    }
    return null;
  }
  
  //กรณีเป็นแบบยื่นปกติ จำนวนเงินได้ที่จ่ายจะต้องมีค่ามากกว่าจำนวนเงินภาษีที่หักและนำส่ง
  static E02PND1027(s : any): ValidatorFn{
    return (control: AbstractControl): { [key: string]: boolean } | null => {
      if (control.value !== undefined && (parseInt(control.value, 10) <= parseInt(s, 10))){
          return { E02PND1027: true };
      }
      return null;
    };
  }

  //กรณีเป็นแบบยื่นปกติ จำนวนเงินได้จะต้องมีค่ามากกว่า 0.00
  static E02PND1028(control: AbstractControl){
    if(parseInt(control.value, 10) < 0){
      return {E02PND1028: true};
    }
    return null;
  }



 //กรณีเป็นแบบยื่นปกติ จำนวนเงินภาษีที่หักและนำส่งจะต้องมีตั้งแต่ 0.00 แต่ไม่เกินจำนวนเงินได้ที่จ่าย
  static E02PND1030(s : any): ValidatorFn{
    return (control: AbstractControl): { [key: string]: boolean } | null => {
      if (control.value !== undefined && (parseInt(control.value, 10) > parseInt(s, 10))){
          return { E02PND1030: true };
      }
      return null;
    };
  }

  //กรณีเป็นแบบยื่นเพิ่มเติมและจำนวนเงินได้มีค่าเท่ากับ 0.00
  static I02PND1004(control: AbstractControl){
    var value: string = control.value;
    return null;
  }

  //กรณีเป็นแบบยื่นปกติ จำนวนเงินได้จะต้องมีค่ามากกว่า หรือ เท่ากับ 0.00
  static E02PND1029(control: AbstractControl){
    if(parseInt(control.value, 10) < 0){
      return {E02PND1029: true};
    }
    return null;
  }

  //ไม่สามารถใส่ค่าเงินได้เท่ากับ 0.00 และภาษีที่หักและนำส่งมีค่าเท่ากับ 0.00 พร้อมกันได้
  static E02PND1031(s : any): ValidatorFn{
    return (control: AbstractControl): { [key: string]: boolean } | null => {
      if (control.value !== undefined && (parseInt(control.value, 10) === 0 && parseInt(s, 10) === 0)){
          return { E02PND1031: true };
      }
      return null;
    };
  }

  // ครั้งที่ยื่นแบบไม่ถูกต้อง
  static E02PND1007(s: any): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
      var even = function (element) {
        return element === parseInt(control.value, 10);
      };
      var result = s.some(even)
      if (control.value !== undefined && result === true) {
        return { E02PND1007: true };
      }
      return null;
    };
  }

  // ครั้งที่ที่ต้องการยื่นแบบจะต้องมีข้อมูล
  static E02PND1008(s: any): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
      if (control.value === '' && s === '1') {
        return { E02PND1008: true };
      }
      return null;
    };
  }

//ชื่อของผู้มีเงินได้ ไม่สามารถมีอักขระพิเศษ หรือตัวเลขได้ 
  static E02PND1023(control: AbstractControl){
    console.log('0')
    var re = new RegExp(/\W/g)
    if(re.test(control.value)){
      console.log('1')
      return {E02PND1023: true};
    }
    return null;
  }
}
