import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ReCaptcha2Component } from 'ngx-captcha';
import { RegisterVerifyService } from '../../services';

@Component({
  selector: 'ef-check-request',
  templateUrl: './check-request.component.html',
  styleUrls: ['./check-request.component.scss']
})
export class CheckRequestComponent implements OnInit {
  inputCheck: boolean = false;
  isCaptchaShow: boolean = true;
  isCaptchaSuccess: boolean = false;
  refNoForm: FormGroup;
  refNo: FormControl;
  recaptcha: FormControl;
  @ViewChild('captcha') captcha: ReCaptcha2Component;

  constructor(private fb: FormBuilder, private router: Router, private registerService:RegisterVerifyService) {
    this.recaptcha = this.fb.control('', Validators.required);
    this.refNo = this.fb.control('', [ Validators.required, Validators.maxLength(20)]);


    this.refNoForm = this.fb.group({
      refNo: this.refNo,
      recaptcha: this.recaptcha
    });

  }
  captchaSuccess(_captcha: string) {
    this.isCaptchaSuccess = true;
    this.recaptcha.setValue(_captcha);
    this.recaptcha.updateValueAndValidity();
  }

  captchaExpire() {
    this.isCaptchaSuccess = false;
    this.recaptcha.setValue('');
    this.recaptcha.updateValueAndValidity();
  }

  onSubmit() {
    if (this.refNoForm.valid) {
      this.registerService.CheckStatus( this.refNo.value).subscribe(res =>{
        this.router.navigate(['/verify-request/list',this.refNo.value]);
      });
      
    }
  }

  clearForm(){
    this.refNoForm.reset();
    this.isCaptchaSuccess = false;
    this.captcha.reloadCaptcha();

  }

  ngOnInit() {
  }

}
