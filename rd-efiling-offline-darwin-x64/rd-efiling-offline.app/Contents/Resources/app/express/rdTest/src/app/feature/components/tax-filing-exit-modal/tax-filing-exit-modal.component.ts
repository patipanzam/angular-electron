import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
    selector: 'ef-tax-filing-exit-modal',
    templateUrl: './tax-filing-exit-modal.component.html',
    styleUrls: ['./tax-filing-exit-modal.component.scss']
})
export class TaxFilingExitModalComponent implements OnInit {

    @ViewChild('exitModal') exitModal: ModalDirective;

    constructor(private router: Router) { }

    ngOnInit() {
    }

    show() {
        this.exitModal.show();
    }

    exit() {
        console.log('exit');
        this.router.navigate(['/tax/dashboard']);
    }

}
