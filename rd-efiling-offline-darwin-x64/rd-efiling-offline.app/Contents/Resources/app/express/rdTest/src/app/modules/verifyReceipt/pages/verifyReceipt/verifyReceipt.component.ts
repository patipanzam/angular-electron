import { Component, OnInit, Input, ViewChild, ElementRef, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ReceiptService } from '../../services/receipt.service';
import { Receipt } from '../../models/receipt.model';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'ef-verifyReceipt',
  templateUrl: './verifyReceipt.component.html',
  styleUrls: ['./verifyReceipt.component.scss']
})
export class VerifyReceiptComponent implements OnInit {
  otpRefCode: String;
  verifyCode: String = "0101010101";

  isSuccess: boolean = false;
  isEnable: boolean = false;
  data: Receipt = {};
  currentDate: Date;
  modalRef: BsModalRef;
  taxYear: number;
  @ViewChild('modalTemplate') template: TemplateRef<any>;
  constructor(private modalService: BsModalService, private receiptService: ReceiptService, private route: ActivatedRoute) {
    modalService.config.keyboard = false;
    modalService.config.backdrop = 'static';
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  closeModal() {
    window.location.href = '/';
  }

  submitReceiptNo(receiptNo: String) {
    //verify
    console.log(receiptNo);
    //close modal
    this.modalRef.hide();

    let scope = this;
    this.receiptService.receiptLookupByQR(this.otpRefCode, receiptNo).then(function (data: Receipt) {
      console.log(data);
      scope.data = data;
      scope.taxYear = Number(data.taxYear) + 543;
      if (data.receiptNo != null) {
        scope.isSuccess = true;
      }
      scope.isEnable = true;
    });

  }

  ngOnInit() {
    this.currentDate = new Date();
    this.currentDate.setFullYear(this.currentDate.getFullYear()+543);
    
    this.route.params.subscribe(params => {
      this.otpRefCode = params['otpRef'];
      if(this.otpRefCode == 'Result'){
        console.log(this.receiptService._data);
        this.data = this.receiptService._data;
        if (this.data.receiptNo != null) {
          this.isSuccess = true;
        }
        this.isEnable = true;
      }else{
        this.openModal(this.template);
      }
    });
  }

}
