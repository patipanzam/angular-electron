import { ToDatePipe } from './to-date.pipe';

describe('ToDatePipe', () => {
  let pipe : ToDatePipe;

  it('create an instance', () => {
    pipe = new ToDatePipe();
    expect(pipe).toBeTruthy();
  });

  var dStr = "13/02/2019";
  var dPtt = "DD/MM/YYYY";
  it('pip Date str : ' + dStr + ", ptt: " + dPtt, () => {
    const dto = pipe.transform(dStr, dPtt);
    console.log(dto);
    // console.log(new Date().toLocaleDateString())

    const expectResult =  dto instanceof Date;
    expect(expectResult).toEqual(true);
    
  });

  var dStr = "13-02-2019";
  var dPtt = "DD-MM-YYYY";
  it('pip Date str : ' + dStr + ", ptt: " + dPtt, () => {
    const dto = pipe.transform(dStr, dPtt);
    console.log(dto);
    // console.log(new Date().toLocaleDateString())

    const expectResult =  dto instanceof Date;
    expect(expectResult).toEqual(true);

  });

  var dStr = "13022019";
  var dPtt = "DDMMYYYY";
  it('pip Date str : ' + dStr + ", ptt: " + dPtt, () => {
    const dto = pipe.transform(dStr, dPtt);
    console.log('result:', dto);
    // console.log(new Date().toLocaleDateString())

    const expectResult =  dto instanceof Date;
    expect(expectResult).toEqual(true);

  });

  var dStr = "20190212"
  var dPtt = "YYYYMMDD";
  it('pip Date str : ' + dStr + ", ptt: " + dPtt, () => {
    const dto = pipe.transform(dStr, dPtt);
    console.log('result:', dto);
    const expectResult =  dto instanceof Date;
    expect(expectResult).toEqual(true);

  });

  var dStr = "25620214";
  var dPtt = "YYYYMMDD";
  it('pip Date str : ' + dStr + ", ptt: " + dPtt, () => {
    const dto = pipe.transform(dStr, dPtt);
    console.log('result:', dto);
    const expectResult =  dto instanceof Date;
    expect(expectResult).toEqual(true);

  })
  
  var dStr = "02142562";
  var dPtt = "MMDDYYYY";
  it('pip Date str : ' + dStr + ", ptt: " + dPtt, () => {
    const dto = pipe.transform(dStr, dPtt);
    console.log('result:', dto);
    const expectResult =  dto instanceof Date;
    expect(expectResult).toEqual(true);
  });

  var dStr = "15/04/2562";
  var dPtt = "DD/MM/YYYY";
  it('pip Date str : ' + dStr + ", ptt: " + dPtt, () => {
    const dto = pipe.transform(dStr, dPtt);
    console.log('result:', dto);
    const expectResult =  dto instanceof Date;
    expect(expectResult).toEqual(true);
  });
  
  
  it('pip Date str : ' + new Date(), () => {
    const dto = pipe.transform(new Date());
    console.log('result:', dto);
    const expectResult =  dto instanceof Date;
    expect(expectResult).toEqual(true);
  });
  
  var dStr = "1991-04-12";
  var dPtt = "YYYY-MM-DD";
  it('pip Date str : ' + dStr + ", ptt: " + dPtt, () => {
    const dto = pipe.transform(dStr, dPtt);
    console.log('result:', dto);
    const expectResult =  dto instanceof Date;
    expect(expectResult).toEqual(true);
  });

  var dStr = "2019-03-05T12:08:56.235";
  var dPtt = "YYYY-MM-DDTHH:mm:ss.SSS";
  it('pip Date str : ' + dStr + ", ptt: " + dPtt, () => {
    const dto = pipe.transform(dStr, dPtt);
    console.log('result:', dto);
    const expectResult =  dto instanceof Date;
    console.log('expectResult:', expectResult);
    expect(expectResult).toEqual(true);
  });
  
  var dStr = "2019-05-14T18:35:11.661";
  var dPtt = "YYYY-MM-DDTHH:mm:ss.SSS";
  it('pip Date str : ' + dStr + ", ptt: " + dPtt, () => {
    const dto = pipe.transform(dStr, dPtt);
    console.log('result:', dto);
    var expectResult : boolean =  dto instanceof Date;
    console.log('---expectResult dStr:', dStr);
    console.log('---expectResult:', dto);
    expectResult = dto.toDateString().includes("14");
    console.log('expectResult:', expectResult);
    expect(expectResult).toEqual(true);
  });



});
