import { District } from './district.model';
import { Province } from './province.model';
import { SubDistrict } from './sub-district.model';
import { Postal } from './postal.model';

export interface AddressOptions {
    provinces: Province[];
    districts: District[];
    subdistricts: SubDistrict[];
    postalCodes: Postal[];
}