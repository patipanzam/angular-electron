export interface Country {
    countryId: number,
    countryNameTh: string,
    countryNameEn?: string,
    countryCode: number,
    nationality: string,
    formalNationality: string
}

export function getCountryDefault() {
    return {
        countryId: undefined,
        countryNameTh: undefined,
        countryNameEn: undefined,
        countryCode: undefined,
        nationality: undefined,
        formalNationality: undefined
    }
}