export interface DownloadForm {
    docType: string;
    fileName: string;
    fileUrl: string;
}