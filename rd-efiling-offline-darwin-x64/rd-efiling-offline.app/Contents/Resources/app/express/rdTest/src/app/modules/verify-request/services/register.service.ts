import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders } from '@angular/common/http';
import { Observable, from, of } from 'rxjs';
import { ResponseReq } from '../../../shared/models/response-req';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RegisterVerifyService {
  private _checkStatusUrl: string = `${environment.registerUrl}/register/check-status-formPOR`;
  private _resendPorUrl: string = `${environment.registerUrl}/register/re-send-por`;
  constructor(private http: HttpClient) { }
  
  CheckStatus(_registerNo: string): Observable<any> {
    let _body = {
      registerNo: _registerNo
    }
    const _httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json; charset=utf-8',
        'noToken': 'noToken'
      })
    };
    var data = null;
 
    data = from(this.http.post<ResponseReq<any>>(encodeURI(this._checkStatusUrl),_body,_httpOptions).toPromise().then((res) => {
      
      return res.data;
    }));
    return data;

  }
  reSendEamil(_registerNo: string): Observable<any> {
    let _body = {
      registerNo: _registerNo
    }
    const _httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json; charset=utf-8',
        'noToken': 'noToken'
      })
    };
    var data = null;
 
    data = from(this.http.post<ResponseReq<any>>(encodeURI(this._resendPorUrl),_body,_httpOptions).toPromise().then((res) => {
      
      return res.data;
    }));
    return data;

  }
//  CheckStatus(_registerNo: string) {
//     let _body = {
//       registerNo: _registerNo
//     }

    
//     let promise = new Promise((resolve, reject) => {
//       let apiURL = this._checkStatusUrl;
//       this.http.post(apiURL,_body)
//         .toPromise()
//         .then(
//           res => { // Success
//             resolve(res);
//           }
//         );
//     });
//     return promise;
//   }
}
