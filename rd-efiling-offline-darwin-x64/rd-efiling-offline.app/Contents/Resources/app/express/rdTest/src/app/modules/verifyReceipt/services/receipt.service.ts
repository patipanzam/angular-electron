import { Injectable } from '@angular/core';
import { Receipt } from '../models/receipt.model';
import { VerifyReceiptRequest } from '../models/verify-receipt-request';
import { environment } from '../../../../environments/environment';
import { HttpClient , HttpParams} from '@angular/common/http';
import * as moment from 'moment';
@Injectable()
export class ReceiptService {
	
	public _data: Receipt;
	private _urlQR: string = 'http://localhost:8798/back-office/receipt/search/qr-refcode/';
	private _url: string = 'http://localhost:8798/back-office/receipt/search/web';

    private _email: string;

	constructor(private http: HttpClient) { }

	public async receiptLookupByQR(ref:String, receiptNo: String): Promise<Receipt> {
		
		return await this.http.get<Receipt>(this._urlQR+ref+"?receiptNo="+receiptNo).toPromise();
	}
	
	public async receiptLookupByData(request: VerifyReceiptRequest): Promise<Receipt> {
		console.log(request);
		let dateFormat = 'DD/MM/YYYY';
        let formattedreceiptDate = moment(request.receiptDate).format(dateFormat);
		let httpParams = new HttpParams()
		.set('nid', request.nid)
		.set('filingRefNo',request.filingRefNo)
		.set('receiptDate',formattedreceiptDate)
		.set('receiptNo',request.receiptNo)
		.set('totalAmount',request.totalAmount.toString());
		console.log(httpParams);
		return await this.http.get<Receipt>(this._url,{params:httpParams}).toPromise();
	}
	
}
