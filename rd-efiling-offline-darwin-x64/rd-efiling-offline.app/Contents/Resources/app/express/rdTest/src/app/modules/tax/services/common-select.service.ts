import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../../../environments/environment';

@Injectable()
export class CommonSelectService {

  constructor(private http: HttpClient) { }

  private isObservableEmpty(value: any): boolean {
    return !(value.source && value.operator);
  }
  private getRemoteSelect(url: string) {
    return this.http.get(url);
  }

  /* Province */
  private _province: Observable<any> = new Observable<any>();
  get province(): Observable<any> { return this._province; }
  set province(value: Observable<any>) { this._province = value; }
  public getProvince(): Observable<any> {
    let _url = `${environment.masterUrl}/master/dopa/province/u`;
    return this.isObservableEmpty(this.province) ? this.province = this.getRemoteSelect(_url) : this.province;
  }

  /* District */
  private _district: Observable<any> = new Observable<any>();
  private _provinceCodeParam: string;
  get district(): Observable<any> { return this._district; }
  set district(value: Observable<any>) { this._district = value; }
  public getDistrict(_provinceCode: string): Observable<any> {
    let _url = `${environment.masterUrl}/master/dopa/province/${_provinceCode}/districtbyprovinceCode/u`;
    if (this._provinceCodeParam !== _provinceCode) {
      this._provinceCodeParam = _provinceCode;
      return this.district = this.getRemoteSelect(_url);
    }
    return this.district;
  }

  /* Sub District */
  private _subDistrict: Observable<any> = new Observable<any>();
  private _districtCodeParam: string;
  get subDistrict(): Observable<any> { return this._subDistrict; }
  set subDistrict(value: Observable<any>) { this._subDistrict = value; }
  public getSubDistrict(_districtCode: string): Observable<any> {
    let _url = `${environment.masterUrl}/master/dopa/district/${_districtCode}/subdistrictbydistrictCode/u`;
    if (this._districtCodeParam !== _districtCode) {
      this._districtCodeParam = _districtCode;
      return this.subDistrict = this.getRemoteSelect(_url);
    }
    return this.subDistrict;
  }

  /* Postal */
  private _postal: Observable<any> = new Observable<any>();
  private _subdistrictCodeParam: string;
  get postal(): Observable<any> { return this._postal; }
  set postal(value: Observable<any>) { this._postal = value; }
  public getPostal(_subdistrictCode: string): Observable<any> {
    let _url = `${environment.masterUrl}/master/dopa/subdistrict/${_subdistrictCode}/postalbysubdistrictCode/u`;
    if (this._subdistrictCodeParam !== _subdistrictCode) {
      this._subdistrictCodeParam = _subdistrictCode;
      return this.postal = this.getRemoteSelect(_url);
    }
    return this.postal;
  }
}
