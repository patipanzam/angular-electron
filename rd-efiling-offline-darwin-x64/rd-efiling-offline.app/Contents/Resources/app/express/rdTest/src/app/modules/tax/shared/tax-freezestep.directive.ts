import { Directive, HostListener, Renderer2, ElementRef } from '@angular/core';

@Directive({ selector: 'div[tax-freezestep]' })
export class TaxFreezeStepDirective {
  scrHeight:any;
  scrWidth:any;
  constructor(private renderer: Renderer2, private el: ElementRef) {
    this.getScreenSize();
  }

  @HostListener("window:scroll", ['$event'])
  onWindowScroll(event: any) {
    if ((this.scrHeight > 0 && this.scrHeight <= 700)) {
      if (event.target.scrollingElement.scrollTop && event.target.scrollingElement.scrollTop > 100) {
        this.renderer.addClass(this.el.nativeElement, 'freezestep');
      } else {
        this.renderer.removeClass(this.el.nativeElement, 'freezestep');
      }
    } else if ((this.scrHeight > 700 && this.scrHeight <= 768)) {
      if (event.target.scrollingElement.scrollTop/this.scrHeight > 0.35) {
        this.renderer.addClass(this.el.nativeElement, 'freezestep');
      } else {
        this.renderer.removeClass(this.el.nativeElement, 'freezestep');
      }
    } else if ((this.scrHeight > 768 && this.scrHeight <= 900)) {
      if (event.target.scrollingElement.scrollTop/this.scrHeight > 0.25) {
        this.renderer.addClass(this.el.nativeElement, 'freezestep');
      } else {
        this.renderer.removeClass(this.el.nativeElement, 'freezestep');
      }
    } else if ((this.scrHeight > 900 && this.scrHeight <= 1080)) {
      if (event.target.scrollingElement.scrollTop/this.scrHeight > 0.18) {
        this.renderer.addClass(this.el.nativeElement, 'freezestep');
      } else {
        this.renderer.removeClass(this.el.nativeElement, 'freezestep');
      }
    } else {
      if (event.target.scrollingElement.scrollTop && event.target.scrollingElement.scrollTop > 140) {
        this.renderer.addClass(this.el.nativeElement, 'freezestep');
      } else {
        this.renderer.removeClass(this.el.nativeElement, 'freezestep');
      }
    }
  }

  @HostListener("window:resize", ['$event'])
  getScreenSize(event?) {
      this.scrHeight = window.innerHeight;
      this.scrWidth = window.innerWidth;
  }
}
