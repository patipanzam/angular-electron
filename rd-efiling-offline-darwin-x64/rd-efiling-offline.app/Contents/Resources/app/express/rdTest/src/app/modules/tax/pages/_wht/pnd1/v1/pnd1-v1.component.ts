import { Component, OnInit ,TemplateRef,ViewChild} from '@angular/core';
import { FormGroup, FormBuilder ,Validators} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalRef, BsModalService,ModalDirective} from 'ngx-bootstrap';
import { TaxBreadcrumbService } from '../../../tax-breadcrumb/service/tax-breadcrumb.service';
import { TaxComponent } from '../../../tax-component/tax.component';
import { Pnd1Service } from '../services/pnd1.service';
import { Pnd1AttachFormFieldConfig, Pnd1CompleteFormFieldConfig } from '../models/pnd1-fieldconfig.model';
import { ValidationMessageService } from '../../../../../../shared/components/validation-message/validation-message.service';
import { ValidationMessageConfigTh } from '../../../../../../config/validation-message.th';
import { saveAs } from 'file-saver';
import { Pnd1V1Step1Component } from './components/pnd1-v1-step1/pnd1-v1-step1.component';
import { Pnd1V1Step2Component } from './components/pnd1-v1-step2/pnd1-v1-step2.component';
import { Pnd1V1Step3Component } from './components/pnd1-v1-step3/pnd1-v1-step3.component';
import { DateTimeShowPipe } from '../../../../../../shared/pipes/date-time-show.pipe';
import { Pnd1CustomValidator } from '../models/pnd1-custom-validator';
@Component({
  selector: 'ef-pnd1',
  templateUrl: './pnd1-v1.component.html',
  styleUrls: ['./pnd1-v1.component.scss']
})
export class Pnd1V1Component implements OnInit {

  @ViewChild(Pnd1V1Step1Component) pnd1V1Step1Child: Pnd1V1Step1Component;
  @ViewChild(Pnd1V1Step2Component) pnd1V1Step2Child: Pnd1V1Step2Component;
  @ViewChild(Pnd1V1Step3Component) pnd1V1Step3Child: Pnd1V1Step3Component;
 
  @ViewChild('successModal') successModal: ModalDirective;
  @ViewChild('exitModal') exitModal: ModalDirective;
   
    content: string;
    closeText: string;
    showModal: boolean = false;
    showModalSaveDeaft: boolean = false;


  formPnd1: FormGroup;
  formInputPnd1:any;
  pnd1CompleteForms;
  checkEditMode : number = 0;
  modalDraft: BsModalRef;
  modalResultDraft: BsModalRef;
  modalExit: BsModalRef;
  pdfSrc: any[];
  calculateRecordForm? : any = {};
  resultRecordForm = [];
  listFilingNo = [];
  filingNo = 0;
  viewPdf: boolean = false;
  hideStepper : boolean = true;
  alertHeader : string;
  msgBody : string;
  isStepTaxSummary: boolean = false;
  month;
  year;
  msgExit : string = ValidationMessageConfigTh.W02PND1011;

 public checkButton1 : boolean = false;
 public checkButton2 : boolean = false;
 public checkButton3 : boolean = false;
 public checkButton4 : boolean = false;

 public draftNo;

  constructor(
    public taxBreadcrumbService: TaxBreadcrumbService,
    private pnd1Service: Pnd1Service,
    private modalService: BsModalService,
    private taxComponent: TaxComponent,
    private fb: FormBuilder,
    private routerActive: ActivatedRoute,
    private router: Router,
    private validationMessageService :ValidationMessageService
  ) {
    this.taxBreadcrumbService.breadcrumb = [
      { title: 'หน้าหลัก', icon: 'icon-step01.svg', stepper: 1 },
      { title: 'บันทึกข้อมูลรายการใบแนบ', icon: 'icon-step02.svg', stepper: 2 },
      { title: 'สรุปรายการภาษีที่นำส่ง', icon: 'icon-step03.svg', stepper: 3 }
    ];
    this.taxBreadcrumbService.updateStepper(1);

    this.formPnd1 = this.fb.group({
      pnd1AttachForm: this.taxComponent.createForm(Pnd1AttachFormFieldConfig),
      pnd1CompleteForm: this.taxComponent.createForm(Pnd1CompleteFormFieldConfig)
    });

    const resolveData = this.routerActive.snapshot.data['resolveData'];
    const jsonAttach = resolveData.jsonAttach;
    const jsonComplete = resolveData.formPnd1;
    this.draftNo = ''//resolveData.formPnd1.draftNo;
    console.log('pnd1Form ===>',resolveData)
    this.listFilingNo = [];//(resolveData.formPnd1.listFilingNo !== null)? resolveData.formPnd1.listFilingNo.listFilingNo : [];

    const attDet = jsonAttach.rdForm.formAttach.attDet || [];
    var attDetArray = [];
    attDet.forEach(obj => { attDetArray.push(this.fb.group(obj)) });
    (this.pnd1AttachForm.get('formAttach') as FormGroup).setControl('attDet', this.fb.array(attDetArray));
    this.pnd1AttachForm.setValue(jsonAttach.rdForm);

    const detail = jsonComplete.rdForm.formDetail.taxDetail.detail || [];
    var detailArray = [];
    detail.forEach(obj => { detailArray.push(this.fb.group(obj)) });
    (this.pnd1CompleteForm.get('formDetail.taxDetail') as FormGroup).setControl('detail', this.fb.array(detailArray));
    this.pnd1CompleteForm.setValue(jsonComplete.rdForm);

    this.month = jsonComplete.rdForm.formDetail.taxDetail.month;
    this.year  = jsonComplete.rdForm.formDetail.taxDetail.year;
  }

  get pnd1AttachForm() {
    return this.formPnd1.controls['pnd1AttachForm'] as FormGroup;
  }

  get pnd1CompleteForm() {
    return this.formPnd1.controls['pnd1CompleteForm'] as FormGroup;
  }

  ngOnInit() {
    // this.getFormPnd1();
  }

  onPrintDraft() { }


  onBackStep() {
    window.scrollTo(0, 0);
    this.taxBreadcrumbService.backStep();
    console.log('0000',this.pnd1V1Step2Child.formPnd1Input)
    if(this.pnd1V1Step2Child !== undefined){
      this.formInputPnd1 = this.pnd1V1Step2Child.formPnd1Input;
    }
  }

  onNextStep() {
    window.scrollTo(0, 0);
    if(this.formPnd1.get('pnd1CompleteForm.formDetail.taxDetail.filingNo').valid === true){
    if (this.taxBreadcrumbService.getThisStepperValue() == 2) {
        this.getDueDate();
        // this.nextToStep3();
    }else{
      this.taxBreadcrumbService.nextStep();
    }
   }else{
    this.filingNo = 1;
   }
    if(this.formPnd1.get('pnd1AttachForm.attachDetail.taxPayerInfo.id13').value === ""){
      var idi3 = this.formPnd1.get('pnd1CompleteForm.formDetail.taxPayerInfo.id13').value;
      this.formPnd1.get('pnd1AttachForm.attachDetail.taxPayerInfo.id13').setValue(idi3);
    }
  }

  nextToStep3(){
    this.formPnd1.get('pnd1CompleteForm.formDetail.taxDetail').value.detail = [];
    var checkData = (this.formPnd1.get('pnd1AttachForm.formAttach.attDet').value.length > 0)? this.formPnd1.get('pnd1AttachForm.formAttach.attDet').value : [];
    if (checkData.length > 0) {
      this.taxBreadcrumbService.nextStep();
      var tab01 = this.formPnd1.get('pnd1AttachForm.formAttach.attDet').value.filter(x => x.type === '401N');
      var tab02 = this.formPnd1.get('pnd1AttachForm.formAttach.attDet').value.filter(x => x.type === '4012');
      var tab03 = this.formPnd1.get('pnd1AttachForm.formAttach.attDet').value.filter(x => x.type === '402I');
      var tab04 = this.formPnd1.get('pnd1AttachForm.formAttach.attDet').value.filter(x => x.type === '402E');
      if (tab01.length > 0) {
        if (tab01[0].attData.length > 0) {
          var result = this.formPnd1.get('pnd1AttachForm.formAttach.attDet').value.filter(x => x.type == '401N')[0].attData;
          var Total = result.length;
          var Income = result.map(item => Number(item.pay)).reduce((a, b) => a + b);
          var Tax = result.map(item => Number(item.tax)).reduce((a, b) => a + b);
          this.formPnd1.get('pnd1CompleteForm.formDetail.taxDetail').value.detail.push({ type: '401N', total: Total, income: Income, tax: Tax})
        }
      }else{
        this.formPnd1.get('pnd1CompleteForm.formDetail.taxDetail').value.detail.push({ type: '401N', total: 0, income: 0, tax: 0})
      }
      if (tab02.length > 0) {
        if (tab02[0].attData.length > 0) {
          var result = this.formPnd1.get('pnd1AttachForm.formAttach.attDet').value.filter(x => x.type == '4012')[0].attData;
          var Total = result.length;
          var Income = result.map(item => Number(item.pay)).reduce((a, b) => a + b);
          var Tax = result.map(item => Number(item.tax)).reduce((a, b) => a + b);
          this.formPnd1.get('pnd1CompleteForm.formDetail.taxDetail').value.detail.push({ type: '4012', total: Total, income: Income, tax: Tax})
        }
      }else{
        this.formPnd1.get('pnd1CompleteForm.formDetail.taxDetail').value.detail.push({ type: '4012', total: 0, income: 0, tax: 0})
      }
      if (tab03.length > 0) {
        if (tab03[0].attData.length > 0) {
          var result = this.formPnd1.get('pnd1AttachForm.formAttach.attDet').value.filter(x => x.type == '402I')[0].attData;
          var Total = result.length;
          var Income = result.map(item => Number(item.pay)).reduce((a, b) => a + b);
          var Tax = result.map(item => Number(item.tax)).reduce((a, b) => a + b);
          this.formPnd1.get('pnd1CompleteForm.formDetail.taxDetail').value.detail.push({ type: '402I', total: Total, income: Income, tax: Tax})
        }
      }else{
        this.formPnd1.get('pnd1CompleteForm.formDetail.taxDetail').value.detail.push({ type: '402I', total: 0, income: 0, tax: 0})
      }
      if (tab04.length > 0) {
        if (tab04[0].attData.length > 0) {
          var result = this.formPnd1.get('pnd1AttachForm.formAttach.attDet').value.filter(x => x.type == '402E')[0].attData;
          var Total = result.length;
          var Income = result.map(item => Number(item.pay)).reduce((a, b) => a + b);
          var Tax = result.map(item => Number(item.tax)).reduce((a, b) => a + b);
          this.formPnd1.get('pnd1CompleteForm.formDetail.taxDetail').value.detail.push({ type: '402E', total: Total, income: Income, tax: Tax})
        }
      }else{
        this.formPnd1.get('pnd1CompleteForm.formDetail.taxDetail').value.detail.push({ type: '402E', total: 0, income: 0, tax: 0})
      }
    }
    console.log('pnd1',this.formPnd1)
  }

  nextToStep4(){
    window.scrollTo(0, 0);
    this.taxBreadcrumbService.updateStepper(4);
    this.hideStepper = false;
  }

  getDueDate() {
    var total = [];
    this.formPnd1.get('pnd1AttachForm.formAttach.attDet').value.forEach(option => {
      option.attData.forEach(data => {
        total.push(data)
      });
    });
    var formCode = this.formPnd1.get('pnd1CompleteForm.docDetail.code').value;
    var filingType = this.formPnd1.get('pnd1CompleteForm.formDetail.taxDetail.filingType').value;
    var netTaxAmount = total.map(item => Number(item.tax)).reduce((a, b) => a + b);
    this.formPnd1.get('pnd1CompleteForm.formDetail.taxDetail.netTax').setValue(netTaxAmount);
    var taxMonth = this.formPnd1.get('pnd1CompleteForm.formDetail.taxDetail.month').value;
    var taxYear = this.formPnd1.get('pnd1CompleteForm.formDetail.taxDetail.year').value;
    var obj = { "filingType": filingType, "formCode": formCode, "netTaxAmount": netTaxAmount, "taxMonth": taxMonth, "taxYear": taxYear }
    this.pnd1Service.getDueDate(obj).subscribe(
      data => {
        console.log('obj',obj)
      this.formPnd1.get('pnd1CompleteForm.formDetail.payment.dueDate').setValue(data.dueDate);
      this.formPnd1.get('pnd1CompleteForm.formDetail.payment.payAmt').setValue(data.totAmt);
      this.formPnd1.get('pnd1CompleteForm.formDetail.taxDetail.criminal').setValue(data.criminalAmount);
      this.formPnd1.get('pnd1CompleteForm.formDetail.taxDetail.surcharge').setValue(data.surchargeAmount);
      this.formPnd1.get('pnd1CompleteForm.formDetail.taxDetail.roundFlag').setValue(data.roundFlag);
      this.formPnd1.get('pnd1CompleteForm.formDetail.taxDetail.taxAmt').setValue(data.taxAmt);
      this.formPnd1.get('pnd1CompleteForm.formDetail.taxDetail.totAmt').setValue(data.totAmt);
      this.formPnd1.get('pnd1CompleteForm.formDetail.taxDetail.totTax').setValue(data.totTax);
      this.calculateRecordForm = {surchargeDesc:data.surchargeDesc,criminalDesc:data.criminalDesc}
      console.log('===>',this.formPnd1)
      this.nextToStep3();
      }, err => {

      });
  }

  onSubmitForm() {
    window.scrollTo(0, 0);
    this.isStepTaxSummary = true;
  }

  onClickPrintDraft($event){
    console.log('onClickSuccessStep2 ====>',$event)
    if($event === 'P'){
      this.genPdf('P');
    }
  }

  onExit() {
    // this.router.navigate(['/tax/dashboard']);
    // this.modalExit = this.modalService.show(templateExit);
    this.exitModal.show();
  }

  submitExit() {
    this.exitModal.hide();
    this.router.navigate(['/tax/dashboard']);
  }

  showAlert(code:string , templateDraft : TemplateRef<any>) {
    //this.checkButton2 = true;
    this.alertHeader = "ข้อมูล";
    this.msgBody = this.validationMessageService.getErrorMsg(code);
    this.modalDraft = this.modalService.show(templateDraft);
  }

  showModalSaveDraft(){
   this.content = 'คุณต้องการบันทึกแบบฉบับร่างนี้ใช่หรือไม่';
   this.showModalSaveDeaft = true;
  }

  onSaveDraft() {
    this.showModalSaveDeaft = false;
    var jsonAttach = this.formPnd1.value.pnd1AttachForm;
    var jsonComplete = this.formPnd1.value.pnd1CompleteForm;
    var taCode = '';
    var obj = {channelCode:1,draftNo:this.draftNo,jsonAttach:{rdForm:jsonAttach},jsonForm:{rdForm:jsonComplete},taCode:taCode}
    console.log(JSON.stringify(obj));
    this.pnd1Service.saveDraftPnd1(obj).subscribe(result => {
        this.checkButton2 = false;
        console.log(result);
        // this.content ='บันทึกข้อมูลแบบร่างเรียบร้อยแล้ว <br/> หมายเลขอ้างอิงแบบร่าง ' + this.draftNo + ' <br/> วันเวลาที่บันทึกแบบร่าง ' + new DateTimeShowPipe().transform(new Date(), 'DD/MM/YYYY HH:mm:ss'),
        this.content ='บันทึกข้อมูลแบบร่างเรียบร้อยแล้ว <br/> หมายเลขอ้างอิงแบบร่าง ' + result.draftNo + ' <br/> วันเวลาที่บันทึกแบบร่าง ' + result.updateDate,
        this.showModal = true;
        this.draftNo = result.draftNo
        console.log('===>1',this.formPnd1)
        // this.modalResultDraft = this.modalService.show(templateResultDraft);
      },
      error => {
        this.showModal = true;
        this.content = ValidationMessageConfigTh.W02PND1011;
        
      });
  }

  genPdf(type : string) {
    //this.checkButton1 = true;
    var pnd1Attach = this.formPnd1.value.pnd1AttachForm;
    var obj = { "rdForm" : pnd1Attach};
    // console.log(obj);
    console.log(JSON.stringify(obj));
    if (type === 'P') {
    this.pnd1Service.printPdf(type,obj).subscribe(
      result => {
        this.checkButton1 = false;
        var blob = new Blob([result], {type: 'application/pdf'});
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(blob);
        }
        else {
            var fileURL = URL.createObjectURL(blob);
            var win = window.open();
            win.document.write('<iframe src="' + fileURL + '" frameborder="0" style="border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;" allowfullscreen></iframe>')
        }
      });
    } else {
      this.pnd1Service.printPdf(type,obj).subscribe(
        result => {
          this.checkButton1 = false;
          var blob = new Blob([result], {type: 'application/pdf'});
          var filename = this.draftNo + "_" + this.formPnd1.value.pnd1AttachForm.rdForm.formDetail.taxDetail.year + ".pdf";
          saveAs(blob,filename);
        });
    }
  }

  genPreviewPdf(type : string) {
    //this.checkButton3 = true;
    this.pnd1CompleteForms = this.formPnd1.value.pnd1CompleteForm;
    var obj = { "rdForm" : this.pnd1CompleteForms};
    console.log(JSON.stringify(obj));
    if (type === 'C') {
    this.pnd1Service.previewPdf('C',obj).subscribe(
      result => {
        this.checkButton3 = false;
        console.log(result);
        this.pdfSrc = result;
        this.viewPdf = true;
        this.hideStepper = false;
        this.pnd1V1Step3Child.genPdfForm();
      });
    }else{
      this.pnd1Service.previewPdf('D',obj).subscribe(
        result => {
          var blob = new Blob([result], {type: 'application/pdf'});
          if (window.navigator && window.navigator.msSaveOrOpenBlob) {
              window.navigator.msSaveOrOpenBlob(blob);
          }
          else {
              var fileURL = URL.createObjectURL(blob);
              var win = window.open();
              win.document.write('<iframe src="' + fileURL + '" frameborder="0" style="border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;" allowfullscreen></iframe>')
          }
      });
    }
  }

  printResult(type : string){
     this.formPnd1.value.pnd1CompleteForm = this.pnd1CompleteForms;
     var obj = { "rdForm" : this.pnd1CompleteForms};
      // console.log('==>',this.formPnd1)
      // console.log(JSON.stringify(obj));
     this.pnd1Service.previewPdf(type, obj).subscribe(
      result => {
        var blob = new Blob([result], { type: 'application/pdf' });
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveOrOpenBlob(blob);
        }
        else {
          var fileURL = URL.createObjectURL(blob);
          var win = window.open();
          win.document.write('<iframe src="' + fileURL + '" frameborder="0" style="border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;" allowfullscreen></iframe>')
        }
      });

  }

  cancelPdf() {
    this.viewPdf = false;
    this.hideStepper = true;
    this.pnd1V1Step3Child.genPdfForm();
  }

  savePnd1(){
    //this.checkButton4 = true;
    var jsonAttach = this.formPnd1.value.pnd1AttachForm;
    var jsonComplete = this.formPnd1.value.pnd1CompleteForm;
    var taCode = '';
    var obj = {channelCode:1,draftNo:this.draftNo,jsonAttach:{rdForm:jsonAttach},jsonForm:{rdForm:jsonComplete},taCode:taCode}
    console.log(jsonAttach);
    console.log(JSON.stringify(obj));
    this.pnd1Service.recordFormPnd1(obj).subscribe(
      result => {
        this.formPnd1.get('pnd1CompleteForm.formDetail.taxDetail.refNo').setValue(result.refNo);
        this.resultFormPnd1();
      });
  }

  resultFormPnd1(){
    var refNo = this.formPnd1.get('pnd1CompleteForm.formDetail.taxDetail.refNo').value;
    this.pnd1Service.resultFormPnd1(refNo).subscribe(
      result => {
        this.checkButton4 = false;
        if(result.refNo != ""){
          this.resultRecordForm = [result];
          this.onSubmitForm();
        }
      });
  }

  onPrintToCheck() { }

  onPayTax() { 
    if (this.resultRecordForm[0].refNo) {
      this.pnd1Service.sendPaymentInfo(this.resultRecordForm[0].refNo).subscribe(data => {
        console.log("sendPaymentInfo ==>" , data);
        if(data) {
          this.router.navigate(['e-payment/channel/' + data.uuid]);
          }
        });
      }
  }

  onHidden() {
    this.showModal = false;
    this.showModalSaveDeaft = false;
}

}
