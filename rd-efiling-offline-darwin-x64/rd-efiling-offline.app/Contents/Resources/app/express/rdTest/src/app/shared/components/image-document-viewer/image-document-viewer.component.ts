import { HttpClient, HttpResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpOptionsNoToken } from '../../../shared/class/http-options-no-token';
import { finalize, take, takeLast, takeWhile, takeUntil, tap } from 'rxjs/operators';

@Component({
  selector: 'ef-image-document-viewer',
  templateUrl: './image-document-viewer.component.html',
  styleUrls: ['./image-document-viewer.component.scss']
})
export class ImageDocumentViewerComponent implements OnInit {

  propertiesState: {} = {};

  @Input('imgUrl') imgUrl: string = "";
  @Input('imgUrlObservable') imgUrl$: Observable<string> = new Observable<string>();
  @Input('noToken') noToken: Boolean = false;
  // @Input('imgType') imgType :string = "image/jpeg";


  imageBlobUrl: any | null = null;

  mHttpOptions: {} = {
    responseType: 'blob' as 'json'
  }

  constructor(private http: HttpClient) {
    this.propertiesState['downloadImgF'] = false;
    this.mHttpOptions['observe'] = 'response';
    this.imageBlobUrl = null;
  }

  ngOnInit() {
    if (this.imgUrl) {
      this.getThumbnail()
    }

    if(this.imgUrl$){
      this.imgUrl$
      .subscribe(
        (data)=>{
          this.imgUrl = data;
          this.getThumbnail()
        }
      );
    }

  }

  getThumbnail(): void {
    // this.getBlobThumbnail("R48151057306760SOayjqzRhD")
    this.propertiesState['downloading'] = true;
    this.getBlobThumbnailUrl(this.imgUrl)
    .pipe(
      finalize(()=>{
        this.propertiesState['downloading'] = false;
      })
    )
      .subscribe((val) => {
        this.createImageFromBlob(val);
      },
        response => {
          console.log("POST - getThumbnail - in error", response);
          this.propertiesState['downloadImgF'] = true;
        },
        () => {
          console.log("POST - getThumbnail - observable is now completed.");
          console.log("-->imageBlobUrl:", this.imageBlobUrl);
        });
  }

  createImageFromBlob(image: any) {
    console.log("image image: ", image);
    this.propertiesState['downloadImgF'] = false;
    let reader = new FileReader();
    if (image) {
      reader.readAsDataURL(image.body);
    }
    reader.addEventListener("load", () => {
      this.imageBlobUrl = reader.result;
      console.log("imageBlobUrl:", this.imageBlobUrl);
    }, false);
  }

  // getBlobThumbnail(referenceCode: string): Observable<any> {
  //   var curTime = new Date().getTime();
  //   return this.http.get<HttpResponse<Blob>>(`${this.imgUrl}?v=${curTime}`,HttpOptionsNoToken.httpRespTypeBlobOptions());
  // }

  getBlobThumbnailUrl(url: string): Observable<any> {
    var curTime = new Date().getTime();
    var mHttpOptions = {};
    if (this.noToken) {
      mHttpOptions = HttpOptionsNoToken.httpRespTypeBlobOptions();
    } else {
      mHttpOptions = this.mHttpOptions;
    }
    return this.http.get<HttpResponse<Blob>>(`${this.imgUrl}?v=${curTime}`, mHttpOptions);
  }

}
