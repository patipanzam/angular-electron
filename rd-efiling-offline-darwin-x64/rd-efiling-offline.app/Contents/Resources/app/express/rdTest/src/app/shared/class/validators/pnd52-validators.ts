import * as moment from 'moment';
import { AbstractControl, ValidatorFn, ValidationErrors } from '@angular/forms';
import { DateTimeShowPipe } from '../../pipes/date-time-show.pipe';

export function E02PND52001(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        let start = control.get('startDate');
        let end = control.get('endDate');
        if ((start && start.value) && (end && end.value)) {
            return start.value > end.value ? { 'E02PND52001': true } : null;
        }
        return null;
    };
}

export function E02PND52002(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        let start = control.get('startDate');
        let end = control.get('endDate');
        if ((start && start.value) && (end && end.value)) {
            let endTarget: Date = moment(start.value).add(12, 'months').add(-1, 'days').toDate();
            return end.value > endTarget ? { 'E02PND52002': true } : null;
        }
        return null;
    };
}

export function E02PND52003(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        let start = control.get('startDate');
        let end = control.get('endDate');
        if ((start && start.value) && (end && end.value)) {
            return start.value > end.value ? { 'E02PND52003': true } : null;
        }
        return null;
    };
}

export function E02PND52054(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        if (control && control.value) {
            return control.value.length != 10  ? { 'E02PND52054': 'กรุณาระบุหมายเลขอ้างอิงแบบร่างให้ครบ 10 หลัก' } : null;
        }
        return null;
    };
}

export function E02PND52057(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        if (control && control.value) {
            let endTarget: Date = moment().add(1, 'days').startOf('day').toDate();
            return control.value >= endTarget ? { 'E02PND52057': true } : null;
        }
        return null;
    };
}

export function Pnd52ModalStartDate(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors => {
        let start = control.get('startDate');
        let end = control.get('endDate');
        if ((!(start && start.value) || (start && start.value == '')) && (!(end && end.value) || (end && end.value == ''))) {
            return { 'requiredDate': 'จะต้องมีข้อมูลวันที่เริ่มต้นและวันที่สิ้นสุดของรอบระยะเวลาบัญชี' };
        } else if (!(start && start.value) || (start && start.value == '')){
            return { 'startDate': 'จะต้องมีข้อมูลวันที่เริ่มต้นและวันที่สิ้นสุดของรอบระยะเวลาบัญชี' };
        } else if ((start && start.value)) {
            if (start.value > moment().toDate()) {
                return { 'startDate': 'วันเริ่มต้นรอบระยะเวลาบัญชี ต้องไม่มากกว่าวันที่บันทึกข้อมูล' }
            } else if (start.value < moment('01/01/2018', 'MM/DD/YYYY').toDate()) {
                return { 'startDate':  'วันเริ่มรอบระยะเวลาบัญชี ต้องมีค่าตั้งแต่วันที่ 1 ม.ค. 2561' }
            }
        }
        return null;
    }
}

export function Pnd52ModalEndDate(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors => {
        let start = control.get('startDate');
        let end = control.get('endDate');
        if ((!(start && start.value) || (start && start.value == '')) && (!(end && end.value) || (end && end.value == ''))) {
            return { 'requiredDate': 'จะต้องมีข้อมูลวันที่เริ่มต้นและวันที่สิ้นสุดของรอบระยะเวลาบัญชี' };
        } else if (!(end && end.value) || (end && end.value == '')) {
            return { 'endDate': 'จะต้องมีข้อมูลวันที่เริ่มต้นและวันที่สิ้นสุดของรอบระยะเวลาบัญชี' };
        } else if ((end && end.value)) {
            if (end.value > moment().toDate()) {
                return { 'endDate': 'วันที่สิ้นสุดรอบระยะเวลาบัญชี ต้องไม่มากกว่าวันที่บันทึกข้อมูล' };
            } else if (moment(end.value).isSame(new Date(), 'day')) {
                return { 'endDate': 'ยังไม่ถึงกำหนดเวลาการยื่นแบบ ภ.ง.ด.52 ของรอบระยะเวลาบัญชีที่มีวันที่ ' + new DateTimeShowPipe().transform(new Date()) }
            } else if (start && start.value) {
                if (end.value > moment(start.value).add(12, 'months').add(-1, 'days').toDate()) {
                    return { 'endDate': 'รอบระยะเวลาบัญชีต้องไม่เกิน 12 เดือน' }
                } else if (start.value > end.value) {
                    return { 'endDate': 'วันสิ้นสุดรอบระยะเวลาบัญชี จะต้องไม่น้อยกว่าวันเริ่มรอบระยะเวลาบัญชี' }
                }
            }
        }
        return null;
    }
}

export function Pnd52ModalDraftStartDate(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors => {
        let start = control.get('startDate');
        let end = control.get('endDate');
        if ((!(start && start.value) || (start && start.value == '')) && (!(end && end.value) || (end && end.value == ''))) {
            return { 'requiredDate': 'จะต้องมีข้อมูลวันที่เริ่มต้นและวันที่สิ้นสุดของรอบระยะเวลาบัญชี' };
        } else if (!(start && start.value) || (start && start.value == '')) {
            return { 'startDate': 'จะต้องมีข้อมูลวันที่เริ่มต้นและวันที่สิ้นสุดของรอบระยะเวลาบัญชี' };
        } else if ((start && start.value)) {
            if (start.value > moment().toDate()) {
                return { 'startDate': 'วันเริ่มต้นรอบระยะเวลาบัญชี ต้องไม่มากกว่าวันที่บันทึกข้อมูล' }
            }
        }
        return null;
    }
}

export function Pnd52ModalDraftEndDate(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors => {
        let start = control.get('startDate');
        let end = control.get('endDate');
        if ((!(start && start.value) || (start && start.value == '')) && (!(end && end.value) || (end && end.value == ''))) {
            return { 'requiredDate': 'จะต้องมีข้อมูลวันที่เริ่มต้นและวันที่สิ้นสุดของรอบระยะเวลาบัญชี' };
        } else if (!(end && end.value) || (end && end.value == '')) {
            return { 'endDate': 'จะต้องมีข้อมูลวันที่เริ่มต้นและวันที่สิ้นสุดของรอบระยะเวลาบัญชี' };
        } else if ((end && end.value)) {
            if (end.value > moment().toDate()) {
                return { 'endDate': 'วันที่สิ้นสุดรอบระยะเวลาบัญชี ต้องไม่มากกว่าวันที่บันทึกข้อมูล' };
            } else if (start && start.value) {
                if (end.value > moment(start.value).add(12, 'months').add(-1, 'days').toDate()) {
                    return { 'endDate': 'รอบระยะเวลาบัญชีต้องไม่เกิน 12 เดือน' }
                } else if (start.value > end.value) {
                    return { 'endDate': 'วันสิ้นสุดรอบระยะเวลาบัญชี จะต้องไม่น้อยกว่าวันเริ่มรอบระยะเวลาบัญชี' }
                }
            }
        }
        return null;
    }
}

export function Pnd52ValidateTaxpayerNo(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors => {
        if (control.value && control.value != '') {
            for (var i = 0, sum = 0; i < 12; i++) {
                sum += parseFloat(control.value.charAt(i)) * (13 - i);
            }
            if ((11 - sum % 11) % 10 != parseFloat(control.value.charAt(12))) {
                return {
                    E02PND52008: 'กรุณาตรวจสอบเลข 13 หลักให้ถูกต้อง'
                };
            }
        }
        return null;
    }
}

export function Pnd52Step2CarriageRequired(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors => {
        let carriage1 =  control.get('carriage1');
        let carriage2 =  control.get('carriage2');
        let carriage3 =  control.get('carriage3');
        let carriageOfPassengers1 = control.get('carriageOfPassengers1');
        let carriageOfPassengers2 = control.get('carriageOfPassengers2');
        let carriageOfPassengers3 = control.get('carriageOfPassengers3');

        if (
            (carriage1 && (carriage1.value == '' || carriage1.value == null)) &&
            (carriage2 && (carriage2.value == '' || carriage2.value == null)) &&
            (carriage3 && (carriage3.value == '' || carriage3.value == null)) &&
            (carriageOfPassengers1 && (carriageOfPassengers1.value == '' || carriageOfPassengers1.value == null)) &&
            (carriageOfPassengers2 && (carriageOfPassengers2.value == '' || carriageOfPassengers2.value == null)) &&
            (carriageOfPassengers3 && (carriageOfPassengers3.value == '' || carriageOfPassengers3.value == null))
        ) {
            return {
                E02PND52024:  'จะต้องมีข้อมูลรายรับก่อนหักรายจ่ายใด ๆ อย่างน้อย 1 ข้อ'
            }
        }

        return null;
    }
}

export function Pnd52Step2RequriedRateWhenTotalHasValue(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors => {

        let carriage1 =  control.get('carriage1');
        let carriage2 =  control.get('carriage2');
        let carriage3 =  control.get('carriage3');
        let carriageOfPassengers1 = control.get('carriageOfPassengers1');
        let carriageOfPassengers2 = control.get('carriageOfPassengers2');
        let carriageOfPassengers3 = control.get('carriageOfPassengers3');
        let incomeTaxComputationRate = control.get('incomeTaxComputationRate');

        if (
            ((carriage1 && carriage1.value > 0) ||
            (carriage2 && carriage2.value > 0) ||
            (carriage3 && carriage3.value > 0) ||
            (carriageOfPassengers1 && carriageOfPassengers1.value > 0) ||
            (carriageOfPassengers2 && carriageOfPassengers2.value > 0) ||
            (carriageOfPassengers3 && carriageOfPassengers3.value > 0)) &&
            (incomeTaxComputationRate && incomeTaxComputationRate.value == null)
        ) {
            return {
                E02PND52025: 'จะต้องมีสถานะภาษีเงินได้คำนวณจาก ร้อยละ 3 หรือภาษีที่ต้องชำระภายหลังจากการยกเว้น'
            }
        }

        return null;
    }
}

export function Pnd52Step2Less1NotMoreThanTotal(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors => {
        
        let lessAmount1 = control.get('lessAmount1');
        let sum = sumTaxTotal(control);
        if (lessAmount1 && lessAmount1.value && +lessAmount1.value >= sum) {
            return {
                E02PND52028: 'ภาษีเงินได้หัก ณ ที่จ่ายตามมาตรา 3 เตรส ต้องน้อยกว่าจำนวนเงินรวมรายรับทั้งหมด'
            }
        }

        return null;
    }
}

export function Pnd52Step2Less2NotMoreThanTotal(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors => {
        
        let lessAmount2 = control.get('lessAmount2');
        let sum = sumTaxTotal(control);
        if (lessAmount2 && lessAmount2.value && +lessAmount2.value >= sum) {
            return {
                E02PND52052:  'ภาษีเงินได้หัก ณ ที่จ่ายตามมาตรา 69 ทวิ ต้องน้อยกว่าจำนวนเงินรวมรายรับทั้งหมด'
            }
        }

        return null;
    }
}

export function Pnd52Step2Less3NotMoreThanTotal(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors => {
        
        let lessAmount3 = control.get('lessAmount3');
        let sum = sumTaxTotal(control);
        if (lessAmount3 && lessAmount3.value && +lessAmount3.value >= sum) {
            return {
                E02PND52050: 'ภาษีที่ชำระแล้วตามแบบ ภ.ง.ด.52 (กรณียื่นเพิ่มเติม) ต้องน้อยกว่าจำนวนเงินรวมรายรับทั้งหมด'
            }
        }

        return null;
    }
}

export function Pnd52Step2ValidateAmount(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors => {

        let incomeTaxComputationRate = control.get('incomeTaxComputationRate');
        let incomeTaxComputationAmount = control.get('incomeTaxComputationAmount');

        if (incomeTaxComputationRate && incomeTaxComputationRate.value != '0') {

            if (incomeTaxComputationRate.value == '1' && 
                (incomeTaxComputationAmount && 
                (incomeTaxComputationAmount.value == null || incomeTaxComputationAmount.value == ''))) {
                return {
                    Pnd52Step2ValidateAmount: 'จะต้องมีภาษีเงินได้คำนวณจาก กรณีที่ต้องชำระภายหลัง จากการยกเว้นตามพระราชกฤษฎีกา (ฉบับที่ 18) หรือ (ฉบับที่ 463) หากไม่มีให้บันทึก 0.00'
                };
            }

            if (incomeTaxComputationRate.value == '2' && 
                (incomeTaxComputationAmount && 
                (incomeTaxComputationAmount.value == null || incomeTaxComputationAmount.value == ''))) {
                return {
                    Pnd52Step2ValidateAmount: 'จะต้องมีภาษีเงินได้คำนวณจาก กรณีภาษีที่ต้องชำระภายหลังจากการยกเว้นตามพระราชกฤษฎีกาอื่น หากไม่มีให้บันทึก 0.00'
                };
            }

        }

        return null;
    }
}

export function Pnd52Step2Validate(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors => {

        let carriage1 =  control.get('carriage1');
        let carriage2 =  control.get('carriage2');
        let carriage3 =  control.get('carriage3');
        let carriageOfPassengers1 = control.get('carriageOfPassengers1');
        let carriageOfPassengers2 = control.get('carriageOfPassengers2');
        let carriageOfPassengers3 = control.get('carriageOfPassengers3');

        if (
            (carriage1 && (carriage1.value == '' || carriage1.value == null)) &&
            (carriage2 && (carriage2.value == '' || carriage2.value == null)) &&
            (carriage3 && (carriage3.value == '' || carriage3.value == null)) &&
            (carriageOfPassengers1 && (carriageOfPassengers1.value == '' || carriageOfPassengers1.value == null)) &&
            (carriageOfPassengers2 && (carriageOfPassengers2.value == '' || carriageOfPassengers2.value == null)) &&
            (carriageOfPassengers3 && (carriageOfPassengers3.value == '' || carriageOfPassengers3.value == null))
        ) {
            return {
                E02PND52024:  'จะต้องมีข้อมูลรายรับก่อนหักรายจ่ายใด ๆ อย่างน้อย 1 ข้อ'
            }
        }

        let incomeTaxComputationRate = control.get('incomeTaxComputationRate');

        if (
            ((carriage1 && carriage1.value > 0) ||
            (carriage2 && carriage2.value > 0) ||
            (carriage3 && carriage3.value > 0) ||
            (carriageOfPassengers1 && carriageOfPassengers1.value > 0) ||
            (carriageOfPassengers2 && carriageOfPassengers2.value > 0) ||
            (carriageOfPassengers3 && carriageOfPassengers3.value > 0)) &&
            (incomeTaxComputationRate && incomeTaxComputationRate.value == null)
        ) {
            return {
                E02PND52025: 'จะต้องมีสถานะภาษีเงินได้คำนวณจาก ร้อยละ 3 หรือภาษีที่ต้องชำระภายหลังจากการยกเว้น'
            }
        }

        let incomeTaxComputationAmount = control.get('incomeTaxComputationAmount');

        if (incomeTaxComputationRate && incomeTaxComputationRate.value != '0') {

            if (incomeTaxComputationRate.value == '1' && 
                (incomeTaxComputationAmount && 
                (incomeTaxComputationAmount.value == null || incomeTaxComputationAmount.value == ''))) {
                return {
                    Pnd52Step2ValidateAmount: 'จะต้องมีภาษีเงินได้คำนวณจาก กรณีที่ต้องชำระภายหลัง จากการยกเว้นตามพระราชกฤษฎีกา (ฉบับที่ 18) หรือ (ฉบับที่ 463) หากไม่มีให้บันทึก 0.00'
                };
            }

            if (incomeTaxComputationRate.value == '2' && 
                (incomeTaxComputationAmount && 
                (incomeTaxComputationAmount.value == null || incomeTaxComputationAmount.value == ''))) {
                return {
                    Pnd52Step2ValidateAmount: 'จะต้องมีภาษีเงินได้คำนวณจาก กรณีภาษีที่ต้องชำระภายหลังจากการยกเว้นตามพระราชกฤษฎีกาอื่น หากไม่มีให้บันทึก 0.00'
                };
            }
        }

        let sum = sumTaxTotal(control);

        let lessAmount1 = control.get('lessAmount1');
        if (lessAmount1 && lessAmount1.value && +lessAmount1.value >= sum) {
            return {
                E02PND52028: 'ภาษีเงินได้หัก ณ ที่จ่ายตามมาตรา 3 เตรส ต้องน้อยกว่าจำนวนเงินรวมรายรับทั้งหมด'
            }
        }

        let lessAmount2 = control.get('lessAmount2');
        if (lessAmount2 && lessAmount2.value && +lessAmount2.value >= sum) {
            return {
                E02PND52052:  'ภาษีเงินได้หัก ณ ที่จ่ายตามมาตรา 69 ทวิ ต้องน้อยกว่าจำนวนเงินรวมรายรับทั้งหมด'
            }
        }

        let lessAmount3 = control.get('lessAmount3');
        if (lessAmount3 && lessAmount3.value && +lessAmount3.value >= sum) {
            return {
                E02PND52050: 'ภาษีที่ชำระแล้วตามแบบ ภ.ง.ด.52 (กรณียื่นเพิ่มเติม) ต้องน้อยกว่าจำนวนเงินรวมรายรับทั้งหมด'
            }
        }

        return null;
    }
}

function sumTaxTotal(control: AbstractControl): number {
    let sum: number;
    let carriage1 = control.get('carriage1');
    let carriage2 = control.get('carriage2');
    let carriage3 = control.get('carriage3');
    let carriageOfPassengers1 = control.get('carriageOfPassengers1');
    let carriageOfPassengers2 = control.get('carriageOfPassengers2');
    let carriageOfPassengers3 = control.get('carriageOfPassengers3');

    if (carriage1 && carriage1.value) {
        if (sum == undefined) sum = 0;
        sum += Number(carriage1.value)
    }

    if (carriage2 && carriage2.value) {
        if (sum == undefined) sum = 0;
        sum += Number(carriage2.value)
    }

    if (carriage3 && carriage3.value) {
        if (sum == undefined) sum = 0;
        sum += Number(carriage3.value)
    }

    if (carriageOfPassengers1 && carriageOfPassengers1.value) {
        if (sum == undefined) sum = 0;
        sum += Number(carriageOfPassengers1.value)
    }

    if (carriageOfPassengers2 && carriageOfPassengers2.value) {
        if (sum == undefined) sum = 0;
        sum += Number(carriageOfPassengers2.value)
    }

    if (carriageOfPassengers3 && carriageOfPassengers3.value) {
        if (sum == undefined) sum = 0;
        sum += Number(carriageOfPassengers3.value)
    }


    return sum;
}