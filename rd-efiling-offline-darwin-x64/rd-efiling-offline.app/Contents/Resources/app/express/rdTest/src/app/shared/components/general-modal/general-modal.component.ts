import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { EventsService } from '../../services/events.service';
import { ModalDirective, ModalOptions } from 'ngx-bootstrap';
import { GeneralModalService } from '../../services/general-modal.service';
import { Subscription } from 'rxjs';
import { GeneralModal } from '../../models/general-modal';
import { Router, NavigationEnd } from '@angular/router';

@Component({
    selector: 'ef-general-modal',
    templateUrl: './general-modal.component.html',
    styleUrls: ['./general-modal.component.scss']
})
export class GeneralModalComponent implements OnInit, OnDestroy {

    @ViewChild('generalModal') generalModal: ModalDirective;
    isModalShow: boolean = false;
    modalSubscription: Subscription;
    generalModalObj: GeneralModal;
    cb: Function;
    modalOptions: ModalOptions;

    constructor(private generalModalService: GeneralModalService, private router: Router) {
        this.modalOptions = {
            show: true
        };
    }

    ngOnInit() {
        this.modalSubscription = this.generalModalService.modalEvent().subscribe((data) => {
            if (data) {
                this.generalModalObj = data;
                this.showModal();
            }
        });

        this.router.events.subscribe((event) => {
            if (event instanceof NavigationEnd) {
                if (this.generalModal.isShown) {
                    this.hideModal(null);
                }
            }
        });
    }

    ngOnDestroy() {
        this.modalSubscription.unsubscribe();
    }

    showModal() {
        this.modalOptions = {
            show: true,
            backdrop: false,
            ignoreBackdropClick: (this.generalModalObj && this.generalModalObj.ignoreBackdropClick) || false
        };
        this.generalModal.config = this.modalOptions;
        this.generalModal.show();
    }

    hideModal(cb?: Function) {
        this.generalModal.hide();
        this.cb = cb;
    }

    onHidden() {
        if (this.cb != null) this.cb();
    }
    
    onShown() {
        this.generalModalService.hide();
    }

}
