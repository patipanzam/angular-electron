import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { catchError, switchMap, finalize, filter, take } from 'rxjs/operators';
import { BehaviorSubject, Observable, throwError } from 'rxjs';

import { AuthService } from '../../auth/auth.service';

@Injectable()
export class RefreshTokenInterceptor implements HttpInterceptor {
  private refreshTokenInProgress = false;
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(public auth: AuthService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError(error => {
        if (request.url.includes('rd-user-oauth-service') && request.body && request.body.includes && request.body.includes('refresh_token')) {
          console.log('error when refresh_token => action logout()');
          this.auth.logout();
          return throwError(error);
        }

        if (error.status == 401 && error.error.error == 'invalid_token' && error.error.error_description.indexOf('Access token expired') > -1) {
          console.log('Access token expired => action refreshAccessToken()');
          return this.tokenExpireError(request, next);
        }

        return throwError(error);
      })
    );
  }

  private tokenExpireError(request: HttpRequest<any>, next: HttpHandler) {

    console.log('refreshTokenInProgress => ', this.refreshTokenInProgress);

    if (!this.refreshTokenInProgress) {
      this.refreshTokenInProgress = true;
      this.refreshTokenSubject.next(null);

      return this.auth.refreshAccessToken().pipe(
        switchMap((response: any) => {
          if (response) {
            console.log('refreshAccessToken seccess');

            this.refreshTokenSubject.next(response.access_token);
            return next.handle(this.addAuthenticationToken(request, response.access_token));
          }
        }),
        catchError((error: any) => {
          this.refreshTokenInProgress = false;
          this.auth.logout();
          return throwError(error);
        }),
        finalize(() => this.refreshTokenInProgress = false)
      );
    } else {
      return this.refreshTokenSubject.pipe(
        filter(token => token !== null),
        take(1),
        switchMap(token => next.handle(this.addAuthenticationToken(request, token)))
      );
    }
  }

  private addAuthenticationToken(request: HttpRequest<any>, token: string) {
    let headers = request.headers;
    if (headers.get('noToken') !== 'noToken') {
      headers = headers.set('Authorization', `Bearer ${token}`);
    } else if (request.headers.get('noToken') === 'noToken') {
      headers = headers.delete('Authorization').delete('noToken').delete('notoken');
    }

    return request.clone({ headers: headers });
  }
}
