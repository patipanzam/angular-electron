export interface Province{
    provinceCode: any,
    provinceNameTh: any,
    provinceNameEn: any
}

export function getProvinceDefault() {
    return {
        provinceCode: undefined,
        provinceNameTh: undefined,
        provinceNameEn: undefined
    }
}