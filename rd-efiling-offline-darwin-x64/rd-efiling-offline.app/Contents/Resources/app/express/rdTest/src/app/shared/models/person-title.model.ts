export interface PersonTitle {
    titleCode: string,
    titleId: number,
    titleName: string,
    status: string,

    // oldVersion
    titleNameTh: string,
    titleNameEn?: string
}

export function getPersonTitleDefault() {
    return {
        titleCode: undefined,
        titleId: undefined,
        titleName: undefined,
        status: undefined,
        titleNameTh: undefined,
        titleNameEn: undefined
    }
}