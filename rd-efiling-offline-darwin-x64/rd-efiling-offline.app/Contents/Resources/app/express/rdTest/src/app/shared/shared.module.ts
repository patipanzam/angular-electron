import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BsDropdownModule, CollapseModule, TooltipModule, PopoverModule, ModalModule, TabsModule } from 'ngx-bootstrap';
import { BsDatepickerConfig, BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap-th';
import { defineLocale } from 'ngx-bootstrap-th/chronos';
import { thLocale } from 'ngx-bootstrap-th/locale';
import { NgxMaskModule } from 'ngx-mask';
import { NgZorroAntdModule, NZ_I18N, th_TH } from 'ng-zorro-antd';
import { NgSelectModule, NgSelectConfig } from '@ng-select/ng-select';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { FileUploadModule } from 'ng2-file-upload';
import { TranslateModule } from '@ngx-translate/core';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

import { LangComponent } from './components/lang/lang.component';
import { RequiredComponent } from './components/required/required.component';
import { BtnSubmitComponent } from './components/btn-submit/btn-submit.component';
import { ValidationMessageComponent } from './components/validation-message/validation-message.component';
import { ValidationMessageService } from './components/validation-message/validation-message.service';
import { LabelComponent } from './components/label/label.component';
import { ValidationFieldGroupComponent } from './components/validation-field-group/validation-field-group.component';
import { UserComponent } from './components/user/user.component';

import { UserNamePipe } from './pipes/user-name.pipe';
import { WsDateShowPipe } from './pipes/ws-date-show.pipe';
import { CurrencyPipe } from './pipes/currency.pipe';
import { DateLocalThPipe } from './pipes/date-local-th.pipe';
import { ToDatePipe } from './pipes/to-date.pipe';
import { TimeShowPipe } from './pipes/time-show.pipe';
import { WsDatetimeShowPipe } from './pipes/ws-datetime-show.pipe';
import { DateTimeShowPipe } from './pipes/date-time-show.pipe';
import { MonthPipe } from './pipes/month-pipe';

import { NidnFieldDirective } from './directive/nidn-field.directive';
import { NumberFormatDirective } from './directive/number-format.directive';
import { Pnd52ValidateTaxpayerDirective } from './directive/validators/pnd52-validate-taxpayer.directive';
import { TaxFreezeStepDirective } from '../modules/tax/shared/tax-freezestep.directive';
import { CharacterInputDirective } from '../modules/tax/shared/input-characters.directive';
import { EfFormControlNameDirective, EfFormControlNameWithMaskDirective } from './directive/form-control-name.directive';
import { Id13FieldDirective } from './directive/id13-field.directive';

import { LangService } from '../lang';
import { ImageDocumentViewerComponent } from './components/image-document-viewer/image-document-viewer.component';
import { FocusInvalidFieldDirective } from './directive/focus-invalid-field';
import { DisableControlDirective } from './directive/disable-control.directive';
import { NotificationValidationMassageService } from './services/notification-validation-massage.service';
import { GeneralModalComponent } from './components/general-modal/general-modal.component';

import { NgxLoaderIndicatorModule } from 'ngx-loader-indicator';
// import { TaxFormDownloadComponent } from './components/tax-form-download/tax-form-download.component';
import { ValidationTaxComponent } from './components/validation-tax/validation-tax.component';
import { ModalMessageComponent } from './components/modal-message/modal-message.component';

@NgModule({
  imports: [
    CommonModule,
    BsDropdownModule.forRoot(),
    NgxMaskModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TooltipModule.forRoot(),
    PopoverModule.forRoot(),
    ModalModule.forRoot(),
    NgZorroAntdModule.forRoot(),
    TabsModule.forRoot(),
    NgxChartsModule,
    PdfViewerModule,
    TranslateModule,
    FileUploadModule,
    NgxSkeletonLoaderModule,
    NgxLoaderIndicatorModule.forRoot(),
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BsDropdownModule,
    CollapseModule,
    NgxMaskModule,
    LangComponent,
    UserComponent,
    BsDatepickerModule,
    TooltipModule,
    PopoverModule,
    NgSelectModule,
    ModalModule,
    NgZorroAntdModule,
    TabsModule,
    NgxChartsModule,
    PdfViewerModule,
    TranslateModule,
    FileUploadModule,
    NgxSpinnerModule,
    NgxSkeletonLoaderModule,
    NgxLoaderIndicatorModule,

    RequiredComponent,
    ValidationMessageComponent,
    LabelComponent,
    BtnSubmitComponent,
    ValidationFieldGroupComponent,
    ValidationTaxComponent,
    ImageDocumentViewerComponent,
    GeneralModalComponent,
    // TaxFormDownloadComponent,

    WsDateShowPipe,
    UserNamePipe,
    CurrencyPipe,
    DateLocalThPipe,
    MonthPipe,
    ToDatePipe,
    WsDatetimeShowPipe,
    DateTimeShowPipe,

    NidnFieldDirective,
    TaxFreezeStepDirective,
    CharacterInputDirective,
    EfFormControlNameDirective,
    EfFormControlNameWithMaskDirective,
    NumberFormatDirective,
    Pnd52ValidateTaxpayerDirective,
    FocusInvalidFieldDirective,
    DisableControlDirective,
    Id13FieldDirective,
    ModalMessageComponent
  ],
  declarations: [
    LangComponent,
    UserComponent,
    RequiredComponent,
    ValidationMessageComponent,
    LabelComponent,
    BtnSubmitComponent,
    ValidationFieldGroupComponent,
    ImageDocumentViewerComponent,
    GeneralModalComponent,
    // TaxFormDownloadComponent,

    WsDateShowPipe,
    UserNamePipe,
    CurrencyPipe,
    DateLocalThPipe,
    MonthPipe,
    ToDatePipe,
    TimeShowPipe,
    WsDatetimeShowPipe,
    DateTimeShowPipe,

    NidnFieldDirective,
    TaxFreezeStepDirective,
    CharacterInputDirective,
    EfFormControlNameDirective,
    EfFormControlNameWithMaskDirective,
    NumberFormatDirective,
    Pnd52ValidateTaxpayerDirective,
    FocusInvalidFieldDirective,
    DisableControlDirective,
    Id13FieldDirective,
    ValidationTaxComponent,
    ModalMessageComponent
  ],
  providers: [
    // {
    //     provide: NG_SELECT_DEFAULT_CONFIG,
    //     useFactory: (lang: LangService) => {
    //         return {
    //             notFoundText: lang.getLanguageForNgSelect('notFoundText'),
    //             loadingText: lang.getLanguageForNgSelect('loadingText')
    //         };
    //     },
    //     deps: [ LangService ]
    // },
    {
      provide: NZ_I18N,
      useValue: th_TH
      // useFactory: (lang: LangService) => { return lang.getLanguageForNz(); },
      // deps: [LangService]
    }, {
      provide: BsDatepickerConfig,
      useFactory: getDatepickerConfig
    }, ValidationMessageService,
    NotificationValidationMassageService
  ]
})
export class SharedModule {
  constructor(private bsLocaleService: BsLocaleService, private langService: LangService, private ngSelectConfig: NgSelectConfig) {
    this.bsLocaleService.use(this.langService.currentLang);

    ngSelectConfig.notFoundText = langService.getLanguageForNgSelect('notFoundText');
    ngSelectConfig.loadingText = langService.getLanguageForNgSelect('loadingText');
    // registerLocaleData(localeTh, localeThExtra);
    // dateAdapter.setLocale('th-TH');
  }
}

defineLocale('th', thLocale);
export function getDatepickerConfig(): BsDatepickerConfig {
  return Object.assign(new BsDatepickerConfig(), {
    showWeekNumbers: false
  });
}
