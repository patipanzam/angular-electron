export interface TaxFilingCard {
    formCode: string;
    formNameTh: string;
    formShortTh: string;
}