import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LOCAL_LANG_KEY } from '../../../config/constants';

@Component({
  selector: 'ef-lang',
  templateUrl: './lang.component.html',
  styleUrls: ['./lang.component.scss']
})
export class LangComponent implements OnInit {

    lang: string;
    constructor(private translate: TranslateService) { 

    }

    ngOnInit() {
        this.lang = this.translate.currentLang;
    }

    toggleLang(_lang: string) {
        this.translate.use(_lang);
        this.lang = _lang;
        localStorage.setItem(LOCAL_LANG_KEY, this.lang);
    }

}
