import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'ef-modal-message',
  templateUrl: './modal-message.component.html',
  styleUrls: ['./modal-message.component.scss']
})
export class ModalMessageComponent implements OnInit {

  @ViewChild('successModal') successModal: ModalDirective;

  msgBody : string;

  constructor() { }

  ngOnInit() {
  }

  success(msg: any) {
    this.msgBody = msg
    this.successModal.show()
  }

}
