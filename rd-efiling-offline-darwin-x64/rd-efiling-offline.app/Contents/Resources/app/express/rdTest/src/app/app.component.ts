
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { filter, map, mergeMap } from 'rxjs/operators';
import { setTheme } from 'ngx-bootstrap/utils';
import { LOCAL_LANG_KEY } from './config/constants';
import { SnotifyService } from 'ng-snotify';

import { ElectronService } from './providers/electron.service';
import { AppConfig } from '../environments/environment';

@Component({
  selector: 'ef-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  includeSpinner: boolean = false;

  constructor(private translate: TranslateService, private router: Router, private title: Title, private activatedRoute: ActivatedRoute, private snotifyService: SnotifyService,public electronService: ElectronService) {
    let lang = localStorage.getItem(LOCAL_LANG_KEY);

    if (!lang) {
        lang = 'th';
        localStorage.setItem(LOCAL_LANG_KEY, lang);
    }
    
    translate.setDefaultLang(lang);
    translate.use(lang);

    setTheme('bs4');

    translate.setDefaultLang('en');
    console.log('AppConfig', AppConfig);

    if (electronService.isElectron()) {
      console.log('Mode electron');
      console.log('Electron ipcRenderer', electronService.ipcRenderer);
      console.log('NodeJS childProcess', electronService.childProcess);
    } else {
      console.log('Mode web');
    }
  }

  ngOnInit() {
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
      map(() => this.activatedRoute),
      map((route) => {
        while (route.firstChild) {
          route = route.firstChild;
        }
        return route;
      }),
      filter((route) => route.outlet === 'primary'),
      mergeMap((route) => route.data),
    ).subscribe((event) => {
      this.title.setTitle(event['title']);
      // this._title = event['title'];
      // this.translate.get(this._title).subscribe((res) => {
      //     this.title.setTitle(res);
      // });
    });

    // this.initialTitleTranslate();
  }

  // private initialTitleTranslate() {
  //     this.translate.onLangChange.subscribe(ev => {
  //         this.translate.get(this._title).subscribe((res) => {
  //             this.title.setTitle(res);
  //         });
  //     });
  // }

}

