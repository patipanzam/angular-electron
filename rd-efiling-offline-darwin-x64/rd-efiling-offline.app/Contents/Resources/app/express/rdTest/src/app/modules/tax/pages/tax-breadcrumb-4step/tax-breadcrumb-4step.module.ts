import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaxBreadcrumb4StepService } from './service/tax-breadcrumb-4step.service';
import { TaxBreadcrumb4StepComponent } from './tax-breadcrumb-4step.component';

@NgModule({
  imports: [CommonModule],
  exports: [TaxBreadcrumb4StepComponent],
  declarations: [TaxBreadcrumb4StepComponent],
  providers: [TaxBreadcrumb4StepService],
})
export class TaxBreadcrumb4StepModule { }
