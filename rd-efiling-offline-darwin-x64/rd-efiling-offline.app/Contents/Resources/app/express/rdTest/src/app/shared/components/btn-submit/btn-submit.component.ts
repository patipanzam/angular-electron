import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'ef-btn-submit',
    templateUrl: './btn-submit.component.html',
    styleUrls: ['./btn-submit.component.scss']
})
export class BtnSubmitComponent implements OnInit {

    @Input('loading') loading: boolean;
    @Input('disabled') disabled: boolean;
    @Input('type') type: string = 'button';
    @Input('class') class: string = 'btn btn-success';
    @Input('icon') icon: string = 'fa fa-circle-notch fa-spin';

    constructor() { }

    ngOnInit() {
    }

}
