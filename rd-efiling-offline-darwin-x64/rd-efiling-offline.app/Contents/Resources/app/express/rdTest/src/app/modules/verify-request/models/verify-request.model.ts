export interface VerifyRequest {
    refNo: number,
    citizenId: string,
    email: string,
    requestDate: Date,
    expireDate: Date,
    requestType: string,
    requestStatus: string
}
