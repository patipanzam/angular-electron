/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { WhtTaxComponent } from './wht-tax.component';

describe('WhtTaxComponent', () => {
  let component: WhtTaxComponent;
  let fixture: ComponentFixture<WhtTaxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhtTaxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhtTaxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
