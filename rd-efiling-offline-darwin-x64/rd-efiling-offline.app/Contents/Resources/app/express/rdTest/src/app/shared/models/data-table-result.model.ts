export interface DataTableResult<T> {
  data: T,
  recordsTotal: number,
  totalPages: number
  draw: number,
  recordsFiltered: number,
  totoalElments: number
}

export function getDataTableResultDefault<T>() {
  return {
    data:Object.assign([]),
    recordsTotal: 0,
    totalPages: 0,
    draw: 0,
    recordsFiltered: 0,
    totoalElments: 0
  };
}
