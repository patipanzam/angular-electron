export interface Postal {
    postalCode: number,
    postalId: number
}

export function getPostalDefault() {
    return {
        postalCode: undefined,
        postalId: undefined
    }
}