import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RegisterVerifyService } from '../../services';

@Component({
  selector: 'ef-check-request-list',
  templateUrl: './check-request-list.component.html',
  styleUrls: ['./check-request-list.component.scss']
})
export class CheckRequestListComponent implements OnInit {
  refNo: string;
  private sub: any;
  dataSet : any =[];

  constructor(private route: ActivatedRoute ,private registerService:RegisterVerifyService, private cd: ChangeDetectorRef) {}
  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.refNo = params['refno']; // (+) converts string 'id' to a number
      this.registerService.CheckStatus( this.refNo).subscribe(res =>{
        
        this.dataSet = res;
        // this.dataSet = [...this.dataSet, res];
        this.cd.detectChanges()
      });
    });
    
  }
  reSendEmail() {
    console.log("Test")
    this.registerService.reSendEamil( this.refNo).subscribe(res =>{
      console.log(res)

      this.cd.detectChanges()
    });
  }


}
