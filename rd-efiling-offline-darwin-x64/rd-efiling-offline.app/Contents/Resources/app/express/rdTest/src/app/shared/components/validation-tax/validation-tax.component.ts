import { Component, Input } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { ValidationMessageService } from '../validation-message/validation-message.service';

@Component({
  selector: 'ef-validation-tax',
  templateUrl: './validation-tax.component.html',
  styleUrls: ['./validation-tax.component.scss']
})
export class ValidationTaxComponent {

  @Input('control') control: AbstractControl;
  @Input('submit') submit: boolean;
  @Input('errSelect') errSelect: boolean;

  constructor(private validationMessageService: ValidationMessageService) { }

  get errMsg (): string {
    if (this.control) {
      if (this.control.invalid && (this.control.touched || this.control.dirty || this.submit)) {
            for (let errorName in this.control.errors) {
              switch (errorName) {
                case 'required':
                  return this.validationMessageService.getErrorMsg(this.errSelect ? 'requiredSelect' : 'requiredTrue');
                case 'pattern':
                  return 'ต้องไม่มีตัวอักษรพิเศษ | และ ^';
                default :
                  return null;
              }
            }
      }
    }
  }

}
