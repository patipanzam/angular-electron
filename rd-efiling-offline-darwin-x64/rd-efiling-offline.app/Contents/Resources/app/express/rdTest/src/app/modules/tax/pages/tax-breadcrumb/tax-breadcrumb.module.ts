import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TaxBreadcrumbComponent } from './tax-breadcrumb.component';
import { TaxBreadcrumbService } from './service/tax-breadcrumb.service';

@NgModule({
  imports: [CommonModule],
  exports: [TaxBreadcrumbComponent],
  declarations: [TaxBreadcrumbComponent],
  providers: [TaxBreadcrumbService],
})
export class TaxBreadcrumbModule { }
