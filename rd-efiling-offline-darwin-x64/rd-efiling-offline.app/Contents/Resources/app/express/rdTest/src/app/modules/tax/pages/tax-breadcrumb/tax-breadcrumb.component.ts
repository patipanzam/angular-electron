import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { TaxBreadcrumbService, ITaxBreadcrumb } from './service/tax-breadcrumb.service';

@Component({
  selector: 'ef-tax-breadcrumb',
  templateUrl: './tax-breadcrumb.component.html',
  styleUrls: ['./tax-breadcrumb.component.scss']
})
export class TaxBreadcrumbComponent implements OnInit {

  stepper$: Observable<number>;
  breadcrumb: ITaxBreadcrumb[];

  constructor(private taxBreadcrumbService: TaxBreadcrumbService) {
    this.stepper$ = this.taxBreadcrumbService.stepper;
    // this.breadcrumb = this.taxBreadcrumbService.breadcrumb;
    this.taxBreadcrumbService.breadcrumb$.subscribe(val => this.breadcrumb = val);
  }

  ngOnInit() {
  }

}
