import { Validators } from "@angular/forms";
import { FieldConfig } from "../../../tax-component/tax.component";

export const Pnd1CompleteFormFieldConfig: FieldConfig[] = [
  {
    name: 'docDetail',
    type: 'object',
    children: [
      { name: 'code', validations: [Validators.required] },
      { name: 'version', validations: [Validators.required] }
    ]
  }, {
    name: 'formDetail',
    type: 'object',
    children: [{
      name: 'taxPayerInfo',
      type: 'object',
      children: [
        { name: 'id10', validations: [Validators.required] },
        { name: 'id13', validations: [Validators.required] },
        { name: 'branchTypeName', validations: [Validators.required] },
        { name: 'branchNo', validations: [Validators.required] },
        { name: 'branchType', validations: [Validators.required] },
        { name: 'lto', validations: [Validators.required] },
        { name: 'midName', validations: [Validators.required] },
        { name: 'name', validations: [Validators.required] },
        { name: 'firstName', validations: [Validators.required] },
        { name: 'surName', validations: [Validators.required] },
        { name: 'titleCode', validations: [Validators.required] },
        { name: 'titleName', validations: [Validators.required] },
        {
          name: 'address',
          type: 'object',
          children: [
            { name: 'building', validations: [Validators.required] },
            { name: 'roomNo', validations: [Validators.required] },
            { name: 'floorNo', validations: [Validators.required] },
            { name: 'village', validations: [Validators.required] },
            { name: 'houseNo', validations: [Validators.required] },
            { name: 'moo', validations: [Validators.required] },
            { name: 'soi', validations: [Validators.required] },
            { name: 'junction', validations: [Validators.required] },
            { name: 'road', validations: [Validators.required] },
            { name: 'subdistrict', validations: [Validators.required] },
            { name: 'district', validations: [Validators.required] },
            { name: 'province', validations: [Validators.required] },
            { name: 'postCode', validations: [Validators.required] },
            { name: 'telNo', validations: [Validators.required] },
            { name: 'subDistrictCode', validations: [Validators.required] },
          ]
        }]
    }, {
      name: 'taxDetail',
      type: 'object',
      children: [
        { name: 'refNo', validations: [Validators.required] },
        { name: 'createDate', validations: [Validators.required] },
        { name: 'criminal', validations: [Validators.required] },
        { name: 'filingType', validations: [Validators.required] },
        { name: 'filingNo', validations: [Validators.required] },
        { name: 'month', validations: [Validators.required] },
        { name: 'year', validations: [Validators.required] },
        { name: 'penalty', validations: [Validators.required] },
        { name: 'roundFlag', validations: [Validators.required] },
        { name: 'surcharge', validations: [Validators.required] },
        { name: 'taxAmt', validations: [Validators.required] },
        { name: 'totAmt', validations: [Validators.required] },
        { name: 'totIncome', validations: [Validators.required] },
        { name: 'totNum', validations: [Validators.required] },
        { name: 'totTax', validations: [Validators.required] },
        { name: 'netTax', validations: [Validators.required] },
        { name: 'detail', type: 'array'},
        { name: 'homeOffice', validations: [Validators.required] }]
    }, {
      name: 'payment',
      type: 'object',
      children: [
        { name: 'dueDate', validations: [Validators.required] },
        { name: 'payAmt', validations: [Validators.required] },
      ]
    }]
  }
];

export const Pnd1AttachFormFieldConfig: FieldConfig[] = [
  {
    name: 'docDetail',
    type: 'object',
    children: [
      { name: 'code', validations: [Validators.required] },
      { name: 'version', validations: [Validators.required] }
    ]
  }, {
    name: 'attachDetail',
    type: 'object',
    children: [{
      name: 'taxPayerInfo',
      type: 'object',
      children: [
        { name: 'id13', validations: [Validators.required] },
        { name: 'branchNo', validations: [Validators.required] }
      ]
    }, {
      name: 'taxDetail',
      type: 'object',
      children: [
        { name: 'refNo', validations: [Validators.required] }
      ]
    }, {
      name: 'printDetail',
      type: 'object',
      children: [
        { name: 'numPerPage', validations: [Validators.required] }
      ]
    }]
  },{
    name: 'formAttach',
    type: 'object',
    children: [
      { name: 'attDet', type: 'array' }
    ]
  }
];

export const Pnd1AttachFormFieldInputConfig: FieldConfig[] = [
  { name: 'seq', validations: [Validators.required] },
  { name: 'id13', validations: [Validators.required,Validators.maxLength(16)] },
  { name: 'id10', validations: [Validators.required] },
  { name: 'titleName', validations: [Validators.required] },
  { name: 'firstName', validations: [Validators.required] },
  { name: 'midName', validations: [] },
  { name: 'surName', validations: [Validators.required] },
  { name: 'date', validations: [Validators.required] },
  { name: 'pay', validations: [Validators.required] },
  { name: 'tax', validations: [Validators.required] },
  { name: 'con', validations: [Validators.required] }
];