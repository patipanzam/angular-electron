import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LangComponent } from './lang.component';
import { TranslateModule, TranslateService } from '@ngx-translate/core';

describe('LangComponent', () => {
    let component: LangComponent;
    let fixture: ComponentFixture<LangComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [ TranslateModule ],
            declarations: [ LangComponent ],
            providers: [ TranslateService ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LangComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
