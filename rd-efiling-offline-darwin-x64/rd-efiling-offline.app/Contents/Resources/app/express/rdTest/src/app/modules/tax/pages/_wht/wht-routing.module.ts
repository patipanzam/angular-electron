import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Pnd1Component } from './pnd1/pnd1.component';;
import { Pnd1Resolve } from './pnd1/services/pnd1.resolve';
import { Pnd1Service } from './pnd1/services/pnd1.service';
import { ProvinceService } from '../../../../modules/tax/services/province.service';

import { Pnd1V1Component } from './pnd1/v1/pnd1-v1.component';

const routes: Routes = [{
  path: 'pnd1',
  // data: { title: 'ยื่นแบบ ภ.ง.ด.1' },
  // component: Pnd1Component,
  children: [
    {
      path: '',
      component: Pnd1V1Component,
      data: {title: 'ยื่นแบบ ภ.ง.ด.1'},
      resolve: {
        resolveData: Pnd1Resolve
      }
    },
    {
      path: 'v1',
      component: Pnd1V1Component,
      data: { title: 'ยื่นแบบ ภ.ง.ด.1'},
      resolve: {
        resolveData: Pnd1Resolve
      }
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    Pnd1Resolve, Pnd1Service,
    ProvinceService
  ]
})
export class WhtRoutingModule { }
